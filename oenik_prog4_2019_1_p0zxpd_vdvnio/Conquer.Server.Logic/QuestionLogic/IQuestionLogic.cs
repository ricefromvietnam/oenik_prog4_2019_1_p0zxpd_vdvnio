﻿//-----------------------------------------------------------------------
// <copyright file="IQuestionLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Conquer.Server.Logic.QuestionLogic
{
    using Conquer.Server.Database.ResponseModel;

    /// <summary>
    /// Logic interface to get the questions.
    /// </summary>
    public interface IQuestionLogic
    {
        /// <summary>
        /// Get the next question for the game.
        /// </summary>
        /// <param name="categoryId">Category Id to ask.</param>
        /// <returns>Returns the new question.</returns>
        ApiQuestionResponse GetNextQuestion(int categoryId);
    }
}