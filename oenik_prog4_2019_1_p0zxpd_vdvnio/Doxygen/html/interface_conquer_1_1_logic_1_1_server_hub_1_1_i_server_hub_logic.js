var interface_conquer_1_1_logic_1_1_server_hub_1_1_i_server_hub_logic =
[
    [ "InitiateConnection", "interface_conquer_1_1_logic_1_1_server_hub_1_1_i_server_hub_logic.html#a9e40b867f03470df17d26fbd2edc99b0", null ],
    [ "JoinRoom", "interface_conquer_1_1_logic_1_1_server_hub_1_1_i_server_hub_logic.html#a05b23db7d4eb2cc3cd6d8b8316ae3e57", null ],
    [ "StopConnection", "interface_conquer_1_1_logic_1_1_server_hub_1_1_i_server_hub_logic.html#a54d779c06644aee38eb7fb728c5e2a36", null ],
    [ "NextRound", "interface_conquer_1_1_logic_1_1_server_hub_1_1_i_server_hub_logic.html#a90fb23fd3ab27d4a458f62e9f360180e", null ],
    [ "OccupyArea", "interface_conquer_1_1_logic_1_1_server_hub_1_1_i_server_hub_logic.html#ac02bafa41a0ab17b8f2e5149219cd954", null ],
    [ "PlayerJoinedRoom", "interface_conquer_1_1_logic_1_1_server_hub_1_1_i_server_hub_logic.html#a236a22902ff4b0f5903490ad6ad9824e", null ],
    [ "StartGame", "interface_conquer_1_1_logic_1_1_server_hub_1_1_i_server_hub_logic.html#a222ffc9326eae8e228914d63549722d1", null ]
];