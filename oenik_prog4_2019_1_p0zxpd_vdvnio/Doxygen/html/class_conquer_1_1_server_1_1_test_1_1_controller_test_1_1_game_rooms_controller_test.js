var class_conquer_1_1_server_1_1_test_1_1_controller_test_1_1_game_rooms_controller_test =
[
    [ "GameRoomsControllerTest", "class_conquer_1_1_server_1_1_test_1_1_controller_test_1_1_game_rooms_controller_test.html#a97a902f28001c017a8b9ebeeabe1aeb6", null ],
    [ "CreateNewGameRoomWhenInvalidModelIsGiven", "class_conquer_1_1_server_1_1_test_1_1_controller_test_1_1_game_rooms_controller_test.html#ac135a2a1a9986a47f6f6f77e49e82472", null ],
    [ "CreateNewGameRoomWhenNullIsGiven", "class_conquer_1_1_server_1_1_test_1_1_controller_test_1_1_game_rooms_controller_test.html#a4ad862669750f96f3bd37342b12d280f", null ],
    [ "CreateNewGameRoomWhenValidModelIsGiven", "class_conquer_1_1_server_1_1_test_1_1_controller_test_1_1_game_rooms_controller_test.html#ac36969e2d169cae4a1eb69a320539006", null ],
    [ "Dispose", "class_conquer_1_1_server_1_1_test_1_1_controller_test_1_1_game_rooms_controller_test.html#ae712b72a29363af66ffdca4db1d80048", null ]
];