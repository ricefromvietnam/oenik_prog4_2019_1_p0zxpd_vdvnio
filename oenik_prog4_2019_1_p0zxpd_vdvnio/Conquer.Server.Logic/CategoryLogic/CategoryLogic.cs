﻿//-----------------------------------------------------------------------
// <copyright file="CategoryLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//----------------------------------------------------------------------

namespace Conquer.Server.Logic.CategoryLogic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Conquer.Server.Database.Database;
    using Conquer.Server.Repository.CategoryRepository;

    /// <summary>
    /// Logic operations for the category.
    /// </summary>
    public class CategoryLogic : ICategoryLogic
    {
        /// <summary>
        /// Category related operations field.
        /// </summary>
        private readonly ICategoryRepository category;

        /// <summary>
        /// Initializes a new instance of the <see cref="CategoryLogic"/> class.
        /// </summary>
        /// <param name="categoryRepo">Injected category repository.</param>
        public CategoryLogic(ICategoryRepository categoryRepo)
        {
            this.category = categoryRepo;
        }

        /// <summary>
        /// Get all category from the database.
        /// </summary>
        /// <returns>Returns a list of categories.</returns>
        public IEnumerable<Category> GetCategories()
        {
            return this.category.GetAllCategory();
        }
    }
}
