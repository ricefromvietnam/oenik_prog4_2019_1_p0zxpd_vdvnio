﻿//-----------------------------------------------------------------------
// <copyright file="MainMenuWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Game
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Media;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using Conquer.Database.Model;
    using Conquer.Database.Model.MenuModel;
    using Conquer.Database.Model.PlayerModel;
    using Conquer.Game.ViewModel;

    /// <summary>
    /// Interaction logic for MainMenuWindow.
    /// </summary>
    public partial class MainMenuWindow : Window
    {
        /// <summary>
        /// View model reference of the main window.
        /// </summary>
        private MainMenuViewModel viewModel;

        /// <summary>
        /// Mediaplayer for background music.
        /// </summary>
        private MediaPlayer backgroundMusicPlayer = new MediaPlayer();

        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenuWindow"/> class.
        /// </summary>
        public MainMenuWindow()
        {
            this.InitializeComponent();
            this.viewModel = this.FindResource("VM") as MainMenuViewModel;
            this.PlaybackMusic();
        }

        /// <summary>
        /// play background music method.
        /// </summary>
        public void PlaybackMusic()
        {
            this.backgroundMusicPlayer.Open(new Uri(System.Environment.CurrentDirectory + @"\media\background_bmg.mp3"));
            this.backgroundMusicPlayer.MediaEnded += new EventHandler(this.Media_Ended);
            this.backgroundMusicPlayer.Play();
        }

        /// <summary>
        /// loopback method.
        /// </summary>
        /// <param name="sender">media target.</param>
        /// <param name="e">event when music end then.</param>
        private void Media_Ended(object sender, EventArgs e)
        {
            this.backgroundMusicPlayer.Position = TimeSpan.Zero;
            this.backgroundMusicPlayer.Play();
        }

        /// <summary>
        /// Button click handler to open a new room creator window.
        /// </summary>
        /// <param name="sender">Sender of the event (button).</param>
        /// <param name="e">Parameters for the call.</param>
        private void AddNewRoomHandler(object sender, RoutedEventArgs e)
        {
            CreateNewRoomWindow create = new CreateNewRoomWindow();
            if (create.ShowDialog() == true)
            {
                CreateRoomModel newRoom = new CreateRoomModel()
                {
                    Name = create.ViewModel.RoomName,
                    CategoryId = create.ViewModel.Categories.Where(k => k.Selected).Select(r => r.Id),
                    Owner = this.viewModel.ActualPlayer,
                };

                this.viewModel.Logic.CreateNewRoom(this.viewModel.Rooms, newRoom);
                this.viewModel.ActualPlayer.PlayerRole = PlayerRole.Local;
                MainMenuRoomModel roomModel = new MainMenuRoomModel()
                {
                    Name = create.ViewModel.RoomName,
                    Topics = create.ViewModel.Categories.Where(k => k.Selected).Select(r => r.Name).ToList(),
                };

                GameLobbyWindow lobby = new GameLobbyWindow(roomModel, this.viewModel.ActualPlayer);
                lobby.ShowDialog();
            }
        }

        /// <summary>
        /// Called automatically when the window is loaded.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Parameters for the event.</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoginPromptWindow log = new LoginPromptWindow();
            if (log.ShowDialog() != null)
            {
                this.viewModel.ActualPlayer = new Player() { Name = log.LoginViewModel.Playername };
            }
        }

        /// <summary>
        /// Hander for the join room button click.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Parameters for the event.</param>
        private void EnterRoomHandler(object sender, RoutedEventArgs e)
        {
            this.viewModel.ActualPlayer.PlayerRole = PlayerRole.Local;
            Player owner = this.viewModel.Logic.EnterRoom(this.viewModel.SelectedRoom.Name, this.viewModel.ActualPlayer);
            owner.PlayerRole = PlayerRole.Remote;
            GameLobbyWindow lobby = new GameLobbyWindow(this.viewModel.SelectedRoom, owner);
            lobby.AddPlayerToWindow(this.viewModel.ActualPlayer);
            lobby.ShowDialog();
        }
    }
}
