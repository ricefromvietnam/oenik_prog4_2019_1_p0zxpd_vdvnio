﻿//-----------------------------------------------------------------------
// <copyright file="PlayerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Repository.PlayerRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Conquer.Server.Database.Database;
    using Conquer.Server.Database.ResponseModel;

    /// <summary>
    /// Repository class to store the player related database operations.
    /// </summary>
    public class PlayerRepository : GenericRepository<Player>, IPlayerRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerRepository"/> class.
        /// </summary>
        /// <param name="context">Database context.</param>
        public PlayerRepository(ConquerDatabase context)
            : base(context)
        {
        }

        /// <summary>
        /// Check in the database whether the user exists or not.
        /// </summary>
        /// <param name="playerName">Name of the player.</param>
        /// <returns>Return a list of players how has the same name.</returns>
        public IEnumerable<Player> CheckForPlayerName(string playerName)
        {
            return from player in this.Context.Players
                   where player.Name == playerName
                   select player;
        }

        /// <summary>
        /// Get all the player.
        /// </summary>
        /// <returns>all the player.</returns>
        public IEnumerable<Player> GetAllPlayer()
        {
            return this.Context.Players;
        }

        /// <summary>
        /// Get the player from the database.
        /// </summary>
        /// <param name="playerName">Name of the player.</param>
        /// <returns>Returns the entity inside the database.</returns>
        public Player GetCreatorPlayer(string playerName)
        {
            return (from player in this.Context.Players
                    where player.Name == playerName
                    select player).FirstOrDefault();
        }

        /// <summary>
        /// Get the other player inside the room.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        /// <returns>Returns the other player inside the room.</returns>
        public Player GetOtherPlayerInsideTheRoom(string roomName)
        {
            return (from player in this.Context.Players
                    where player.GameRoomId == roomName
                    select player).FirstOrDefault();
        }
    }
}
