﻿//-----------------------------------------------------------------------
// <copyright file="HubOccupyModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Database.Model.MapModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Model to handle the occupy.
    /// </summary>
    public class HubOccupyModel
    {
        /// <summary>
        /// Gets or sets the id of the area.
        /// </summary>
        public int AreaId { get; set; }

        /// <summary>
        /// Gets or sets the X position of the click.
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Gets or sets the Y position of the click.
        /// </summary>
        public int Y { get; set; }
    }
}
