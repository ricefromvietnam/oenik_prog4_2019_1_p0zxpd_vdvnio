﻿//-----------------------------------------------------------------------
// <copyright file="PlayerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.Logic.PlayerLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Conquer.Database.Model.PlayerModel;
    using Conquer.Logic.ConquerHttp;
    using Newtonsoft.Json;

    /// <summary>
    /// Logic operations for the player.
    /// </summary>
    public class PlayerLogic : IPlayerLogic
    {
        /// <summary>
        /// Http helper class.
        /// </summary>
        private readonly ConquerHttpClient http;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerLogic"/> class.
        /// </summary>
        public PlayerLogic()
        {
            this.http = new ConquerHttpClient();
        }

        /// <summary>
        /// Login to the server with the new player.
        /// </summary>
        /// <param name="newPlayer">Model of the new player.</param>
        /// <returns>Returns the inserted player.</returns>
        public Player PlayerLogin(Player newPlayer)
        {
            ConquerHttpClient http = new ConquerHttpClient();
            HttpContent serverRoom = new StringContent(JsonConvert.SerializeObject(newPlayer), Encoding.UTF8, "application/json");
            Player result = http.ConquerHttp.PostAsync("/player", serverRoom).Result.ReadAndCastJsonResponse<Player>();
            return result;
        }
    }
}
