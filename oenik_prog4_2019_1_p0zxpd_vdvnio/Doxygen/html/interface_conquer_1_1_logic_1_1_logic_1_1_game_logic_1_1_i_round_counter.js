var interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_i_round_counter =
[
    [ "IncreaseRoundCount", "interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_i_round_counter.html#a729420022a1803591a458b0b79bc1eca", null ],
    [ "IncreaseRoundCountAndSwapColor", "interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_i_round_counter.html#a8da62b25ec7f392f18d100bb8d79ada3", null ],
    [ "SetColorPick", "interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_i_round_counter.html#a8fe5b324fad7ebafa90357fc67e2f0da", null ],
    [ "PlayerColorPick", "interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_i_round_counter.html#aa92f92e3e77821b5b1f5e72efa499f98", null ],
    [ "Round", "interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_i_round_counter.html#a8381ee9a6e8220920f6b99eb709ef003", null ]
];