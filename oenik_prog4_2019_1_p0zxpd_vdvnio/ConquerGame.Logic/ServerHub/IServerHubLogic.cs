﻿//-----------------------------------------------------------------------
// <copyright file="IServerHubLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.ServerHub
{
    using System;
    using Conquer.Database.Model.MapModel;
    using Conquer.Database.Model.PlayerModel;
    using Conquer.Database.Model.RoundModel;

    /// <summary>
    /// Hub logic to handle the hub events.
    /// </summary>
    public interface IServerHubLogic
    {
        /// <summary>
        /// Event indicating that a player has joined the room.
        /// </summary>
        event EventHandler<Player> PlayerJoinedRoom;

        /// <summary>
        /// Event to notify the ui about the attack.
        /// </summary>
        event EventHandler<HubOccupyModel> OccupyArea;

        /// <summary>
        /// Event indicating that the game has started.
        /// </summary>
        event EventHandler StartGame;

        /// <summary>
        /// Event indicating that the next game round has started.
        /// </summary>
        event EventHandler<PlayerRoundModel> NextRound;

        /// <summary>
        /// Start the connection.
        /// </summary>
        void InitiateConnection();

        /// <summary>
        /// Join to a game room.
        /// </summary>
        /// <param name="gameRoom">Room name to join.</param>
        void JoinRoom(string gameRoom);

        /// <summary>
        /// Stop the connection to the hub.
        /// </summary>
        void StopConnection();
    }
}