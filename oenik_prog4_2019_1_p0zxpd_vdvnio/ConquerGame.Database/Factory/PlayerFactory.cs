﻿//-----------------------------------------------------------------------
// <copyright file="PlayerFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Database.Factory
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using Conquer.Database.Model.PlayerModel;
    using Conquer.Database.PlayerModel.Model;

    /// <summary>
    /// Factory for converting player into game players.
    /// </summary>
    public static class PlayerFactory
    {
        /// <summary>
        /// Get the game players list from list of players.
        /// </summary>
        /// <param name="players">List of players.</param>
        /// <returns>Returns the list of game players.</returns>
        public static IList<GamePlayerModel> GetPlayerList(IList<Player> players)
        {
            List<GamePlayerModel> playerModelList = new List<GamePlayerModel>();
            try
            {
                Player firstPlayer = players.ElementAt(0);
                playerModelList.Add(new GamePlayerModel()
                {
                    Id = firstPlayer.Id,
                    Name = firstPlayer.Name,
                    PlayerColor = Brushes.Red,
                    Point = 0,
                    Position = firstPlayer.PlayerRole,
                });
                Player secondPlayer = players.ElementAt(1);
                playerModelList.Add(new GamePlayerModel()
                {
                    Id = secondPlayer.Id,
                    Name = secondPlayer.Name,
                    PlayerColor = Brushes.Black,
                    Point = 0,
                    Position = secondPlayer.PlayerRole,
                });
            }
            catch (Exception)
            {
                MessageBox.Show("Not enough player");
            }

            return playerModelList;
        }
    }
}
