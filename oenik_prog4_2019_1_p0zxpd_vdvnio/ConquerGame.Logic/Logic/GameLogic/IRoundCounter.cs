﻿//-----------------------------------------------------------------------
// <copyright file="IRoundCounter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.Logic.GameLogic
{
    using System.Windows.Media;

    /// <summary>
    /// Interface for the round counting logic.
    /// </summary>
    public interface IRoundCounter
    {
        /// <summary>
        /// Gets the color of the player who can choose.
        /// </summary>
        SolidColorBrush PlayerColorPick { get; }

        /// <summary>
        /// Gets the actual round.
        /// </summary>
        int Round { get; }

        /// <summary>
        /// Increase the round count by one.
        /// </summary>
        void IncreaseRoundCount();

        /// <summary>
        /// Increase the round count by one and change the choosing player.
        /// </summary>
        void IncreaseRoundCountAndSwapColor();

        /// <summary>
        /// Set the color of the player who can choose.
        /// </summary>
        /// <param name="color">New color of the choosing player.</param>
        void SetColorPick(SolidColorBrush color);
    }
}