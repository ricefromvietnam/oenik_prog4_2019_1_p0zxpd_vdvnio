﻿//-----------------------------------------------------------------------
// <copyright file="LogicScoreBoardSendModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.LogicModel.ScoreBoard
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Logic model to represent the model to send to the server.
    /// </summary>
    public class LogicScoreBoardSendModel
    {
        /// <summary>
        /// Gets the highScore of the ScoreBoard.
        /// </summary>
        public int HighScore { get; private set; }

        /// <summary>
        /// Gets  the userName of the ScoreBoard.
        /// </summary>
        public string UserName { get; private set; }
    }
}
