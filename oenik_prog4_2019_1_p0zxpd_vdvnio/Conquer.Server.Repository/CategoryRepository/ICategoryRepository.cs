﻿//-----------------------------------------------------------------------
// <copyright file="ICategoryRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Repository.CategoryRepository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Conquer.Server.Database.Database;

    /// <summary>
    /// Interface for the category based database operations.
    /// </summary>
    public interface ICategoryRepository : IRepository<Category>
    {
        /// <summary>
        /// Get all category from the database.
        /// </summary>
        /// <returns>Returns a list of categories.</returns>
        IEnumerable<Category> GetAllCategory();
    }
}
