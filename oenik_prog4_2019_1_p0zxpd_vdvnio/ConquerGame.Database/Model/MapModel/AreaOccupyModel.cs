﻿//-----------------------------------------------------------------------
// <copyright file="AreaOccupyModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Database.Model.MapModel
{
    using Conquer.Database.Model.PlayerModel;

    /// <summary>
    /// Model to hold the properties of the occupy event.
    /// </summary>
    public class AreaOccupyModel
    {
        /// <summary>
        /// Gets or sets the id of the area that is occupied.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the type of the area that is occupied.
        /// </summary>
        public AreaType Type { get; set; }

        /// <summary>
        /// Gets or sets the previous owner of the field.
        /// </summary>
        public string PreviousOwnerName { get; set; }

        /// <summary>
        /// Gets or sets the actual player of the occupy.
        /// </summary>
        public Player ActualPlayer { get; set; }

        /// <summary>
        /// Gets or sets the X coordinate of the click.
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Gets or sets the Y coordinate of the click.
        /// </summary>
        public int Y { get; set; }
    }
}
