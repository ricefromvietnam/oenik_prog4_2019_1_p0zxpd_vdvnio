﻿//-----------------------------------------------------------------------
// <copyright file="GameLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Test.LogicTest
{
    using System;
    using System.Collections.Generic;
    using Conquer.Server.Database.RequestModel;
    using Conquer.Server.Database.ResponseModel;
    using Conquer.Server.Logic.GameLogic;
    using Conquer.Server.Logic.QuestionLogic;
    using Conquer.Server.Repository.AcknowledgedRepositroy;
    using Conquer.Server.Repository.GameRoomRepository;
    using Conquer.Server.Test.TestUtils;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test for the game logic.
    /// </summary>
    public class GameLogicTest : TestingContext<GameLogic>, IDisposable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogicTest"/> class.
        /// </summary>
        public GameLogicTest()
        {
            this.ControllerSetup();
        }

        /// <summary>
        /// Dispose method of the testing class.
        /// </summary>
        public void Dispose()
        {
            this.FixtureConf = null;
            this.InjectedMock.Clear();
        }

        /// <summary>
        /// Setup tests. Clear all fixtures before.
        /// </summary>
        [SetUp]
        public void SetupTests()
        {
            this.InjectedMock.Clear();
        }

        /// <summary>
        /// Test the game logic when a player send an acknowledge requests with invalid parameters.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        /// <param name="player">Player model who sends the quest.</param>
        /// <returns>Returns an object.</returns>
        [Test]
        [TestCaseSource(typeof(GameLogicFactory), "TestCases")]
        public ConquerNextRoundModel AcknowledgeRequestTestWhenInvalidParametersAreGiven(string roomName, PlayerQuestionModel player)
        {
            // this.GetMockFor<IAcknowledgedRepository>().Setup(e => e.AreAllPlayersAcknowledged("room")).Returns(false);
            return this.TestClass.AcknowledgeRequest(roomName, player);
        }

        /// <summary>
        /// Test the game logic when a player sends an acknowledgment with valid parameters and waiting for the other one.
        /// </summary>
        [Test]
        public void AcknowledgeRequestTestWhenValidParametersAreGivenButOnlyOnePlayerAcknowledgedIt()
        {
            this.GetMockFor<IAcknowledgedRepository>().Setup(e => e.AreAllPlayersAcknowledged("room")).Returns(false);
            ConquerNextRoundModel roundModel = this.TestClass.AcknowledgeRequest("aaa", new PlayerQuestionModel());

            Assert.IsNull(roundModel);
        }

        /// <summary>
        /// Test case when both players has answered correctly but the first player was faster.
        /// </summary>
        [Test]
        public void AcknowledgeRequestTestWhenValidParametersAreGivenAndBothAcknowLedgedAndBothPlayersAnsweredCorrectlyAndFirstPlayerWasFaster()
        {
            string roomName = "room1";
            AcknowledgeModel returns = new AcknowledgeModel()
            {
                RoomName = "aaa",
                Acknowledge = new List<PlayerQuestionModel>()
                {
                    new PlayerQuestionModel()
                    {
                        AnswerIsCorrect = true,
                        GameRoomId = "aaa",
                        Name = "Momo",
                        Id = 1,
                        AnswerTime = new DateTime(2018, 12, 12, 12, 45, 56, 78),
                    },
                    new PlayerQuestionModel()
                    {
                        AnswerIsCorrect = true,
                        GameRoomId = "aaa",
                        Name = "Momoar",
                        Id = 2,
                        AnswerTime = new DateTime(2018, 12, 12, 12, 45, 56, 200),
                    },
                },
            };

            ApiQuestionResponse question = new ApiQuestionResponse()
            {
                Questions = new List<ApiQuestionModel>()
                {
                    new ApiQuestionModel()
                    {
                        Category = "History",
                        CorrectAnswer = "1914",
                        Difficulty = "Hard",
                        Incorrect_Answers = new List<string>()
                            {
                            "1994",
                            "1987",
                            "1994",
                            },
                        Question = "When did the World War I start?",
                    },
                },
                ResponseCode = 200,
            };

            this.GetMockFor<IAcknowledgedRepository>().Setup(e => e.AreAllPlayersAcknowledged(roomName)).Returns(true);
            this.GetMockFor<IAcknowledgedRepository>().Setup(e => e.GetPlayerAcknowledgements(roomName)).Returns(returns);
            this.GetMockFor<IGameRoomRepository>().Setup(e => e.GetCategoryIdsForGameRoom(roomName)).Returns(new List<int>() { 1, 2, 3 });
            this.GetMockFor<IQuestionLogic>().Setup(e => e.GetNextQuestion(It.IsAny<int>())).Returns(question);

            ConquerNextRoundModel roundModel = this.TestClass.AcknowledgeRequest(roomName, new PlayerQuestionModel());
            Assert.AreEqual("Momo", roundModel.PrevWinnerPlayer);
            Assert.AreEqual(question.Questions[0].CorrectAnswer, roundModel.Question.CorrectAnswer);
        }

        /// <summary>
        /// Test case when both players has answered correctly but the first player was faster.
        /// </summary>
        [Test]
        public void AcknowledgeRequestTestWhenValidParametersAreGivenAndBothAcknowLedgedAndBothPlayersAnsweredCorrectlyAndTheSecondPlayerWasFaster()
        {
            string roomName = "room1";
            AcknowledgeModel secondFaster = new AcknowledgeModel()
            {
                RoomName = "aaa",
                Acknowledge = new List<PlayerQuestionModel>()
                {
                    new PlayerQuestionModel()
                    {
                        AnswerIsCorrect = true,
                        GameRoomId = "aaa",
                        Name = "Momo",
                        Id = 1,
                        AnswerTime = new DateTime(2018, 12, 12, 12, 45, 56, 78),
                    },
                    new PlayerQuestionModel()
                    {
                        AnswerIsCorrect = true,
                        GameRoomId = "aaa",
                        Name = "Momoar",
                        Id = 2,
                        AnswerTime = new DateTime(2017, 12, 12, 12, 40, 54, 200),
                    },
                },
            };

            ApiQuestionResponse question = new ApiQuestionResponse()
            {
                Questions = new List<ApiQuestionModel>()
                {
                    new ApiQuestionModel()
                    {
                        Category = "History",
                        CorrectAnswer = "1914",
                        Difficulty = "Hard",
                        Incorrect_Answers = new List<string>()
                            {
                            "1994",
                            "1987",
                            "1994",
                            },
                        Question = "When did the World War I start?",
                    },
                },
                ResponseCode = 200,
            };

            this.GetMockFor<IAcknowledgedRepository>().Setup(e => e.AreAllPlayersAcknowledged(roomName)).Returns(true);
            this.GetMockFor<IAcknowledgedRepository>().Setup(e => e.GetPlayerAcknowledgements(roomName)).Returns(secondFaster);
            this.GetMockFor<IGameRoomRepository>().Setup(e => e.GetCategoryIdsForGameRoom(roomName)).Returns(new List<int>() { 1, 2, 3 });
            this.GetMockFor<IQuestionLogic>().Setup(e => e.GetNextQuestion(It.IsAny<int>())).Returns(question);

            ConquerNextRoundModel roundModel = this.TestClass.AcknowledgeRequest(roomName, new PlayerQuestionModel());
            Assert.AreEqual("Momoar", roundModel.PrevWinnerPlayer);
            Assert.AreEqual(question.Questions[0].CorrectAnswer, roundModel.Question.CorrectAnswer);
        }

        /// <summary>
        /// Test case when the first player has answered correctly.
        /// </summary>
        [Test]
        public void AcknowledgeRequestTestWhenValidParametersAreGivenAndBothAcknowLedgedAndFirstPlayersAnsweredCorrectly()
        {
            string roomName = "room1";
            AcknowledgeModel secondFaster = new AcknowledgeModel()
            {
                RoomName = "aaa",
                Acknowledge = new List<PlayerQuestionModel>()
                {
                    new PlayerQuestionModel()
                    {
                        AnswerIsCorrect = true,
                        GameRoomId = "aaa",
                        Name = "Momo",
                        Id = 1,
                        AnswerTime = new DateTime(2018, 12, 12, 12, 45, 56, 78),
                    },
                    new PlayerQuestionModel()
                    {
                        AnswerIsCorrect = false,
                        GameRoomId = "aaa",
                        Name = "Momoar",
                        Id = 2,
                        AnswerTime = new DateTime(2017, 12, 12, 12, 40, 54, 200),
                    },
                },
            };

            ApiQuestionResponse question = new ApiQuestionResponse()
            {
                Questions = new List<ApiQuestionModel>()
                {
                    new ApiQuestionModel()
                    {
                        Category = "History",
                        CorrectAnswer = "1914",
                        Difficulty = "Hard",
                        Incorrect_Answers = new List<string>()
                            {
                            "1994",
                            "1987",
                            "1994",
                            },
                        Question = "When did the World War I start?",
                    },
                },
                ResponseCode = 200,
            };

            this.GetMockFor<IAcknowledgedRepository>().Setup(e => e.AreAllPlayersAcknowledged(roomName)).Returns(true);
            this.GetMockFor<IAcknowledgedRepository>().Setup(e => e.GetPlayerAcknowledgements(roomName)).Returns(secondFaster);
            this.GetMockFor<IGameRoomRepository>().Setup(e => e.GetCategoryIdsForGameRoom(roomName)).Returns(new List<int>() { 1, 2, 3 });
            this.GetMockFor<IQuestionLogic>().Setup(e => e.GetNextQuestion(It.IsAny<int>())).Returns(question);

            ConquerNextRoundModel roundModel = this.TestClass.AcknowledgeRequest(roomName, new PlayerQuestionModel());
            Assert.AreEqual("Momo", roundModel.PrevWinnerPlayer);
            Assert.AreEqual(question.Questions[0].CorrectAnswer, roundModel.Question.CorrectAnswer);
        }

        /// <summary>
        /// Test case when the second player has answered correctly.
        /// </summary>
        [Test]
        public void AcknowledgeRequestTestWhenValidParametersAreGivenAndBothAcknowLedgedAndSecondPlayersAnsweredCorrectly()
        {
            string roomName = "room1";
            AcknowledgeModel secondFaster = new AcknowledgeModel()
            {
                RoomName = "aaa",
                Acknowledge = new List<PlayerQuestionModel>()
                {
                    new PlayerQuestionModel()
                    {
                        AnswerIsCorrect = false,
                        GameRoomId = "aaa",
                        Name = "Momo",
                        Id = 1,
                        AnswerTime = new DateTime(2018, 12, 12, 12, 45, 56, 78),
                    },
                    new PlayerQuestionModel()
                    {
                        AnswerIsCorrect = true,
                        GameRoomId = "aaa",
                        Name = "Momoar",
                        Id = 2,
                        AnswerTime = new DateTime(2017, 12, 12, 12, 40, 54, 200),
                    },
                },
            };

            ApiQuestionResponse question = new ApiQuestionResponse()
            {
                Questions = new List<ApiQuestionModel>()
                {
                    new ApiQuestionModel()
                    {
                        Category = "History",
                        CorrectAnswer = "1914",
                        Difficulty = "Hard",
                        Incorrect_Answers = new List<string>()
                            {
                            "1994",
                            "1987",
                            "1994",
                            },
                        Question = "When did the World War I start?",
                    },
                },
                ResponseCode = 200,
            };

            this.GetMockFor<IAcknowledgedRepository>().Setup(e => e.AreAllPlayersAcknowledged(roomName)).Returns(true);
            this.GetMockFor<IAcknowledgedRepository>().Setup(e => e.GetPlayerAcknowledgements(roomName)).Returns(secondFaster);
            this.GetMockFor<IGameRoomRepository>().Setup(e => e.GetCategoryIdsForGameRoom(roomName)).Returns(new List<int>() { 1, 2, 3 });
            this.GetMockFor<IQuestionLogic>().Setup(e => e.GetNextQuestion(It.IsAny<int>())).Returns(question);

            ConquerNextRoundModel roundModel = this.TestClass.AcknowledgeRequest(roomName, new PlayerQuestionModel());
            Assert.AreEqual("Momoar", roundModel.PrevWinnerPlayer);
            Assert.AreEqual(question.Questions[0].CorrectAnswer, roundModel.Question.CorrectAnswer);
        }

        /// <summary>
        /// Test case when neither of players has answered correctly.
        /// </summary>
        [Test]
        public void AcknowledgeRequestTestWhenValidParametersAreGivenAndBothAcknowLedgedAndNeitherOfPlayersAnsweredCorrectly()
        {
            string roomName = "room1";
            AcknowledgeModel secondFaster = new AcknowledgeModel()
            {
                RoomName = "aaa",
                Acknowledge = new List<PlayerQuestionModel>()
                {
                    new PlayerQuestionModel()
                    {
                        AnswerIsCorrect = false,
                        GameRoomId = "aaa",
                        Name = "Momo",
                        Id = 1,
                        AnswerTime = new DateTime(2018, 12, 12, 12, 45, 56, 78),
                    },
                    new PlayerQuestionModel()
                    {
                        AnswerIsCorrect = false,
                        GameRoomId = "aaa",
                        Name = "Momoar",
                        Id = 2,
                        AnswerTime = new DateTime(2017, 12, 12, 12, 40, 54, 200),
                    },
                },
            };

            ApiQuestionResponse question = new ApiQuestionResponse()
            {
                Questions = new List<ApiQuestionModel>()
                {
                    new ApiQuestionModel()
                    {
                        Category = "History",
                        CorrectAnswer = "1914",
                        Difficulty = "Hard",
                        Incorrect_Answers = new List<string>()
                            {
                            "1994",
                            "1987",
                            "1994",
                            },
                        Question = "When did the World War I start?",
                    },
                },
                ResponseCode = 200,
            };

            this.GetMockFor<IAcknowledgedRepository>().Setup(e => e.AreAllPlayersAcknowledged(roomName)).Returns(true);
            this.GetMockFor<IAcknowledgedRepository>().Setup(e => e.GetPlayerAcknowledgements(roomName)).Returns(secondFaster);
            this.GetMockFor<IGameRoomRepository>().Setup(e => e.GetCategoryIdsForGameRoom(roomName)).Returns(new List<int>() { 1, 2, 3 });
            this.GetMockFor<IQuestionLogic>().Setup(e => e.GetNextQuestion(It.IsAny<int>())).Returns(question);

            ConquerNextRoundModel roundModel = this.TestClass.AcknowledgeRequest(roomName, new PlayerQuestionModel());
            Assert.AreEqual(string.Empty, roundModel.PrevWinnerPlayer);
            Assert.AreEqual(question.Questions[0].CorrectAnswer, roundModel.Question.CorrectAnswer);
        }
    }
}
