﻿//-----------------------------------------------------------------------
// <copyright file="CategoryController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Conquer.Server.Logic.CategoryLogic;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Controller to hold the category based operations.
    /// </summary>
    [Route("[controller]")]
    public class CategoryController : Controller
    {
        /// <summary>
        /// Category related logic operations.
        /// </summary>
        private readonly ICategoryLogic category;

        /// <summary>
        /// Initializes a new instance of the <see cref="CategoryController"/> class.
        /// </summary>
        /// <param name="category">Category logic.</param>
        public CategoryController(ICategoryLogic category)
        {
            this.category = category;
        }

        /// <summary>
        /// Get all category from the database.
        /// </summary>
        /// <returns>Returns a list of categories.</returns>
        [HttpGet("")]
        public IActionResult GetquestionCategories()
        {
            return this.Ok(this.category.GetCategories());
        }
    }
}