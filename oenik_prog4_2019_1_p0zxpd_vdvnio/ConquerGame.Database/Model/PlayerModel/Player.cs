﻿//-----------------------------------------------------------------------
// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Database.Model.PlayerModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Model to hold the basic properties for a player.
    /// </summary>
    public class Player : ObservableObject
    {
        /// <summary>
        /// Id of the player.
        /// </summary>
        private int id;

        /// <summary>
        /// Name of the player.
        /// </summary>
        private string name;

        /// <summary>
        /// Role of the player.
        /// </summary>
        private PlayerRole playerRole;

        /// <summary>
        /// Gets or sets the role of the player.
        /// </summary>
        public PlayerRole PlayerRole
        {
            get { return this.playerRole; }
            set { this.playerRole = value; }
        }

        /// <summary>
        /// Gets or sets the id of the player.
        /// </summary>
        public int Id
        {
            get { return this.id; }
            set { this.Set(ref this.id, value); }
        }

        /// <summary>
        /// Gets or sets the name of the player.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }
    }
}
