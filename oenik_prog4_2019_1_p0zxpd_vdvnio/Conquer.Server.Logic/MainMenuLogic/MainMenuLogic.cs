﻿//-----------------------------------------------------------------------
// <copyright file="MainMenuLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Conquer.Server.Logic.MainMenuLogic
{
    using System.Collections.Generic;
    using Conquer.Server.Database.Database;
    using Conquer.Server.Database.ResponseModel;
    using Conquer.Server.Logic.MainMenuLogic.Model;
    using Conquer.Server.Repository.AcknowledgedRepositroy;
    using Conquer.Server.Repository.GameRoomRepository;
    using Conquer.Server.Repository.PlayerRepository;

    /// <summary>
    /// Implementation of the logic interface.
    /// </summary>
    public class MainMenuLogic : IMainMenuLogic
    {
        /// <summary>
        /// Repository for the menu operations.
        /// </summary>
        private readonly IGameRoomRepository menuRepo;

        /// <summary>
        /// Repository for the player operations.
        /// </summary>
        private readonly IPlayerRepository playerRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenuLogic"/> class.
        /// </summary>
        /// <param name="menuRepo">Reference to the menu repository.</param>
        /// <param name="playerRepo">Reference to the player repository.</param>
        public MainMenuLogic(IGameRoomRepository menuRepo, IPlayerRepository playerRepo)
        {
            this.menuRepo = menuRepo;
            this.playerRepo = playerRepo;
        }

        /// <summary>
        /// Create a new game room into the database.
        /// </summary>
        /// <param name="newRoom">New room instance.</param>
        /// <returns>Returns the created entity.</returns>
        public GameRoom CreateNewGameRoom(GameRoomCategoryModel newRoom)
        {
            // TODO As there is not identity in the database add check whether it is unique or not
            GameRoom newR = new GameRoom() { Name = newRoom.Name };
            GameRoom insertedItem = this.menuRepo.Insert(newR);
            this.menuRepo.Save();
            Player owner = this.playerRepo.GetCreatorPlayer(newRoom.Owner.Name);
            owner.GameRoomId = newRoom.Name;

            this.playerRepo.Update(owner, owner.Id);

            if (newRoom.CategoryId != null)
            {
                this.menuRepo.InsertGameCategoryConnections(insertedItem.Id, newRoom.CategoryId);
                this.menuRepo.Save();
            }

            return insertedItem;
        }

        /// <summary>
        /// Implementation of the interface method get all rooms.
        /// </summary>
        /// <returns>Returns the list of available rooms.</returns>
        public IEnumerable<ConquerMainMenuRoomsModel> GetAllGameRoom()
        {
            return this.menuRepo.GetAllGameRooms();
        }
    }
}
