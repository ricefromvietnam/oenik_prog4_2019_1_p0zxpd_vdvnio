﻿//-----------------------------------------------------------------------
// <copyright file="IScoreLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.Logic.GameLogic.ScoreLogic
{
    using Conquer.Database.Model.MapModel;
    using Conquer.Database.PlayerModel.Model;

    /// <summary>
    /// Score board based logic.
    /// </summary>
    public interface IScoreLogic
    {
        /// <summary>
        /// Increase the point of the provided player.
        /// </summary>
        /// <param name="player">PLayer to give point.</param>
        /// <param name="area">Area that was occupied.</param>
        void IncreasePlayerScore(GamePlayerModel player, AreaType area);
    }
}