﻿//-----------------------------------------------------------------------
// <copyright file="IMapLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.Logic.GameLogic.Map
{
    using Conquer.Database.Model.MapModel;
    using Conquer.Database.Model.PlayerModel;

    /// <summary>
    /// Interface to handle the map connected internal logic.
    /// </summary>
    public interface IMapLogic
    {
        /// <summary>
        /// Occupy one area on the map.
        /// Update the internal list.
        /// </summary>
        /// <param name="areaId">Id of the area to conquer.</param>
        /// <param name="occupyingPlayer">Player who starts the occupy.</param>
        /// <returns>Returns a bool value to indicate if the occupy was a success or not.</returns>
        bool? OccupyOneArea(int areaId, Player occupyingPlayer);

        /// <summary>
        /// Get the type of area.
        /// </summary>
        /// <param name="areaId">Id of the area.</param>
        /// <returns>Returns the type of area.</returns>
        AreaType GetTypeOfArea(int areaId);

        /// <summary>
        /// Occupy one area on the map.
        /// Update the state of the map list.
        /// </summary>
        /// <param name="areaId">Id of the area to occupy.</param>
        /// <param name="occupyingPlayer">Player who starts the occupy.</param>
        /// <returns>Returns the type of the occupy field.</returns>
        AreaType GetPlayerAreaType(int areaId, Player occupyingPlayer);

        /// <summary>
        /// Get the owner of area.
        /// </summary>
        /// <param name="areaId">Id of the area.</param>
        /// <returns>Returns the owner of area.</returns>
        Player GetOwnerOfTheArea(int areaId);
    }
}