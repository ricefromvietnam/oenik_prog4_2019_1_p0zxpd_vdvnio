﻿//-----------------------------------------------------------------------
// <copyright file="ConquerNextRoundModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Conquer.Server.Database.ResponseModel
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Model to store the important properties of the next round hub method.
    /// </summary>
    public class ConquerNextRoundModel
    {
        /// <summary>
        /// Gets or sets the name of the winning player.
        /// </summary>
        public string PrevWinnerPlayer { get; set; }

        /// <summary>
        /// Gets or sets the round of the game.
        /// </summary>
        public int Round { get; set; }

        /// <summary>
        /// Gets or sets the question for the next round.
        /// </summary>
        public ConquerQuestionModel Question { get; set; }
    }
}
