﻿//-----------------------------------------------------------------------
// <copyright file="ConquerQuestionModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Database.Model.RoundModel
{
    using System.Collections.ObjectModel;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Converted model of the question for the client side.
    /// </summary>
    public class ConquerQuestionModel : ObservableObject
    {
        /// <summary>
        /// Category of the question.
        /// </summary>
        private string category;

        /// <summary>
        /// Difficulty of the question.
        /// </summary>
        private string difficulty;

        /// <summary>
        /// Actual question.
        /// </summary>
        private string question;

        /// <summary>
        /// Correct answer for the question.
        /// </summary>
        private string correctAnswer;

        /// <summary>
        /// Gets or sets the options of the question.
        /// </summary>
        public ObservableCollection<string> Options { get; set; }

        /// <summary>
        /// Gets or sets the correct answer of the question.
        /// </summary>
        public string CorrectAnswer
        {
            get { return this.correctAnswer; }
            set { this.Set(ref this.correctAnswer, value); }
        }

        /// <summary>
        /// Gets or sets the question.
        /// </summary>
        public string Question
        {
            get { return this.question; }
            set { this.Set(ref this.question, value); }
        }

        /// <summary>
        /// Gets or sets the difficulty of the question.
        /// </summary>
        public string Difficulty
        {
            get { return this.difficulty; }
            set { this.Set(ref this.difficulty, value); }
        }

        /// <summary>
        /// Gets or sets the category of the question.
        /// </summary>
        public string Category
        {
            get { return this.category; }
            set { this.Set(ref this.category, value); }
        }
    }
}