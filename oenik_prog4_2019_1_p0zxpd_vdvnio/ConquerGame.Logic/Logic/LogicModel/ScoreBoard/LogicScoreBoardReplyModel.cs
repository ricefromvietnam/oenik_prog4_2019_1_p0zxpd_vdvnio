﻿//-----------------------------------------------------------------------
// <copyright file="LogicScoreBoardReplyModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.LogicModel.ScoreBoard
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Logic model to represent the reply of the server.
    /// </summary>
    public class LogicScoreBoardReplyModel : LogicScoreBoardSendModel
    {
        /// <summary>
        /// Gets the position of the player.
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Gets the position of the player.
        /// </summary>
        public int Position { get; private set; }
    }
}
