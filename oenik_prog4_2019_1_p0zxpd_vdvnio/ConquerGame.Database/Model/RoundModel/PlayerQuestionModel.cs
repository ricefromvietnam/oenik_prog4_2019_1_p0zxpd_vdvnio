﻿//-----------------------------------------------------------------------
// <copyright file="PlayerQuestionModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Database.Model.RoundModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Conquer.Database.Model.PlayerModel;

    /// <summary>
    /// Model to hold the properties of a question round.
    /// </summary>
    public class PlayerQuestionModel : Player
    {
        /// <summary>
        /// Gets or sets a value indicating whether player answered correctly.
        /// </summary>
        public bool AnswerIsCorrect { get; set; }

        /// <summary>
        /// Gets or sets the time of the answer in order to decide who won that round.
        /// Faster wins it all in the occupy phase.
        /// </summary>
        public DateTime AnswerTime { get; set; }

        /// <summary>
        /// Gets or sets the actual round of the game.
        /// </summary>
        public int Round { get; set; }
    }
}
