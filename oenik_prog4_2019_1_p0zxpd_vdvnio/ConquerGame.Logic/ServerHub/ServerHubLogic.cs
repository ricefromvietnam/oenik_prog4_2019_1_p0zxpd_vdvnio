﻿//-----------------------------------------------------------------------
// <copyright file="ServerHubLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.ServerHub
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Conquer.Constants;
    using Conquer.Database.Model;
    using Conquer.Database.Model.MapModel;
    using Conquer.Database.Model.PlayerModel;
    using Conquer.Database.Model.RoundModel;
    using Microsoft.AspNetCore.SignalR.Client;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Holds the logic of the server hub.
    /// </summary>
    public class ServerHubLogic : IServerHubLogic
    {
        /// <summary>
        /// Property to hold the main class of the hub.
        /// </summary>
        private HubConnection hubCon;

        /// <summary>
        /// Previous round of the game.
        /// </summary>
        private int lastRound;

        /// <summary>
        /// Event to notify the ui that a player has joined to the room.
        /// </summary>
        public event EventHandler<Player> PlayerJoinedRoom;

        /// <summary>
        /// Event to notify the ui that a start game signal has arrived.
        /// </summary>
        public event EventHandler StartGame;

        /// <summary>
        /// Event to notify the ui about the next round.
        /// </summary>
        public event EventHandler<PlayerRoundModel> NextRound;

        /// <summary>
        /// Event to notify the ui about the attack.
        /// </summary>
        public event EventHandler<HubOccupyModel> OccupyArea;

        /// <summary>
        /// Build the hub connection to the server and start it.
        /// </summary>
        public void InitiateConnection()
        {
            if (this.hubCon == null)
            {
                this.hubCon = new HubConnectionBuilder()
                        .WithUrl($"{Constant.ServerIP}{Constant.ServerPort}/game")
                        .ConfigureLogging(logging =>
                            {
                                // Log to the Console
                                logging.AddDebug();

                                // This will set ALL logging to Debug level
                                logging.SetMinimumLevel(LogLevel.Debug);
                            })
                        .Build();

                this.StartConnection();
                this.RegisterHubEvents();
            }
        }

        /// <summary>
        /// Join to a room with the specified name.
        /// </summary>
        /// <param name="gameRoom">Name of the room.</param>
        public async void JoinRoom(string gameRoom)
        {
            await this.hubCon.InvokeAsync<string>("join", gameRoom);
        }

        /// <summary>
        /// Invoke a hub event to occupy one area on the map.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        /// <param name="area">Id of the area.</param>
        public async void OccupyHubMethod(string roomName, HubOccupyModel area)
        {
            await this.hubCon.InvokeAsync("occupy", roomName, area);
        }

        /// <summary>
        /// Stop the connection to the hub.
        /// </summary>
        public async void StopConnection()
        {
            await this.hubCon.StopAsync();
            this.hubCon = null;
        }

        /// <summary>
        /// Register the possible events that can arrive on the hub.
        /// Hidden dependency. Bad example :(.
        /// </summary>
        private void RegisterHubEvents()
        {
            this.hubCon.On<Player>(
                                    "playerJoined",
                                    param =>
                                    {
                                        param.PlayerRole = PlayerRole.Remote;
                                        this.PlayerJoinedRoom?.Invoke(this, param);
                                    });

            this.hubCon.On(
                            "startGame",
                            () =>
                           {
                               this.StartGame?.Invoke(this, EventArgs.Empty);
                           });
            this.hubCon.On<PlayerRoundModel>(
                                             "nextRound",
                                             round =>
                                             {
                                                 Debug.Print("NEXTROUND" + round.PrevWinnerPlayer);
                                                 if (this.lastRound != round.Round || round.Round == 0)
                                                 {
                                                     Debug.Print("NOT SAME ROUND " + round.Round);
                                                     this.lastRound = round.Round;
                                                     this.NextRound?.Invoke(this, round);
                                                 }
                                                 else
                                                 {
                                                     Debug.Print("SAMEROUND " + round.Round);
                                                 }
                                             });
            this.hubCon.On<HubOccupyModel>(
                                  "areaOccupy",
                                  round =>
                                   {
                                       Debug.Print("OCCUPY");
                                       this.OccupyArea?.Invoke(this, round);
                                   });
        }

        /// <summary>
        /// Start the connection.
        /// </summary>
        private async void StartConnection()
        {
            await this.hubCon.StartAsync();
        }
    }
}
