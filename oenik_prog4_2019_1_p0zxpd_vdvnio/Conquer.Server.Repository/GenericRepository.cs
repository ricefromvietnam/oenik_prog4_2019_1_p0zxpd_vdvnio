﻿//-----------------------------------------------------------------------
// <copyright file="GenericRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using AutoMapper;
    using Conquer.Server.Database.Database;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Contains implementation of the basic database operations.
    /// </summary>
    /// <typeparam name="TEntity">Type of the table that is being used.</typeparam>
    public class GenericRepository<TEntity>
        : IRepository<TEntity>
        where TEntity : class
    {
        /// <summary>
        /// Property mapper for classes.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Current context of database.
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Reviewed.")]
        private DbSet<TEntity> dbSet;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericRepository{TEntity}"/> class.
        /// </summary>
        /// <param name="context">Database context from which we can determine the state.</param>
        public GenericRepository(ConquerDatabase context)
        {
            this.Context = context;
            this.mapper = AutomapperConfig.InitializeMapper();
            this.dbSet = context.Set<TEntity>();
        }

        /// <summary>
        /// Gets the current Entity type database table.
        /// </summary>
        public IEnumerable<TEntity> GetAll
        {
            get { return this.dbSet.ToList(); }
        }

        /// <summary>
        /// Gets the databaseContext.
        /// </summary>
        public ConquerDatabase Context { get; private set; }

        /// <summary>
        /// Modified version of Where extension where we can specify also the tracking type and includes as well.
        /// </summary>
        /// <param name="filter">Filter query. If not specified than the whole table will be returned.</param>
        /// <param name="orderBy">Property from which the list will be ordered.</param>
        /// <param name="includeProperties">Includes the foreign key properties .</param>
        /// <param name="noTracking">Tracking state of database objects.</param>
        /// <returns>List of instances which fulfills the filter parameter.</returns>
        public virtual IQueryable<TEntity> GetWithExtras(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "",
            bool noTracking = false)
        {
            IQueryable<TEntity> query = null;
            if (noTracking)
            {
                query = this.dbSet.AsNoTracking();
            }
            else
            {
                query = this.dbSet.AsQueryable();
            }

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query);
            }
            else
            {
                return query;
            }
        }

        /// <summary>
        /// Generic version of inserting to the database.
        /// </summary>
        /// <typeparam name="T">Type of object.</typeparam>
        /// <param name="entity">Input object that wanted to be inserted.</param>
        /// <returns>Return null if there was an error during the procedure.
        /// Returns the inserted item.</returns>
        public virtual TEntity Insert<T>(T entity)
        {
            var item = this.mapper.Map<TEntity>(entity);
            this.dbSet.Add(item);
            return item;
        }

        /// <summary>
        /// Generic version of Delete from database based on certain primary key.
        /// </summary>
        /// <param name="id">Primary key of the object wanted to be deleted.</param>
        /// <returns>Return the deleted object.</returns>
        public virtual TEntity Delete(object id)
        {
            TEntity entityToDelete = this.dbSet.Find(id);
            this.Delete(entityToDelete);
            return entityToDelete;
        }

        /// <summary>
        /// Generic version of Delete from database based on the object.
        /// </summary>
        /// <exception cref="ArgumentNullException">If the input parameter was null.</exception>
        /// <param name="entityToDelete">Object that wanted to be deleted.</param>
        /// <returns>Returns the deleted item.</returns>
        public virtual TEntity Delete(TEntity entityToDelete)
        {
            if (entityToDelete == null)
            {
                throw new ArgumentNullException("entityToDelete", "Null delete input");
            }
            else
            {
                if (this.Context.Entry(entityToDelete).State == EntityState.Detached)
                {
                    this.dbSet.Attach(entityToDelete);
                }

                this.dbSet.Remove(entityToDelete);
            }

            return entityToDelete;
        }

        /// <summary>
        /// Generic version of database update.
        /// </summary>
        /// <typeparam name="T">Type of entity.</typeparam>
        /// <param name="entityToUpdate">Object with new values.</param>
        /// <param name="key">Primary key of the object.</param>
        /// <returns>Return the updated element.</returns>
        public virtual TEntity Update<T>(T entityToUpdate, object key)
        {
            if (entityToUpdate != null)
            {
                TEntity exist = this.Context.Set<TEntity>().Find(key);
                if (exist != null)
                {
                    this.Context.Entry(exist).CurrentValues.SetValues(entityToUpdate);
                    this.Save();
                }

                return exist;
            }

            return null;
        }

        /// <summary>
        /// Save changes in the database.
        /// </summary>
        public void Save()
        {
            this.Context.SaveChanges();
        }
    }
}
