﻿//-----------------------------------------------------------------------
// <copyright file="DifficultyToColorStringConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Game.Converter
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Data;

    /// <summary>
    /// Converter to convert difficulty tag to a color string.
    /// </summary>
    public class DifficultyToColorStringConverter : IValueConverter
    {
        /// <summary>
        /// Convert the difficulty tag to a color string.
        /// </summary>
        /// <param name="value">Difficulty string.</param>
        /// <param name="targetType">Target type.</param>
        /// <param name="parameter">Parameter for the conversion.</param>
        /// <param name="culture">Font culture.</param>
        /// <returns>Returns a color string from the tag.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string tag = value as string;
            string colorString = string.Empty;
            switch (tag.ToLower())
            {
                case "easy":
                    {
                        colorString = "ForestGreen";
                        break;
                    }

                case "medium":
                    {
                        colorString = "LightYellow";
                        break;
                    }

                case "hard":
                    {
                        colorString = "Red";
                        break;
                    }
            }

            return colorString;
        }

        /// <summary>
        /// Convert a color string to a difficulty tag.
        /// </summary>
        /// <param name="value">Color string.</param>
        /// <param name="targetType">Target type.</param>
        /// <param name="parameter">Parameter for the conversion.</param>
        /// <param name="culture">Font culture.</param>
        /// <returns>Returns difficulty tag.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
