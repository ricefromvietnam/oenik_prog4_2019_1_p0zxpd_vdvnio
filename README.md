# Description

Base: Simple quiz game based on the logic of famous Hungarian Honfoglalo game.

# Game Logic

Before starting the actual game player can decide create a room or join a room. They can set the difficulty level and the chosen topics as well. By default no hardness is selected and all (random) topics included. Quiz will be generated from api json.

The actual game consists of two main round where in the first round the player can occupy territories on the map. The player can occupy only if answer for the question is correct. 

Territories worth points in the range of 200-500 based on the size.
