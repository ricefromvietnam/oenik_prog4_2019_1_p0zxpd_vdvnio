﻿//-----------------------------------------------------------------------
// <copyright file="ConquerHttpClient.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.ConquerHttp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Conquer.Constants;
    using Newtonsoft.Json;

    /// <summary>
    /// Customized http client directly attached to the server.
    /// </summary>
    public class ConquerHttpClient
    {
        /// <summary>
        /// Base http client set by the constructor.
        /// </summary>
        private HttpClient httpClient;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConquerHttpClient"/> class.
        /// </summary>
        public ConquerHttpClient()
        {
            this.httpClient = new HttpClient();
            this.httpClient.BaseAddress = new Uri($"{Constant.ServerIP}{Constant.ServerPort}");
        }

        /// <summary>
        /// Gets the customized http client in order to spare the base url to the server.
        /// </summary>
        public HttpClient ConquerHttp
        {
            get { return this.httpClient; }
        }
    }
}
