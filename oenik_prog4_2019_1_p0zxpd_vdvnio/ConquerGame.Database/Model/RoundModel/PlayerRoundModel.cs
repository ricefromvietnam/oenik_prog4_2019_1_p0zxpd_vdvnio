﻿//-----------------------------------------------------------------------
// <copyright file="PlayerRoundModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Database.Model.RoundModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Model to store the important properties of the next round hub method.
    /// </summary>
    public class PlayerRoundModel
    {
        /// <summary>
        /// Gets or sets the name of the winning player.
        /// </summary>
        public string PrevWinnerPlayer { get; set; }

        /// <summary>
        /// Gets or sets the question for the next round.
        /// </summary>
        public ConquerQuestionModel Question { get; set; }

        /// <summary>
        /// Gets or sets the actual round.
        /// </summary>
        public int Round { get; set; }
    }
}
