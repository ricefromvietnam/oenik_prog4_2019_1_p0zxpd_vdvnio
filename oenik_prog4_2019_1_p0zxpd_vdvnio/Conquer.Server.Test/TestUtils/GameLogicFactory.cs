﻿//-----------------------------------------------------------------------
// <copyright file="GameLogicFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Test.TestUtils
{
    using System.Collections;
    using Conquer.Server.Database.RequestModel;
    using NUnit.Framework;

    /// <summary>
    /// Factory method for generating the test cases.
    /// </summary>
    public class GameLogicFactory
    {
        /// <summary>
        /// Gets the test cases for the acknowledge request.
        /// </summary>
        public static IEnumerable TestCases
        {
            get
            {
                yield return new TestCaseData(string.Empty, null).Returns(null);
                yield return new TestCaseData(string.Empty, new PlayerQuestionModel()).Returns(null);
                yield return new TestCaseData("alma", null).Returns(null);
            }
        }
    }
}
