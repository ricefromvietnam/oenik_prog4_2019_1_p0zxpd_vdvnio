﻿//-----------------------------------------------------------------------
// <copyright file="IMainMenuLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.Logic.MainMenuLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Conquer.Database.Model;
    using Conquer.Database.Model.MenuModel;
    using Conquer.Database.Model.PlayerModel;

    /// <summary>
    /// Interface to handle the main menu operations.
    /// </summary>
    public interface IMainMenuLogic
    {
        /// <summary>
        /// Ask the server about the available playable rooms.
        /// </summary>
        /// <param name="listToAdd">List of rooms to add to.</param>
        void GetMainMenuRooms(IList<MainMenuRoomModel> listToAdd);

        /// <summary>
        /// Create new room on the server side as well.
        /// </summary>
        /// <param name="listToAdd">List of room to add to.</param>
        /// <param name="newRoom">New room to insert.</param>
        void CreateNewRoom(IList<MainMenuRoomModel> listToAdd, CreateRoomModel newRoom);

        /// <summary>
        /// Enter to a room.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        /// <param name="actualPlayer">Actual player who wants to enter.</param>
        /// <returns>Return the other player inside the room.</returns>
        Player EnterRoom(string roomName, Player actualPlayer);
    }
}
