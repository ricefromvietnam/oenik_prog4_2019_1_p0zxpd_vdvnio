﻿//-----------------------------------------------------------------------
// <copyright file="Constant.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Constants
{
    using System;

    /// <summary>
    /// Configuration class to hold the constant values.
    /// </summary>
    public class Constant
    {
        /// <summary>
        /// Url for the background.
        /// </summary>
        public const string BackgroundImg = "img/base_2.bmp";

        /// <summary>
        /// Url for the heat amp.
        /// </summary>
        public const string HeatMatImg = "img/heat_map.bmp";

        /// <summary>
        /// Url for the castle.
        /// </summary>
        public const string CastleImg = "img/castle-me.png";

        /// <summary>
        /// Width of the map.
        /// </summary>
        public const int MapSizeWidth = 1200;

        /// <summary>
        /// Number of area on the map.
        /// </summary>
        public const int NumOfArea = 19;

        /// <summary>
        /// Height of the map.
        /// </summary>
        public const int MapSizeHeight = 751;

        /// <summary>
        /// Width of the castle.
        /// </summary>
        public const int CastleWidth = 100;

        /// <summary>
        /// Height of the castle.
        /// </summary>
        public const int CastleHeight = 74;

        /// <summary>
        /// Total game time.
        /// </summary>
        public const byte MapGameTime = 10;

        /// <summary>
        /// Port of the server.
        /// </summary>
        public const int ServerPort = 65189;

        /// <summary>
        /// Ip of the server.
        /// </summary>
        public const string ServerIP = "http://localhost:";

        /// <summary>
        /// Gets or sets the max round in the game.
        /// </summary>
        public const int MaxRound = 19;
    }
}
