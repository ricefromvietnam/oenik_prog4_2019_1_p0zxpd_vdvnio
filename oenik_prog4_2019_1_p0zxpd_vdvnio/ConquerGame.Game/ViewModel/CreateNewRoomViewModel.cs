﻿//-----------------------------------------------------------------------
// <copyright file="CreateNewRoomViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Game.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CommonServiceLocator;
    using Conquer.Database.Model.NewRoomModel;
    using Conquer.Logic.Logic.MainMenuLogic;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// View model for the create new room window.
    /// </summary>
    public class CreateNewRoomViewModel : ViewModelBase
    {
        /// <summary>
        /// Random value for the random name generator.
        /// </summary>
        private static Random random = new Random();

        /// <summary>
        /// Name of the room.
        /// </summary>
        private string roomName;

        /// <summary>
        /// Initializes a new instance of the <see cref="CreateNewRoomViewModel"/> class.
        /// </summary>
        public CreateNewRoomViewModel()
        {
            this.Categories = new ObservableCollection<QuestionCategoryModel>();
            if (this.IsInDesignMode)
            {
                this.roomName = "Almafa";
                this.Categories.Add(new QuestionCategoryModel() { Id = 1, Name = "Health", Selected = false });
                this.Categories.Add(new QuestionCategoryModel() { Id = 2, Name = "Speed", Selected = false });
                this.Categories.Add(new QuestionCategoryModel() { Id = 3, Name = "Margin", Selected = false });
            }
            else
            {
                this.Logic.GetAllCategories(this.Categories);
            }
        }

        /// <summary>
        /// Gets the list of Categories.
        /// </summary>
        public ObservableCollection<QuestionCategoryModel> Categories { get; private set; }

        /// <summary>
        /// Gets or sets the name of the room.
        /// </summary>
        public string RoomName
        {
            get { return this.roomName; }
            set { this.Set(ref this.roomName, value); }
        }

        /// <summary>
        /// Gets logic injected by the IOC container.
        /// </summary>
        private ICreateNewRoomLogic Logic
        {
            get { return ServiceLocator.Current.GetInstance<ICreateNewRoomLogic>(); }
        }

        /// <summary>
        /// Generate random string if the user was lazy and just clicked ok/cancel.
        /// </summary>
        /// <returns>Returns the name of the room.</returns>
        public string RandomString()
        {
            const string Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(Chars, 8)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
