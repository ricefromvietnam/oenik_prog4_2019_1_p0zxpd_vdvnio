﻿//-----------------------------------------------------------------------
// <copyright file="GameHub.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server
{
    using System;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using Conquer.Server.Database.Database;
    using Conquer.Server.Database.RequestModel;
    using Microsoft.AspNetCore.SignalR;

    /// <summary>
    /// Core server functions of the game.
    /// </summary>
    public class GameHub : Hub
    {
        /// <summary>
        /// Create a new room in the hub to join to for later usage.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        /// <returns>Void task.</returns>
        [HubMethodName("join")]
        public async Task JoinRoom(string roomName)
        {
            await this.Groups.AddToGroupAsync(this.Context.ConnectionId, roomName);
        }

        /// <summary>
        /// Send signal to other player in the room about occupy one area on the map.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        /// <param name="area">Area that want to occupy.</param>
        /// <returns>Void task.</returns>
        [HubMethodName("occupy")]
        public async Task OccupyOneArea(string roomName, CaptureModel area)
        {
            await this.Clients.OthersInGroup(roomName).SendAsync("areaOccupy", area);
        }

        /// <summary>
        /// Called when a player has leaved a room.
        /// </summary>
        /// <param name="roomName">Name of the room to leave.</param>
        /// <returns>Void function.</returns>
        [HubMethodName("leave")]
        public Task LeaveRoom(string roomName)
        {
            return this.Groups.RemoveFromGroupAsync(this.Context.ConnectionId, roomName);
        }

        /// <summary>
        /// Override the disconnect to see when the user disconnects.
        /// </summary>
        /// <param name="exception">Exception raised by disconnect.</param>
        /// <returns>Void task.</returns>
        public override Task OnDisconnectedAsync(Exception exception)
        {
            Debug.Print("DISCONNECT");
            return base.OnDisconnectedAsync(exception);
        }

        /// <summary>
        /// Override the connect to see when the user connects.
        /// </summary>
        /// <returns>Void task.</returns>
        public override Task OnConnectedAsync()
        {
            Debug.Print("CONNECT");
            return base.OnConnectedAsync();
        }
    }
}
