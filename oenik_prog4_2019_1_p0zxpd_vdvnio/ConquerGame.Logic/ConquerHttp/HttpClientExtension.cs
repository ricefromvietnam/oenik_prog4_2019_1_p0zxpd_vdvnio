﻿//-----------------------------------------------------------------------
// <copyright file="HttpClientExtension.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.ConquerHttp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Newtonsoft.Json;

    /// <summary>
    /// Extension method for the request response read.
    /// </summary>
    public static class HttpClientExtension
    {
        /// <summary>
        /// Reads and cast the answer of the request response.
        /// </summary>
        /// <typeparam name="T">Type that you want to convert.</typeparam>
        /// <exception cref="HttpRequestException">Throws if the request does not have a success status code.</exception>
        /// <param name="result">Request response object.</param>
        /// <returns>Returns the casted json object in the T type. </returns>
        public static T ReadAndCastJsonResponse<T>(this HttpResponseMessage result)
        {
            if (!result.IsSuccessStatusCode)
            {
                // TODO error handling if something is wrong.
                //  throw new HttpRequestException(result.StatusCode.ToString());
            }

            T response = JsonConvert.DeserializeObject<T>(result.Content.ReadAsStringAsync().Result);

            return response;
        }
    }
}
