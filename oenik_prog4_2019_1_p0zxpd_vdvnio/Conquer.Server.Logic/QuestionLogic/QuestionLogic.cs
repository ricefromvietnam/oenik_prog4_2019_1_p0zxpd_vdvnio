﻿//-----------------------------------------------------------------------
// <copyright file="QuestionLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Conquer.Server.Logic.QuestionLogic
{
    using System.Diagnostics;
    using System.IO;
    using System.Net.Http;
    using System.Web;
    using Conquer.Server.Database.ResponseModel;
    using Newtonsoft.Json;

    /// <summary>
    /// Logic to handle the external question logic.
    /// </summary>
    public class QuestionLogic : IQuestionLogic
    {
        /// <summary>
        /// Get the next question with the provided id.
        /// </summary>
        /// <param name="categoryId">Category id of the question needed.</param>
        /// <returns>Returns the question model.</returns>
        public ApiQuestionResponse GetNextQuestion(int categoryId)
        {
            HttpClient request = new HttpClient();

            var responseResult = request.GetAsync($"https://opentdb.com/api.php?amount=1&category=" + categoryId + "&type=multiple").Result;
            string responseString = responseResult.Content.ReadAsStringAsync().Result;
            Debug.Print("NEXTQUESTION: " + responseString);
            return JsonConvert.DeserializeObject<ApiQuestionResponse>(responseString);
        }
    }
}
