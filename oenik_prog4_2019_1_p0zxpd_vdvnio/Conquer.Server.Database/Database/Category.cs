﻿//-----------------------------------------------------------------------
// <copyright file="Category.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Database.Database
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Database table to hold the category.
    /// </summary>
    public class Category
    {
        /// <summary>
        /// Gets or sets the primary id of the category.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the category.
        /// </summary>
        public string Name { get; set; }
    }
}
