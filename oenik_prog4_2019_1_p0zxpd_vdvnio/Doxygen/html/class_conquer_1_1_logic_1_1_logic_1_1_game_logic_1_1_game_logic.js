var class_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_game_logic =
[
    [ "GameLogic", "class_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_game_logic.html#ad369b082bdd541f95d4e57dddd61fa24", null ],
    [ "CreateHub", "class_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_game_logic.html#a863a2621047fc8b73bf8fa9121b1d2f6", null ],
    [ "DisconnectFromHub", "class_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_game_logic.html#a1e2dd6efd6fe9b4ac60329d7005ab9c2", null ],
    [ "GetCanClick", "class_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_game_logic.html#a3d85ee5a5028dc31fb480d96e1b17d0c", null ],
    [ "GetTypeOfArea", "class_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_game_logic.html#a9684d6767eb001cc5def62463b95cfc4", null ],
    [ "OccupyOneArea", "class_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_game_logic.html#a387b30595e013cd81d3e58c56542a459", null ],
    [ "SendRoundAcknowledgement", "class_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_game_logic.html#adab3bf90a32271c49af268d0dfb40daa", null ],
    [ "SetRemotePlayer", "class_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_game_logic.html#a1b6836ff4694dd81788b70bc2f5dc3c2", null ],
    [ "CanChoose", "class_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_game_logic.html#a17d127f2d6f498d1b9538467742a3d36", null ],
    [ "HubLogic", "class_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_game_logic.html#a4ff0fd37654fbb868f2391f1ab7842a4", null ],
    [ "MapLogic", "class_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_game_logic.html#a01bc1cf6bced44ca64852a50102c124e", null ],
    [ "RemotePlayer", "class_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_game_logic.html#a88a06c5cfd78369c062c9a5d597ab33f", null ],
    [ "RoundLogic", "class_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_game_logic.html#a4d60757cd97515dc533a1b8bddb831f8", null ],
    [ "OccupiedArea", "class_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_game_logic.html#a2b3ece8af85a72fb32e127888d0c28b1", null ],
    [ "QuestionArrived", "class_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_game_logic.html#ad04dfd5f7fdbe8cabf7254f82fc5a284", null ]
];