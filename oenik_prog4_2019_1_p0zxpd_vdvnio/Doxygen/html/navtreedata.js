/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Conquer", "index.html", [
    [ "LICENSE", "md__c_1__users_sando__desktop__p_r_o_g4_oenik_prog4_2019_1_p0zxpd_vdvnio_packages__newtonsoft_8_json_812_80_82__l_i_c_e_n_s_e.html", null ],
    [ "NUnit 3.12 - May 14, 2019", "md__c_1__users_sando__desktop__p_r_o_g4_oenik_prog4_2019_1_p0zxpd_vdvnio_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html", null ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ],
        [ "Properties", "functions_prop.html", null ],
        [ "Events", "functions_evnt.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_acknowledge_model_8cs_source.html",
"class_conquer_1_1_constants_1_1_constant.html#aeb88d635bd865f489b6de90dab65bc40",
"class_conquer_1_1_logic_1_1_logic_model_1_1_game_board_1_1_logic_occupy_model.html#aa005ed478cc907278888d7392e7ce419",
"dir_171ae871acae4896501cf2bc67bf9419.html",
"namespace_conquer_1_1_client_1_1_test_1_1_score_logic_test.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';