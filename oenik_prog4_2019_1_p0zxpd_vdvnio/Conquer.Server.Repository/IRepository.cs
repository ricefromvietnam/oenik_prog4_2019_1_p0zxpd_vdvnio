﻿//-----------------------------------------------------------------------
// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Generic interface for basic CRUD database operations.
    /// </summary>
    /// <typeparam name="TEntity">Table of the database.</typeparam>
    public interface IRepository<TEntity>
        where TEntity : class
    {
        /// <summary>
        /// Generic version of database insert. Requires mapping from object to the wanted Entity.
        /// </summary>
        /// <typeparam name="T">Type of parameter.</typeparam>
        /// <param name="entity">Item that is wanted to be Inserted.</param>
        /// <returns>Inserted item.</returns>
        TEntity Insert<T>(T entity);

        /// <summary>
        /// Generic version of delete with capability of finding the object in database.
        /// </summary>
        /// <param name="entityToDelete">Object that is wanted to be deleted.</param>
        /// <returns>Deleted item.</returns>
        TEntity Delete(TEntity entityToDelete);

        /// <summary>
        /// Generic version of delete from the specified Entity.
        /// </summary>
        /// <param name="id">Primary key of the object.</param>
        /// <returns>Deleted item.</returns>
        TEntity Delete(object id);

        /// <summary>
        /// Generic version of Update function. Does not save to the database have to call Save afterwards.
        /// </summary>
        /// <typeparam name="T">Type of input that is wanted to be updated.
        /// Requires the  mapping from that object.</typeparam>
        /// <param name="entityToUpdate">Object that is wanted to be updated.</param>
        /// <param name="key">Primary key of the object that will be updated.</param>
        /// <returns>The updated/ saved item.</returns>
        TEntity Update<T>(T entityToUpdate, object key);

        /// <summary>
        /// Saves the changes to the database.
        /// </summary>
        void Save();
    }
}
