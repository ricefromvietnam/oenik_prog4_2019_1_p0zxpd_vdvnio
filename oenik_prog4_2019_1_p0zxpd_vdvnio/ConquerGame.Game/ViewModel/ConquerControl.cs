﻿//-----------------------------------------------------------------------
// <copyright file="ConquerControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Game.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Threading;
    using Conquer.Constants;
    using Conquer.Database.Model;
    using Conquer.Database.Model.MapModel;
    using Conquer.Database.Model.PlayerModel;
    using Conquer.Database.Model.RoundModel;
    using Conquer.Display;
    using Conquer.Display.Castle;
    using Conquer.Logic.Logic.GameLogic;

    /// <summary>
    /// Class to initialize the window elements.
    /// </summary>
    public class ConquerControl : FrameworkElement
    {
        /// <summary>
        /// List of players inside the room.
        /// </summary>
        private readonly IList<Player> listOfPlayers;

        /// <summary>
        /// Actual room.
        /// </summary>
        private readonly MainMenuRoomModel actualRoom;

        /// <summary>
        /// Logic reference to send acknowledgment requests to the server.
        /// </summary>
        private readonly IGameLogic gameLogic;

        /// <summary>
        /// Property to hold all the display methods of the main gameScreen.
        /// </summary>
        private ConquerGameDisplay display;

        /// <summary>
        /// Name of the player who won the last round.
        /// </summary>
        private string prevWinner = string.Empty;

        /// <summary>
        /// Timer for the animations.
        /// </summary>
        private DispatcherTimer timer;

        /// <summary>
        /// Location of the last click.
        /// </summary>
        private Point loc;

        /// <summary>
        /// Remote timer on the occupy event.
        /// </summary>
        private DispatcherTimer remoteTimer;

        /// <summary>
        /// Timer for the animation while waiting for one of the players to choose area.
        /// </summary>
        private DispatcherTimer chooseTimer;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConquerControl"/> class.
        /// </summary>
        /// <param name="room">Actual room playing in.</param>
        /// <param name="listOfPlayers">List of players that are playing.</param>
        public ConquerControl(MainMenuRoomModel room, IList<Player> listOfPlayers)
        {
            this.listOfPlayers = listOfPlayers;
            this.actualRoom = room;
            this.display = new ConquerGameDisplay(this.listOfPlayers);
            this.gameLogic = new GameLogic(room.Name, this.LocalPlayer);
            this.gameLogic.SetRemotePlayer(this.RemotePlayer);

            this.gameLogic.CreateHub();
            this.timer = new DispatcherTimer();
            this.remoteTimer = new DispatcherTimer();
            this.timer.Interval = TimeSpan.FromMilliseconds(40);
            this.remoteTimer.Interval = TimeSpan.FromMilliseconds(40);
            this.chooseTimer = new DispatcherTimer();
            this.chooseTimer.Interval = TimeSpan.FromMilliseconds(100);

            // Event subscriptions
            this.Loaded += this.ConquerControl_Loaded;
            this.CanOpen += this.ConquerControl_CanOpen;
            this.timer.Tick += this.Timer_Tick;
            this.remoteTimer.Tick += this.RemoteTimer_Tick;
            this.chooseTimer.Tick += this.ChooseTimer_Tick;
            this.gameLogic.OccupiedArea += this.GameLogic_OccupiedArea;
            this.gameLogic.QuestionArrived += this.GameLogic_QuestionArrived;
        }

        /// <summary>
        /// Event to signal to the window to close;
        /// </summary>
        public event EventHandler ControlClose;

        /// <summary>
        /// Event handler to notify ui that the fame can go on.
        /// </summary>
        private event EventHandler CanOpen;

        /// <summary>
        /// Gets the actual question.
        /// </summary>
        public PlayerRoundModel ActualQuestion { get; private set; }

        /// <summary>
        /// Gets the local player of the game.
        /// </summary>
        private Player LocalPlayer
        {
            get
            {
                return this.listOfPlayers.FirstOrDefault(player => player.PlayerRole == PlayerRole.Local);
            }
        }

        /// <summary>
        /// Gets the remote player of the game.
        /// </summary>
        private Player RemotePlayer
        {
            get
            {
                return this.listOfPlayers.FirstOrDefault(player => player.PlayerRole == PlayerRole.Remote);
            }
        }

        /// <summary>
        /// Override the basic onRender method the draw our own images.
        /// </summary>
        /// <param name="drawingContext">Context of the draw.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.display != null)
            {
                drawingContext.DrawDrawing(this.display.BuildDrawings());
            }
        }

        /// <summary>
        /// Event handler methdo for the timer tick of the choosing player animation.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Arguments for the event. (null).</param>
        private void ChooseTimer_Tick(object sender, EventArgs e)
        {
            this.display.ChangeWaitingLabelColor();
            Application.Current.Dispatcher.Invoke(new Action(() => this.InvalidateVisual()));
        }

        /// <summary>
        /// Called when the animation timer has finished so the player can start the next round.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Arguments for the event.</param>
        private void ConquerControl_CanOpen(object sender, EventArgs e)
        {
            this.OpenQuestionWindow();
        }

        /// <summary>
        /// Tick event for the castle animation.
        /// </summary>
        /// <param name="sender">SEnder of the event.</param>
        /// <param name="e">Arguments for the event.</param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            if (this.display.CanLocalCastleMoveMore(this.LocalPlayer.Name, this.loc))
            {
                this.display.MoveLocalPlayerCastle(this.LocalPlayer.Name);
            }
            else
            {
                this.timer.Stop();
                this.CanOpen?.Invoke(this, EventArgs.Empty);
            }

            this.InvalidateVisual();
        }

        /// <summary>
        /// Tick event for the castle animation.
        /// </summary>
        /// <param name="sender">SEnder of the event.</param>
        /// <param name="e">Arguments for the event.</param>
        private void RemoteTimer_Tick(object sender, EventArgs e)
        {
            if (this.display.CanLocalCastleMoveMore(this.RemotePlayer.Name, this.loc))
            {
                this.display.MoveLocalPlayerCastle(this.RemotePlayer.Name);
            }
            else
            {
                this.remoteTimer.Stop();
                this.CanOpen?.Invoke(this, EventArgs.Empty);
            }

            this.InvalidateVisual();
        }

        /// <summary>
        /// Handler method for the ui when the other player occupy an area.
        /// </summary>
        /// <param name="sender">Sender of the event. (Logic).</param>
        /// <param name="e">Field id that is captured.</param>
        private void GameLogic_OccupiedArea(object sender, AreaOccupyModel e)
        {
            AreaType type = this.gameLogic.GetTypeOfArea(e.Id);
            if (e.PreviousOwnerName == this.LocalPlayer.Name)
            {
                this.display.DecreasePointForPlayer(this.LocalPlayer, type);
            }

            if (type == AreaType.Castle)
            {
                CastleModel castle = this.display.StartCastleAnimation(
                    this.RemotePlayer.Name,
                    new Point(e.X, e.Y));
                this.loc = new Point(e.X, e.Y);
                Application.Current.Dispatcher.Invoke(new Action(() => this.remoteTimer.Start()));
            }

            this.display.IncreasePointForPlayer(e.ActualPlayer, e.Type);

            this.ColorizeField(e.Id, e.ActualPlayer);
            Application.Current.Dispatcher.Invoke(new Action(() => this.InvalidateVisual()));

            if (type != AreaType.Castle)
            {
                this.OpenQuestionWindow();
            }
        }

        /// <summary>
        /// Start the animation for the castle movement.
        /// </summary>
        /// <param name="point">Point of click.</param>
        private void StartCastleAnimation(Point point)
        {
            CastleModel model = this.display.StartCastleAnimation(this.LocalPlayer.Name, this.loc);
        }

        /// <summary>
        /// Called when the hub sends out a new question to the players.
        /// </summary>
        /// <param name="sender">Sender of the event. (Logic).</param>
        /// <param name="e">Question to answer.</param>
        private void GameLogic_QuestionArrived(object sender, PlayerRoundModel e)
        {
            if (e.Round < Constant.MaxRound)
            {
                this.OpenWindow(e);
            }
            else
            {
                MessageBox.Show(this.display.GetPlayerStatus());
                this.gameLogic.DisconnectFromHub();
                this.ControlClose?.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Handle the next round signal.
        /// </summary>
        /// <param name="question">Question model to answer.</param>
        private void OpenWindow(PlayerRoundModel question)
        {
            this.ActualQuestion = question;
            this.prevWinner = question.PrevWinnerPlayer;
            Debug.Print("WINNER" + question.PrevWinnerPlayer);
            if (this.gameLogic.RoundLogic.Round == 0 || question.PrevWinnerPlayer == string.Empty)
            {
                this.OpenQuestionWindow();
            }
            else
            {
                this.display.SetWaitingText($"Waiting for {question.PrevWinnerPlayer} to choose");
                this.chooseTimer.Start();
            }
        }

        /// <summary>
        /// Handles the window loaded event and calls the appropriate drawings.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Event parameters.</param>
        private void ConquerControl_Loaded(object sender, RoutedEventArgs e)
        {
            Window win = Window.GetWindow(this);
            if (win != null)
            {
                this.display = new ConquerGameDisplay(this.listOfPlayers);
                this.MouseDown += this.ConquerControl_MouseDown;
                this.gameLogic.SendRoundAcknowledgement(this.gameLogic.RoundLogic.Round, false);
            }

            this.InvalidateVisual();
        }

        /// <summary>
        /// Handles the click event for the mouse.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Event parameters.</param>
        private void ConquerControl_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (this.gameLogic.GetCanClick(this.prevWinner))
            {
                this.chooseTimer.Stop();
                this.display.EnsureWaitingLabelColorToWhite();
                this.InvalidateVisual();
                this.loc = e.GetPosition(this);

                // Get the id of the clicked area
                int id = this.display.GetGameFieldIdFromPoint((int)this.loc.X, (int)this.loc.Y);

                HubOccupyModel model = new HubOccupyModel()
                {
                    AreaId = id,
                    X = (int)this.loc.X,
                    Y = (int)this.loc.Y,
                };
                string prevOwner = this.gameLogic.MapLogic.GetOwnerOfTheArea(id).Name;
                if (this.gameLogic.OccupyOneArea(model) != null)
                {
                    AreaType type = this.gameLogic.GetTypeOfArea(id);

                    // If we occupied
                    if (prevOwner == this.RemotePlayer.Name)
                    {
                        this.display.DecreasePointForPlayer(this.RemotePlayer, type);
                    }

                    if (type == AreaType.Castle)
                    {
                        CastleModel castle = this.display.StartCastleAnimation(this.LocalPlayer.Name, this.loc);
                        this.timer.Start();
                    }

                    this.display.IncreasePointForPlayer(this.LocalPlayer, type);

                    this.ColorizeField(id, this.LocalPlayer);
                    this.prevWinner = this.ActualQuestion.PrevWinnerPlayer;

                    this.InvalidateVisual();
                    this.gameLogic.CanChoose = false;
                    if (type != AreaType.Castle)
                    {
                        this.OpenQuestionWindow();
                    }
                }
            }
        }

        /// <summary>
        /// Open the question window on the ui thread.
        /// </summary>
        private void OpenQuestionWindow()
        {
            Application.Current.Dispatcher.Invoke(new Action(() => this.OpenWindowOnUiThread()));
        }

        /// <summary>
        /// Open question.
        /// </summary>
        private void OpenWindowOnUiThread()
        {
            this.chooseTimer.Stop();
            this.display.EnsureWaitingLabelColorToWhite();
            this.InvalidateVisual();

            this.gameLogic.CanChoose = true;
            QuestionWindow questionWindow = new QuestionWindow(
                       this.actualRoom.Name,
                       this.ActualQuestion.Question,
                       this.gameLogic.RoundLogic.Round,
                       this.LocalPlayer);
            if (questionWindow.ShowDialog() != null)
            {
                this.chooseTimer.Start();
            }
        }

        /// <summary>
        /// Colorize one field on the map.
        /// </summary>
        /// <param name="id">Id of the area.</param>
        /// <param name="player">Player of occupy.</param>
        private void ColorizeField(int id, Player player)
        {
            this.display.ColorizeOneField(id, player);
        }
    }
}
