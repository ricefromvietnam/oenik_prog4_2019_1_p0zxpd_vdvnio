﻿//-----------------------------------------------------------------------
// <copyright file="QuestionWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Game
{
    using System;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Threading;
    using Conquer.Database.Model.PlayerModel;
    using Conquer.Database.Model.RoundModel;
    using Conquer.Game.ViewModel;

    /// <summary>
    /// Interaction logic for QuestionWindow.
    /// </summary>
    public partial class QuestionWindow : Window
    {
        /// <summary>
        /// Name of the room.
        /// </summary>
        private readonly string roomName;

        /// <summary>
        /// Actual round of the game.
        /// </summary>
        private readonly int round;

        /// <summary>
        /// View model reference.
        /// </summary>
        private QuestionModel viewModel;

        /// <summary>
        /// Timer for the window to get closed.
        /// </summary>
        private DispatcherTimer moveTimer;

        /// <summary>
        /// Initializes a new instance of the <see cref="QuestionWindow"/> class.
        /// </summary>
        public QuestionWindow()
        {
            this.InitializeComponent();
            this.viewModel = this.FindResource("VM") as QuestionModel;
            this.Loaded += this.QuestionWindow_Loaded;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QuestionWindow"/> class.
        /// </summary>
        /// <param name="roomName">Name of the room currently played.</param>
        /// <param name="question">Question to be answer.</param>
        /// <param name="round">Actual Round of the game.</param>
        /// <param name="localPlayer">Local player of the game.</param>
        public QuestionWindow(string roomName, ConquerQuestionModel question, int round, Player localPlayer)
                        : this()
        {
            this.viewModel.QuestionBase = question;
            this.roomName = roomName;
            this.Answered = false;
            this.round = round;
            this.viewModel.CreateGameLogic(roomName, localPlayer);
        }

        /// <summary>
        /// Gets a value indicating whether the player has answered for the question.
        /// </summary>
        public bool Answered { get; private set; }

        /// <summary>
        /// Loaded event handler.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Parameters for the event.</param>
        private void QuestionWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.moveTimer = new DispatcherTimer();
            this.moveTimer.Interval = TimeSpan.FromSeconds(1);
            this.moveTimer.Tick += this.Timer_Tick;
            this.moveTimer.Start();
        }

        /// <summary>
        /// Tick handler event.
        /// </summary>
        /// <param name="sender">Sender of the event (timer tick).</param>
        /// <param name="e">Parameters for the event (null).</param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            if (this.viewModel.LeftTime > 0)
            {
                this.viewModel.LeftTime--;
            }
            else
            {
                this.moveTimer.Stop();
                this.CheckAnswer(string.Empty);
                this.DialogResult = true;
            }
        }

        /// <summary>
        /// Check the answer for the question.
        /// </summary>
        /// <param name="sender">Sender of the event (UI).</param>
        /// <param name="e">Parameters for the event.</param>
        private void CheckAnswerHandler(object sender, RoutedEventArgs e)
        {
            Button pressed = sender as Button;
            string value = pressed.Content as string;
            if (!this.Answered)
            {
                this.Answered = true;
                this.CheckAnswer(value);
            }
        }

        /// <summary>
        /// Check answer for the question and send acknowledgment about it.
        /// </summary>
        /// <param name="answer">Answer for the question.</param>
        private void CheckAnswer(string answer)
        {
            bool correct = answer == this.viewModel.QuestionBase.CorrectAnswer;
            if (correct)
            {
                this.viewModel.Answer = "Correct";
            }
            else
            {
                this.viewModel.Answer = $"Not correct. The correct is {this.viewModel.QuestionBase.CorrectAnswer}";
            }

            this.viewModel.ViewGameLogic.SendRoundAcknowledgement(this.round, correct);
        }

        /// <summary>
        /// Handler method when the.
        /// </summary>
        /// <param name="sender">Sender of the event (UI).</param>
        /// <param name="e">Parameters for the event. (null).</param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!this.Answered)
            {
                this.CheckAnswer(string.Empty);
            }
        }
    }
}
