﻿//-----------------------------------------------------------------------
// <copyright file="LogicQuestion.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.LogicModel.Question
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Model to hold the question with its answers.
    /// </summary>
    public class LogicQuestion
    {
        /// <summary>
        /// Gets or sets the actual question.
        /// </summary>
        public string Question { get; set; }

        /// <summary>
        /// Gets or sets the list of options for the answer.
        /// </summary>
        public IEnumerable<string> Options { get; set; }

        /// <summary>
        /// Gets or sets the index of the right option for the question.
        /// </summary>
        public int RightAnswerIndex { get; set; }
    }
}
