﻿//-----------------------------------------------------------------------
// <copyright file="GameDisplayColorMapModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Display.Config
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Holds the properties of the heatmap id,color code.
    /// </summary>
    public class GameDisplayColorMapModel
    {
        /// <summary>
        /// Gets or sets the id of the area.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the Color code of the area.
        /// </summary>
        public RGBAModel Color { get; set; }
    }
}
