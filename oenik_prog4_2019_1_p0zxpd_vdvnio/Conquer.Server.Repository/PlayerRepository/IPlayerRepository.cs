﻿//-----------------------------------------------------------------------
// <copyright file="IPlayerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Repository.PlayerRepository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Conquer.Server.Database.Database;
    using Conquer.Server.Database.ResponseModel;

    /// <summary>
    /// Repository interface to store the player related database operations.
    /// </summary>
    public interface IPlayerRepository : IRepository<Player>
    {
        /// <summary>
        /// Get the other player inside the room.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        /// <returns>Returns the other player inside the room.</returns>
        Player GetOtherPlayerInsideTheRoom(string roomName);

        /// <summary>
        /// Check in the database for the unique player name.
        /// </summary>
        /// <param name="playerName">Name of the player.</param>
        /// <returns>Returns a list of players with that name.</returns>
        IEnumerable<Player> CheckForPlayerName(string playerName);

        /// <summary>
        /// Get the creator player from the database.
        /// </summary>
        /// <param name="playerName">Name of the player.</param>
        /// <returns>Returns the entity from the database.</returns>
        Player GetCreatorPlayer(string playerName);

        /// <summary>
        /// Get all the player.
        /// </summary>
        /// <returns>all the player.</returns>
        IEnumerable<Player> GetAllPlayer();
    }
}
