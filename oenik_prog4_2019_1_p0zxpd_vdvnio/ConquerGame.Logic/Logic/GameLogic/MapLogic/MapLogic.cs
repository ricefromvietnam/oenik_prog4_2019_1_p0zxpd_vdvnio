﻿//-----------------------------------------------------------------------
// <copyright file="MapLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.Logic.GameLogic.Map
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Conquer.Constants;
    using Conquer.Database.Model.MapModel;
    using Conquer.Database.Model.PlayerModel;

    /// <summary>
    /// Logic operations in connection with the map.
    /// </summary>
    public class MapLogic : IMapLogic
    {
        /// <summary>
        /// List storing the current status of the map.
        /// </summary>
        private readonly IList<ConquerMapArea> mapStatus;

        /// <summary>
        /// Initializes a new instance of the <see cref="MapLogic"/> class.
        /// </summary>
        public MapLogic()
        {
            this.mapStatus = new List<ConquerMapArea>();
            this.BuildMapStatus();
        }

        /// <summary>
        /// Occupy one area on the map.
        /// Update the state of the map list.
        /// </summary>
        /// <param name="areaId">Id of the area to occupy.</param>
        /// <param name="occupyingPlayer">Player who starts the occupy.</param>
        /// <returns>Returns a bool if the area was captured.</returns>
        public bool? OccupyOneArea(int areaId, Player occupyingPlayer)
        {
            ConquerMapArea area = this.mapStatus.FirstOrDefault(e => e.Id == areaId);
            if (area != null)
            {
                if (area.Type == AreaType.Castle && area.Life > 0)
                {
                    area.Life--;
                    return false;
                }
                else
                {
                    if (area.Owner.Name == occupyingPlayer.Name)
                    {
                        return null;
                    }

                    area.Type = this.GetPlayerAreaType(areaId, occupyingPlayer);
                    area.Owner = occupyingPlayer;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Get the type of area.
        /// </summary>
        /// <param name="areaId">Id of the area.</param>
        /// <returns>Returns the type of area.</returns>
        public AreaType GetTypeOfArea(int areaId)
        {
            ConquerMapArea area = this.mapStatus.FirstOrDefault(e => e.Id == areaId);
            if (area == null)
            {
                throw new ArgumentException("Invalid id was given");
            }

            return area.Type;
        }

        /// <summary>
        /// Get the owner of area.
        /// </summary>
        /// <param name="areaId">Id of the area.</param>
        /// <returns>Returns the owner of area.</returns>
        public Player GetOwnerOfTheArea(int areaId)
        {
            ConquerMapArea area = this.mapStatus.FirstOrDefault(e => e.Id == areaId);
            if (area == null)
            {
                throw new ArgumentException("Invalid id was given");
            }

            return area.Owner;
        }

        /// <summary>
        /// Occupy one area on the map.
        /// Update the state of the map list.
        /// </summary>
        /// <param name="areaId">Id of the area to occupy.</param>
        /// <param name="occupyingPlayer">Player who starts the occupy.</param>
        /// <returns>Returns the type of the occupy field.</returns>
        public AreaType GetPlayerAreaType(int areaId, Player occupyingPlayer)
        {
            ConquerMapArea area = this.mapStatus.FirstOrDefault(e => e.Owner.Name == occupyingPlayer.Name);
            AreaType type;
            if (area != null)
            {
                type = AreaType.Normal;
            }
            else
            {
                type = AreaType.Castle;
            }

            return type;
        }

        /// <summary>
        /// Build the initial state of the map.
        /// </summary>
        private void BuildMapStatus()
        {
            for (int i = 1; i <= Constant.NumOfArea; i++)
            {
                this.mapStatus.Add(new ConquerMapArea()
                {
                    Id = i,
                    Owner = new Player(),
                    Type = AreaType.Normal,
                    Life = 1,
                });
            }
        }
    }
}