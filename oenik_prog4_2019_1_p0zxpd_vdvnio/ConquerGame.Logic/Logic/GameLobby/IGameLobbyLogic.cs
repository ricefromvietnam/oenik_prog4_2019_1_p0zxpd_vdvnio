﻿//-----------------------------------------------------------------------
// <copyright file="IGameLobbyLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.Logic.GameLobby
{
    using System.Collections.Generic;
    using Conquer.Database.Model.PlayerModel;

    /// <summary>
    /// Interface to hold the game lobby logic methods.
    /// </summary>
    public interface IGameLobbyLogic
    {
        /// <summary>
        /// Add a new player to the list of players.
        /// </summary>
        /// <param name="list">List of players.</param>
        /// <param name="newPlayer">Player who has just arrived.</param>
        void AddPlayerToList(IList<Player> list, Player newPlayer);

        /// <summary>
        /// Start the actual game.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        void StartTheGame(string roomName);
    }
}