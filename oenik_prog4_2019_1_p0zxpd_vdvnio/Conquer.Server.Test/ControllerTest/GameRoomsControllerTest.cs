﻿//-----------------------------------------------------------------------
// <copyright file="GameRoomsControllerTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Test.ControllerTest
{
    using System;
    using System.Collections.Generic;
    using Conquer.Server.Controllers;
    using Conquer.Server.Database.Database;
    using Conquer.Server.Logic.MainMenuLogic;
    using Conquer.Server.Logic.MainMenuLogic.Model;
    using Conquer.Server.Test.TestUtils;
    using Microsoft.AspNetCore.Mvc;
    using NUnit.Framework;

    /// <summary>
    /// Test the logic of the game rooms controller.
    /// </summary>
    public class GameRoomsControllerTest : TestingContext<GameRoomsController>, IDisposable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameRoomsControllerTest"/> class.
        /// </summary>
        public GameRoomsControllerTest()
        {
            this.ControllerSetup();
        }

        /// <summary>
        /// Disposition of the object.
        /// </summary>
        public void Dispose()
        {
            this.FixtureConf = null;
            this.InjectedMock.Clear();
        }

        /// <summary>
        /// Test when the input of create room is null.
        /// </summary>
        [Test]
        public void CreateNewGameRoomWhenNullIsGiven()
        {
            var result = this.TestClass.CreateNewGameRoom(null);
            Assert.IsInstanceOf(typeof(NoContentResult), result);
        }

        /// <summary>
        /// Test when the input of create room is not valid and can't be created.
        /// </summary>
        [Test]
        public void CreateNewGameRoomWhenInvalidModelIsGiven()
        {
            GameRoomCategoryModel category = new GameRoomCategoryModel() { Name = string.Empty, CategoryId = new List<int>() { 1, 2, 3 } };
            var testController = this.TestClass;
            testController.ModelState.AddModelError("error", "some error");
            var result = testController.CreateNewGameRoom(category);
            Assert.IsInstanceOf(typeof(NoContentResult), result);
        }

        /// <summary>
        /// Test when the input of create room is valid and can be created.
        /// </summary>
        [Test]
        public void CreateNewGameRoomWhenValidModelIsGiven()
        {
            GameRoom newRoom = new GameRoom() { Id = "halacska", Name = "halacska", RoomId = "halacska" };
            GameRoomCategoryModel category = new GameRoomCategoryModel() { Name = "halacska", CategoryId = new List<int>() { 1, 2, 3 } };
            this.GetMockFor<IMainMenuLogic>().Setup(e => e.CreateNewGameRoom(category)).Returns(newRoom);
            var result = this.TestClass.CreateNewGameRoom(category);

            var response = result as OkObjectResult;
            Assert.NotNull(response);

            var modelValue = response.Value as GameRoom;
            Assert.NotNull(modelValue);
            Assert.IsInstanceOf<OkObjectResult>(result);
            Assert.AreEqual("halacska", modelValue.Name);
        }
    }
}
