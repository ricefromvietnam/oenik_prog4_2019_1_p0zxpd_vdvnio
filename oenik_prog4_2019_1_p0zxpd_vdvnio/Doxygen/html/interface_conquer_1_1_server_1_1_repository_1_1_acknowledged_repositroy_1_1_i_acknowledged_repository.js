var interface_conquer_1_1_server_1_1_repository_1_1_acknowledged_repositroy_1_1_i_acknowledged_repository =
[
    [ "AddAcknowledgeToRoom", "interface_conquer_1_1_server_1_1_repository_1_1_acknowledged_repositroy_1_1_i_acknowledged_repository.html#a03103b377f7eb2596b6225e783065691", null ],
    [ "AreAllPlayersAcknowledged", "interface_conquer_1_1_server_1_1_repository_1_1_acknowledged_repositroy_1_1_i_acknowledged_repository.html#a97948b69cd51c681f7f3aea0af8a17fd", null ],
    [ "GetPlayerAcknowledgements", "interface_conquer_1_1_server_1_1_repository_1_1_acknowledged_repositroy_1_1_i_acknowledged_repository.html#a12eb8d06098afc0fe559e31595a53f2d", null ],
    [ "ResetAcknowledgeForRoom", "interface_conquer_1_1_server_1_1_repository_1_1_acknowledged_repositroy_1_1_i_acknowledged_repository.html#a14629f222df328c380ae785533753313", null ]
];