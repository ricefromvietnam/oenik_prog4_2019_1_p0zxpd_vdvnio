﻿//-----------------------------------------------------------------------
// <copyright file="ICreateNewRoomLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.Logic.MainMenuLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Conquer.Database.Model.NewRoomModel;

    /// <summary>
    /// Logic interface connected to the create new room operations.
    /// </summary>
    public interface ICreateNewRoomLogic
    {
        /// <summary>
        /// Send a request to BE to get all the question categories.
        /// </summary>
        /// <param name="listToAdd">List to write the result to.</param>
        void GetAllCategories(IList<QuestionCategoryModel> listToAdd);
    }
}
