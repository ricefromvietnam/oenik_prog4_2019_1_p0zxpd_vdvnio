﻿//-----------------------------------------------------------------------
// <copyright file="QuestionAnswerOption.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.LogicModel.Question
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Model to hold the options for the questions.
    /// </summary>
    public class QuestionAnswerOption
    {
        /// <summary>
        /// Gets or sets the unique id for the answer option.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the value of the answer option.
        /// </summary>
        public int Text { get; set; }
    }
}
