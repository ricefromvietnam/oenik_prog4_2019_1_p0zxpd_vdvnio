var class_conquer_1_1_server_1_1_test_1_1_logic_test_1_1_player_logic_test =
[
    [ "PlayerLogicTest", "class_conquer_1_1_server_1_1_test_1_1_logic_test_1_1_player_logic_test.html#a9afbc34d4747683efbf0fa5e78d47521", null ],
    [ "CheckPlayerNameWhenNameIsEmpty", "class_conquer_1_1_server_1_1_test_1_1_logic_test_1_1_player_logic_test.html#a0c1dc7ea5ac7c6d19fee033cd0b0b707", null ],
    [ "CheckPlayerNameWhenNameIsNotEmptyAndExistent", "class_conquer_1_1_server_1_1_test_1_1_logic_test_1_1_player_logic_test.html#aa2df975c1af89c4b1fae149a3fa6f346", null ],
    [ "CheckPlayerNameWhenNameIsNotEmptyAndNonExistent", "class_conquer_1_1_server_1_1_test_1_1_logic_test_1_1_player_logic_test.html#a4742e6b9157d6b088b3ee2dfc041b29a", null ],
    [ "Dispose", "class_conquer_1_1_server_1_1_test_1_1_logic_test_1_1_player_logic_test.html#a93b6a3f121d2216976fbc79918ae6408", null ],
    [ "InsertNewPlayer", "class_conquer_1_1_server_1_1_test_1_1_logic_test_1_1_player_logic_test.html#af984a4244c22264ea3648e713657b3b7", null ],
    [ "JoinToRoomWhenGameRoomIdIsEmptyAndPlayerIsNull", "class_conquer_1_1_server_1_1_test_1_1_logic_test_1_1_player_logic_test.html#a36a01a87d94e786422d5da8571a26a8c", null ],
    [ "JoinToRoomWhenGameRoomIdIsNotEmptyAndPlayerIsNotNull", "class_conquer_1_1_server_1_1_test_1_1_logic_test_1_1_player_logic_test.html#a2657255e4ae66ecfe0d0d78f47e14b4f", null ],
    [ "JoinToRoomWhenGameRoomIdIsNotEmptyAndPlayerIsNull", "class_conquer_1_1_server_1_1_test_1_1_logic_test_1_1_player_logic_test.html#a8fbc5e249956686d961f556f17c0bedb", null ],
    [ "SetupMethod", "class_conquer_1_1_server_1_1_test_1_1_logic_test_1_1_player_logic_test.html#a080db710567dea5ba81418ae0c14af09", null ]
];