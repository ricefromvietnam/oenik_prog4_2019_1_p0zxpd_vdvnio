var class_conquer_1_1_logic_1_1_server_hub_1_1_server_hub_logic =
[
    [ "InitiateConnection", "class_conquer_1_1_logic_1_1_server_hub_1_1_server_hub_logic.html#af939a551865db8c4c597ac53d5f6de33", null ],
    [ "JoinRoom", "class_conquer_1_1_logic_1_1_server_hub_1_1_server_hub_logic.html#abc5cc5230621f3cde8ddec5f04be759b", null ],
    [ "OccupyHubMethod", "class_conquer_1_1_logic_1_1_server_hub_1_1_server_hub_logic.html#a5fb06c0f085908da7fdeec616b19e5cc", null ],
    [ "StopConnection", "class_conquer_1_1_logic_1_1_server_hub_1_1_server_hub_logic.html#a939399a742a7fe89ff0f08c006ffc350", null ],
    [ "NextRound", "class_conquer_1_1_logic_1_1_server_hub_1_1_server_hub_logic.html#ab151b62e285ac93d859c1a26258ce0b9", null ],
    [ "OccupyArea", "class_conquer_1_1_logic_1_1_server_hub_1_1_server_hub_logic.html#a19482bd73728ff4203c56db68245f579", null ],
    [ "PlayerJoinedRoom", "class_conquer_1_1_logic_1_1_server_hub_1_1_server_hub_logic.html#aca6165148321bae6e9eb77c8de58deaa", null ],
    [ "StartGame", "class_conquer_1_1_logic_1_1_server_hub_1_1_server_hub_logic.html#adf20710960699c05d62fa5f0cf746402", null ]
];