var interface_conquer_1_1_server_1_1_repository_1_1_player_repository_1_1_i_player_repository =
[
    [ "CheckForPlayerName", "interface_conquer_1_1_server_1_1_repository_1_1_player_repository_1_1_i_player_repository.html#a08e18f3c13ff7761775e1df208287c18", null ],
    [ "GetAllPlayer", "interface_conquer_1_1_server_1_1_repository_1_1_player_repository_1_1_i_player_repository.html#a570326906519b207e4dfb02640aa251c", null ],
    [ "GetCreatorPlayer", "interface_conquer_1_1_server_1_1_repository_1_1_player_repository_1_1_i_player_repository.html#a8ab0502efbb229a6b180df2dd0b90f1f", null ],
    [ "GetOtherPlayerInsideTheRoom", "interface_conquer_1_1_server_1_1_repository_1_1_player_repository_1_1_i_player_repository.html#aacca01d9866ebdab3afd086ce8fff3a4", null ]
];