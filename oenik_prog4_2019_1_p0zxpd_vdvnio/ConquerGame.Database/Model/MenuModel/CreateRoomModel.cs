﻿//-----------------------------------------------------------------------
// <copyright file="CreateRoomModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Database.Model.MenuModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Conquer.Database.Model.PlayerModel;

    /// <summary>
    /// Model to hold the basic properties when creating a new room. Extended with the creator player.
    /// </summary>
    public class CreateRoomModel
    {
        /// <summary>
        /// Gets or sets the Id of the new room id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the new room.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the acceptable categories for the room.
        /// </summary>
        public IEnumerable<int> CategoryId { get; set; }

        /// <summary>
        /// Gets or sets the owner of the room.
        /// </summary>
        public Player Owner { get; set; }
    }
}
