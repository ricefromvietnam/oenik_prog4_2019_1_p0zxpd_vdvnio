﻿//-----------------------------------------------------------------------
// <copyright file="CaptureModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Database.RequestModel
{
    /// <summary>
    /// Model to handle the occupy.
    /// </summary>
    public class CaptureModel
    {
        /// <summary>
        /// Gets or sets the id of the area.
        /// </summary>
        public int AreaId { get; set; }

        /// <summary>
        /// Gets or sets the X position of the click.
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Gets or sets the Y position of the click.
        /// </summary>
        public int Y { get; set; }
    }
}
