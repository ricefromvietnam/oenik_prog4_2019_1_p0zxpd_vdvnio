﻿//-----------------------------------------------------------------------
// <copyright file="GameRoom.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Database.Database
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Database model to hold important properties about game rooms.
    /// </summary>
    public partial class GameRoom
    {
        /// <summary>
        /// Gets or sets the primary Id of the game room.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the game room.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the id of the room that the player is in currently.
        /// </summary>
        public string RoomId { get; set; }
    }
}
