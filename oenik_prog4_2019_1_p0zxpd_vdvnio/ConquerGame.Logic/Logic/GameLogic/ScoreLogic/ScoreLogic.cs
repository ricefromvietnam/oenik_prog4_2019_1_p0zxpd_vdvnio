﻿//-----------------------------------------------------------------------
// <copyright file="ScoreLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.Logic.GameLogic.ScoreLogic
{
    using System;
    using Conquer.Database.Model.MapModel;
    using Conquer.Database.PlayerModel.Model;

    /// <summary>
    /// Logic to handle the score modifications.
    /// </summary>
    public class ScoreLogic : IScoreLogic
    {
        /// <summary>
        /// Increase the player points based on the type.
        /// </summary>
        /// <param name="player">PLayer to increase point.</param>
        /// <param name="area">Area type (Castle/Normal).</param>
        public void IncreasePlayerScore(GamePlayerModel player, AreaType area)
        {
            if (player == null)
            {
                throw new ArgumentException();
            }

            if (area == AreaType.Castle)
            {
                player.Point += 1000;
            }
            else
            {
                player.Point += 400;
            }
        }

        /// <summary>
        /// Decreas the score of the player.
        /// </summary>
        /// <param name="subject">Player whose point to modify.</param>
        /// <param name="type">Type of the area.</param>
        public void DecreasePlayerScore(GamePlayerModel subject, AreaType type)
        {
            if (subject == null)
            {
                throw new ArgumentException();
            }

            if (type == AreaType.Castle)
            {
                subject.Point -= 1000;
            }
            else
            {
                subject.Point -= 400;
            }
        }
    }
}
