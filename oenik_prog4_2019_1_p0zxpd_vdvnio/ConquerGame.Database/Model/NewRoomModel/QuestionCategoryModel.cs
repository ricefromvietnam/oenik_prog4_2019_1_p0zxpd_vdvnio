﻿//-----------------------------------------------------------------------
// <copyright file="QuestionCategoryModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Database.Model.NewRoomModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Model to hold the properties of the category.
    /// </summary>
    public class QuestionCategoryModel : ObservableObject
    {
        /// <summary>
        /// Id of the question category.
        /// </summary>
        private int id;

        /// <summary>
        /// Name of the category.
        /// </summary>
        private string name;

        /// <summary>
        /// Selected property for the default checkbox.
        /// </summary>
        private bool selected;

        /// <summary>
        /// Gets or sets a value indicating whether the selected item is selected.
        /// </summary>
        public bool Selected
        {
            get { return this.selected; }
            set { this.Set(ref this.selected, value); }
        }

        /// <summary>
        /// Gets or sets the primary id of the category.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets the primary id of the category.
        /// </summary>
        public int Id
        {
            get { return this.id; }
            set { this.Set(ref this.id, value); }
        }
    }
}
