﻿//-----------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Game
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using Conquer.Database.Model;
    using Conquer.Database.Model.PlayerModel;
    using Conquer.Game.ViewModel;
    using Conquer.Logic.ServerHub;

    /// <summary>
    /// Interaction logic for MainWindow.
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        /// <param name="room">Actual room currently played.</param>
        /// <param name="players">List of players that are playing.</param>
        public MainWindow(MainMenuRoomModel room, IList<Player> players)
                : this()
        {
            ConquerControl control = new ConquerControl(room, players);
            control.ControlClose += this.Control_ControlClose;
            this.mainGrid.Children.Add(control);
        }

        /// <summary>
        /// Function invoked by the game to close itself.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Parameters for the event.</param>
        private void Control_ControlClose(object sender, EventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
