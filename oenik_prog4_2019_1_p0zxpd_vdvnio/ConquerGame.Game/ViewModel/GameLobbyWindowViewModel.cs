﻿//-----------------------------------------------------------------------
// <copyright file="GameLobbyWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Game.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Data;
    using CommonServiceLocator;
    using Conquer.Database.Model;
    using Conquer.Database.Model.PlayerModel;
    using Conquer.Logic.Logic.GameLobby;
    using Conquer.Logic.ServerHub;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// View model for the game lobby.
    /// </summary>
    public class GameLobbyWindowViewModel : ViewModelBase
    {
        /// <summary>
        /// Thread locker object to be accessible from other threads.
        /// https://www.codeproject.com/Questions/874376/How-do-I-bind-to-ObservableCollection-T-which-is-b .
        /// </summary>
        private static readonly object Lock = new object();

        /// <summary>
        /// Name of the room.
        /// </summary>
        private MainMenuRoomModel actualRoom;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLobbyWindowViewModel"/> class.
        /// </summary>
        public GameLobbyWindowViewModel()
        {
            this.Players = new ObservableCollection<Player>();
            if (this.Hub == null)
            {
                this.Hub = new ServerHubLogic();
            }

            BindingOperations.EnableCollectionSynchronization(this.Players, Lock);

            if (this.IsInDesignMode)
            {
                this.Players.Add(new Player() { Name = "Joco", PlayerRole = PlayerRole.Local });
                this.Players.Add(new Player() { Name = "Feri", PlayerRole = PlayerRole.Remote });
                this.ActualRoom = new MainMenuRoomModel() { Name = "Hello_there!!", RoomId = "hi", Topics = new List<string>() { "Music", "History", "Gaming" } };
            }
        }

        /// <summary>
        /// Gets or sets the name of the room.
        /// </summary>
        public MainMenuRoomModel ActualRoom
        {
            get { return this.actualRoom; }
            set { this.Set(ref this.actualRoom, value); }
        }

        /// <summary>
        /// Gets the hub connection to the server.
        /// </summary>
        public ServerHubLogic Hub { get; private set; }

        /// <summary>
        /// Gets the list of players inside the room.
        /// </summary>
        public ObservableCollection<Player> Players { get; private set; }

        /// <summary>
        /// Gets logic injected by the IOC container.
        /// </summary>
        private IGameLobbyLogic Logic
        {
            get { return ServiceLocator.Current.GetInstance<IGameLobbyLogic>(); }
        }

        /// <summary>
        /// Create a new hub connection and handle the incoming events.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        public void CreateHubAndHandleEvent(string roomName)
        {
            this.Hub.InitiateConnection();
            this.Hub.JoinRoom(roomName);
            this.Hub.PlayerJoinedRoom += this.PlayerJoinedRoom;
        }

        /// <summary>
        /// Send a request to the logic to start the game.
        /// </summary>
        public void SendStartSignal()
        {
            this.Logic.StartTheGame(this.ActualRoom.Name);
        }

        /// <summary>
        /// Add the owner of the room to the list of players.
        /// </summary>
        /// <param name="owner">Owner of the room.</param>
        public void AddOwnerToTheRoom(Player owner)
        {
            this.Logic.AddPlayerToList(this.Players, owner);
        }

        /// <summary>
        /// Automatically called when a guest player enters the room.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Player who joined the room.</param>
        private void PlayerJoinedRoom(object sender, Player e)
        {
            if (!this.Players.Contains(e))
            {
                this.Logic.AddPlayerToList(this.Players, e);
            }
        }
    }
}
