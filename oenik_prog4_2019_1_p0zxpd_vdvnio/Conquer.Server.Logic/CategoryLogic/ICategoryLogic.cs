﻿//-----------------------------------------------------------------------
// <copyright file="ICategoryLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//----------------------------------------------------------------------
namespace Conquer.Server.Logic.CategoryLogic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Conquer.Server.Database.Database;

    /// <summary>
    /// Logic interface for the category based operations.
    /// </summary>
    public interface ICategoryLogic
    {
        /// <summary>
        /// Get all categories from the database.
        /// </summary>
        /// <returns>Returns a list of categories.</returns>
        IEnumerable<Category> GetCategories();
    }
}
