﻿//-----------------------------------------------------------------------
// <copyright file="MainMenuRoomModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Database.Model
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Class to hold the important properties of one instance of a room.
    /// </summary>
    public class MainMenuRoomModel : ObservableObject
    {
        /// <summary>
        /// Name of the room.
        /// </summary>
        private string name;

        /// <summary>
        /// Id of the room.
        /// </summary>
        private string roomId;

        /// <summary>
        /// List of available topics in the room.
        /// </summary>
        private IEnumerable<string> topics;

        /// <summary>
        /// Gets or sets the list of topics for the room.
        /// </summary>
        public IEnumerable<string> Topics
        {
            get { return this.topics; }
            set { this.Set(ref this.topics, value); }
        }

        /// <summary>
        /// Gets or sets the Name of the room.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets the unique Id of the room.
        /// </summary>
        public string RoomId
        {
            get { return this.roomId; }
            set { this.Set(ref this.roomId, value); }
        }
    }
}
