﻿//-----------------------------------------------------------------------
// <copyright file="PlayerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Logic.PlayerLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Conquer.Server.Database.Database;
    using Conquer.Server.Database.ResponseModel;
    using Conquer.Server.Repository.PlayerRepository;

    /// <summary>
    /// Logic class to store the player based operations.
    /// </summary>
    public class PlayerLogic : IPlayerLogic
    {
        /// <summary>
        /// Logic reference.
        /// </summary>
        private readonly IPlayerRepository player;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerLogic"/> class.
        /// </summary>
        /// <param name="playerRepo">Repository for the player.</param>
        public PlayerLogic(IPlayerRepository playerRepo)
        {
            this.player = playerRepo;
        }

        /// <summary>
        /// Check if the user name is taken or not.
        /// </summary>
        /// <param name="playerName">Name of the player.</param>
        /// <returns>Returns a bool model indicating the user name is available.</returns>
        public PlayerExistModel CheckPlayerName(string playerName)
        {
            PlayerExistModel exists = new PlayerExistModel();
            if (playerName != string.Empty)
            {
                List<Player> player = this.player.CheckForPlayerName(playerName).ToList();

                if (player.Count != 0)
                {
                    exists.Exists = true;
                }
            }

            return exists;
        }

        /// <summary>
        /// Insert a new player to the database.
        /// </summary>
        /// <param name="player">Player to insert.</param>
        /// <returns>Returns the inserted player.</returns>
        public Player InsertNewPlayer(Player player)
        {
            Player insertedPlayer = this.player.Insert(player);
            this.player.Save();
            return insertedPlayer;
        }

        /// <summary>
        /// Join to a game room.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        /// <param name="newPlayer">New player model.</param>
        /// <returns>Returns the other player in the room.</returns>
        public Player JoinToRoom(string roomName, Player newPlayer)
        {
            if (roomName == string.Empty || newPlayer == null)
            {
                throw new ArgumentNullException("Name or player is null");
            }

            if (newPlayer.GameRoomId != string.Empty)
            {
                newPlayer.GameRoomId = roomName;
            }

            Player otherPLayer = this.player.GetOtherPlayerInsideTheRoom(roomName);
            this.player.Update(newPlayer, newPlayer.Id);
            this.player.Save();
            return otherPLayer;
        }

        /// <summary>
        /// Return all player.
        /// </summary>
        /// <returns>all player.</returns>
        public IQueryable<Player> GetAllPlayer()
        {
            return this.player.GetAllPlayer().AsQueryable();
        }
    }
}
