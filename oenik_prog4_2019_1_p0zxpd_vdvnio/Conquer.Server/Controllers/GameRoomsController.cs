﻿//-----------------------------------------------------------------------
// <copyright file="GameRoomsController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Conquer.Server.Database.Database;
    using Conquer.Server.Database.RequestModel;
    using Conquer.Server.Logic.MainMenuLogic;
    using Conquer.Server.Logic.MainMenuLogic.Model;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.SignalR;

    /// <summary>
    /// Handles the requests about the game room.
    /// </summary>
    [Route("[controller]")]
    public class GameRoomsController : Controller
    {
        /// <summary>
        /// Main menu related logic operations.
        /// </summary>
        private readonly IMainMenuLogic logic;

        /// <summary>
        /// Reference to the game hub.
        /// </summary>
        private readonly IHubContext<GameHub> hub;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameRoomsController"/> class.
        /// </summary>
        /// <param name="menuLogic">Injected logic interface.</param>
        /// <param name="hub">Hub for the game.</param>
        public GameRoomsController(IMainMenuLogic menuLogic, IHubContext<GameHub> hub)
        {
            this.logic = menuLogic;
            this.hub = hub;
        }

        /// <summary>
        /// Path the get all the available game rooms.
        /// </summary>
        /// <returns>Returns a list of game rooms.</returns>
        [HttpGet("")]
        public IActionResult GetAllGameRoom()
        {
            return this.Ok(this.logic.GetAllGameRoom());
        }

        /// <summary>
        /// Create a new room into the database.
        /// </summary>
        /// <param name="newRoom">New room instance.</param>
        /// <returns>Returns the created entity. </returns>
        [HttpPost("")]
        public IActionResult CreateNewGameRoom([FromBody]GameRoomCategoryModel newRoom)
        {
            if (newRoom != null && this.ModelState.IsValid)
            {
                GameRoom createdRoom = this.logic.CreateNewGameRoom(newRoom);
                return this.Ok(createdRoom);
            }

            return this.NoContent();
        }

        /// <summary>
        /// Start the game for the room.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        /// <returns>Returns a hub event named start game.</returns>
        [HttpPost("{roomName}/start")]
        public IActionResult StartGame(string roomName)
        {
            this.hub.Clients.Groups(roomName).SendAsync("startGame");
            return this.Ok();
        }
    }
}