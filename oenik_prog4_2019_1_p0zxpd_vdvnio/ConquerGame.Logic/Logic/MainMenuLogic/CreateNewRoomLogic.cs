﻿//-----------------------------------------------------------------------
// <copyright file="CreateNewRoomLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.Logic.MainMenuLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Conquer.Database.Model.NewRoomModel;
    using Conquer.Logic.ConquerHttp;

    /// <summary>
    /// Logic operations to create a new room.
    /// </summary>
    public class CreateNewRoomLogic : ICreateNewRoomLogic
    {
        /// <summary>
        /// Get all categories from the BE.
        /// </summary>
        /// <param name="listToAdd">list to add to.</param>
        public void GetAllCategories(IList<QuestionCategoryModel> listToAdd)
        {
            ConquerHttpClient http = new ConquerHttpClient();

            List<QuestionCategoryModel> result = http.ConquerHttp.GetAsync("/category").Result.ReadAndCastJsonResponse<List<QuestionCategoryModel>>();
            listToAdd.Clear();
            foreach (QuestionCategoryModel category in result)
            {
                listToAdd.Add(category);
            }
        }
    }
}
