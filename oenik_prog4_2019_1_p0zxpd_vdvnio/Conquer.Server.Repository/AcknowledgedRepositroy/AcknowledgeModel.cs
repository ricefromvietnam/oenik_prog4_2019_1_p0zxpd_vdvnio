﻿//-----------------------------------------------------------------------
// <copyright file="AcknowledgeModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Repository.AcknowledgedRepositroy
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Conquer.Server.Database.Database;
    using Conquer.Server.Database.RequestModel;

    /// <summary>
    /// Model to hold the acknowledgements for one room.
    /// </summary>
    public class AcknowledgeModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AcknowledgeModel"/> class.
        /// </summary>
        public AcknowledgeModel()
        {
            this.RoomName = string.Empty;
            this.Acknowledge = new List<PlayerQuestionModel>();
        }

        /// <summary>
        /// Gets or sets the name of the room.
        /// </summary>
        public string RoomName { get; set; }

        /// <summary>
        /// Gets or sets the acknowledge signals.
        /// </summary>
        public IList<PlayerQuestionModel> Acknowledge { get; set; }
    }
}
