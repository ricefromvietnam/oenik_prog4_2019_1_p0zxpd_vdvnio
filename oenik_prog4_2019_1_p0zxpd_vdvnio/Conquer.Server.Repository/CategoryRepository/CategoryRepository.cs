﻿//-----------------------------------------------------------------------
// <copyright file="CategoryRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Repository.CategoryRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Conquer.Server.Database.Database;

    /// <summary>
    /// Category based database operations.
    /// </summary>
    public class CategoryRepository : GenericRepository<Category>, ICategoryRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CategoryRepository"/> class in order to get the result of CRUD operations on database.
        /// </summary>
        /// <param name="context">Database context which is injected to the repository.</param>
        public CategoryRepository(ConquerDatabase context)
                                        : base(context)
        {
        }

        /// <summary>
        /// Get all category from the database.
        /// </summary>
        /// <returns>Returns a list of categories.</returns>
        public IEnumerable<Category> GetAllCategory()
        {
            return from category in this.Context.Category
                   select category;
        }
    }
}
