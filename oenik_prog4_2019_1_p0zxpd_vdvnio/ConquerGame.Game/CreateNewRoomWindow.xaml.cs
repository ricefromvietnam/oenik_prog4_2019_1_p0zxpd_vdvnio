﻿//-----------------------------------------------------------------------
// <copyright file="CreateNewRoomWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using Conquer.Game.ViewModel;

    /// <summary>
    /// Interaction logic for CreateNewRoomWindow.
    /// </summary>
    public partial class CreateNewRoomWindow : Window
    {
        /// <summary>
        /// View model of the window.
        /// </summary>
        private CreateNewRoomViewModel viewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="CreateNewRoomWindow"/> class.
        /// </summary>
        public CreateNewRoomWindow()
        {
            this.InitializeComponent();
            this.viewModel = this.FindResource("VM") as CreateNewRoomViewModel;
        }

        /// <summary>
        /// Gets the view model of the window.
        /// </summary>
        public CreateNewRoomViewModel ViewModel
        {
            get
            {
                return this.viewModel;
            }
        }

        /// <summary>
        /// Called when clicking on the cancel button.
        /// </summary>
        /// <param name="sender">Sender of the event (Button).</param>
        /// <param name="e">Params for the event.</param>
        private void CancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// Called when clicking on the save button.
        /// </summary>
        /// <param name="sender">Sender of the event (Button).</param>
        /// <param name="e">Params for the event.</param>
        private void SaveClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        /// <summary>
        /// Automatically called when the window is closed.
        /// </summary>
        /// <param name="sender">Sender of the event (window).</param>
        /// <param name="e">Cancel arguments for the event.</param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(this.viewModel.RoomName))
            {
                this.viewModel.RoomName = this.viewModel.RandomString();
            }
        }
    }
}
