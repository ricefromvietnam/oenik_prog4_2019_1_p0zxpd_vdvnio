﻿//-----------------------------------------------------------------------
// <copyright file="ConquerGameDisplay.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Display
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Conquer.Constants;
    using Conquer.Database.Factory;
    using Conquer.Database.Model;
    using Conquer.Database.Model.MapModel;
    using Conquer.Database.Model.PlayerModel;
    using Conquer.Database.Model.WaitingTextModel;
    using Conquer.Database.PlayerModel.Model;
    using Conquer.Display.Castle;
    using Conquer.Display.Config;
    using Conquer.Display.Utils;
    using Conquer.Logic.Logic.GameLogic.ScoreLogic;
    using Newtonsoft.Json;

    /// <summary>
    /// Class to handle the display of the main playing game screen.
    /// </summary>
    public class ConquerGameDisplay
    {
        /// <summary>
        /// Font of the label texts.
        /// </summary>
        private readonly Typeface font = new Typeface("Arial");

        /// <summary>
        /// Logic reference to handle score based operations.
        /// </summary>
        private readonly ScoreLogic scoreLogic;

        /// <summary>
        /// Heat map image behind the background to hold the image.
        /// </summary>
        private BitmapImage heatMapImg;

        /// <summary>
        /// Main Background image to hold the background.
        /// </summary>
        private BitmapImage backgroundImage;

        /// <summary>
        /// Image to hold the castle.
        /// </summary>
        private IList<CastleModel> castleImage;

        /// <summary>
        /// List of players that are playing the game.
        /// </summary>
        private IList<GamePlayerModel> listOfPlayers;

        /// <summary>
        /// Hold the list of game Colors.
        /// </summary>
        private IEnumerable<GameDisplayColorMapModel> gameDisplayColors;

        /// <summary>
        /// Model to hold the properties of the waiting text.
        /// </summary>
        private WaitingPlayerModel waitingText;

        /// <summary>
        ///  Initializes a new instance of the <see cref="ConquerGameDisplay"/> class.
        /// </summary>
        /// <param name="playerList">List of players that are playing.</param>
        public ConquerGameDisplay(IList<Player> playerList)
        {
            this.scoreLogic = new ScoreLogic();
            this.listOfPlayers = PlayerFactory.GetPlayerList(playerList);
            this.heatMapImg = new BitmapImage(new Uri(Constant.HeatMatImg, UriKind.Relative));
            this.backgroundImage = new BitmapImage(new Uri(Constant.BackgroundImg, UriKind.Relative));
            GamePlayerModel locaPlayer = this.listOfPlayers.FirstOrDefault(e => e.Position == PlayerRole.Local);
            GamePlayerModel remotePlayer = this.listOfPlayers.FirstOrDefault(e => e.Position == PlayerRole.Remote);
            this.waitingText = new WaitingPlayerModel() { Content = string.Empty, TextColor = System.Windows.Media.Brushes.White };
            this.castleImage = new List<CastleModel>
            {
                new CastleModel()
                    {
                        Owner = new Player()
                        {
                            Name = locaPlayer.Name,
                            PlayerRole = PlayerRole.Local,
                        },
                    },
                new CastleModel()
                    {
                        Owner = new Player()
                        {
                            Name = remotePlayer.Name,
                            PlayerRole = PlayerRole.Remote,
                        },
                    },
            };
            this.LoadHeatMapColors();
        }

        /// <summary>
        /// Calculate the field id from the point where the click has happened.
        /// </summary>
        /// <param name="x">X coordinate point of the click.</param>
        /// <param name="y">Y coordinate point of the click.</param>
        /// <returns>Field id which the player clicked on.</returns>
        public int GetGameFieldIdFromPoint(int x, int y)
        {
            CroppedBitmap bmp = new CroppedBitmap(this.heatMapImg, new Int32Rect(x, y, 1, 1));
            var pixels = new byte[4];
            bmp.CopyPixels(pixels, 4, 0);
            return this.gameDisplayColors.FirstOrDefault(e => e.Color.Blue == pixels[0] && e.Color.Green == pixels[1] && e.Color.Red == pixels[2] && e.Color.Alpha == pixels[3]).Id;
        }

        /// <summary>
        /// Set color for one field.
        /// </summary>
        /// <param name="colorId">Id of the area in the color array.</param>
        /// <param name="player">Player color.</param>
        public void ColorizeOneField(int colorId, Player player)
        {
            GamePlayerModel subject = this.listOfPlayers.FirstOrDefault(e => e.Name == player.Name);
            Bitmap heatMap = this.heatMapImg.BitmapImage2Bitmap();
            Bitmap mainMap = this.backgroundImage.BitmapImage2Bitmap();
            GameDisplayColorMapModel field = this.gameDisplayColors.FirstOrDefault(e => e.Id == colorId);
            for (int y = 0; y < heatMap.Height; y++)
            {
                for (int x = 0; x < heatMap.Width; x++)
                {
                    System.Drawing.Color pixelColor = heatMap.GetPixel(x, y);
                    if (pixelColor.A == field.Color.Alpha && pixelColor.R == field.Color.Red && pixelColor.G == field.Color.Green && pixelColor.B == field.Color.Blue)
                    {
                        System.Drawing.Color myColor = System.Drawing.Color.FromArgb(
                                                                   subject.PlayerColor.Color.A,
                                                                   subject.PlayerColor.Color.R,
                                                                   subject.PlayerColor.Color.G,
                                                                   subject.PlayerColor.Color.B);

                        mainMap.SetPixel(x, y, myColor);
                    }
                }
            }

            this.backgroundImage = mainMap.BitmapToBitmapImage();
        }

        /// <summary>
        /// Change the color of the waiting label text.
        /// </summary>
        public void ChangeWaitingLabelColor()
        {
            if (this.waitingText.TextColor == System.Windows.Media.Brushes.White)
            {
                this.waitingText.TextColor = System.Windows.Media.Brushes.Red;
            }
            else
            {
                this.waitingText.TextColor = System.Windows.Media.Brushes.White;
            }
        }

        /// <summary>
        /// Change the color of the waiting label text to white.
        /// </summary>
        public void EnsureWaitingLabelColorToWhite()
        {
            this.waitingText.TextColor = System.Windows.Media.Brushes.White;
        }

        /// <summary>
        /// Set the text of the waiting label.
        /// </summary>
        /// <param name="text">Text to set for the label.</param>
        public void SetWaitingText(string text)
        {
            this.waitingText.Content = text;
        }

        /// <summary>
        /// Generates the drawing for the main image with the heatmap behind it.
        /// </summary>
        /// <returns>The actual game screen drawing.</returns>
        public Drawing BuildDrawings()
        {
            Geometry heatMap = new RectangleGeometry(new Rect(0, 0, Constant.MapSizeWidth, Constant.MapSizeHeight));
            Geometry backgroundMap = new RectangleGeometry(new Rect(0, 0, Constant.MapSizeWidth, Constant.MapSizeHeight));

            DrawingGroup dg = new DrawingGroup();

            ImageBrush ib = new ImageBrush(this.heatMapImg);
            ImageBrush background = new ImageBrush(this.backgroundImage);

            dg.Children.Add(new GeometryDrawing(ib, null, heatMap));
            dg.Children.Add(new GeometryDrawing(background, null, backgroundMap));

            int startPos = 50;

            foreach (GamePlayerModel player in this.listOfPlayers)
            {
                FormattedText text = new FormattedText($"{player.Name} : {player.Point.ToString()}", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.font, 16, player.PlayerColor);

                // Build the geometry object that represents the text.
                Geometry textGeometry = text.BuildGeometry(new System.Windows.Point(startPos + 80, 40));
                Geometry playerLabel = new RectangleGeometry(new Rect(startPos, 0, 250, 100), 10, 10);

                dg.Children.Add(new GeometryDrawing(player.PlayerColor, null, playerLabel));
                dg.Children.Add(new GeometryDrawing(System.Windows.Media.Brushes.White, null, textGeometry));
                startPos += 350;
            }

            this.AddDrawingToGroup(dg);

            FormattedText waitingTextObject = new FormattedText(this.waitingText.Content, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.font, 16, this.waitingText.TextColor);

            // Build the geometry object that represents the text.
            Geometry waitingTextGeometry = waitingTextObject.BuildGeometry(new System.Windows.Point(80, 120));
            dg.Children.Add(new GeometryDrawing(this.waitingText.TextColor, null, waitingTextGeometry));
            return dg;
        }

        /// <summary>
        /// Get the string for the end of the game indicating whether the local
        /// player won or not.
        /// </summary>
        /// <returns>Returns a string indicating the local player won or not.</returns>
        public string GetPlayerStatus()
        {
            var localPlayer = this.listOfPlayers.FirstOrDefault(e => e.Position == PlayerRole.Local);
            var remotePlayer = this.listOfPlayers.FirstOrDefault(e => e.Position == PlayerRole.Remote);
            string returnString = string.Empty;
            if (localPlayer.Point < remotePlayer.Point)
            {
                returnString = "Congratulations, You won";
            }
            else
            {
                returnString = "Soory, You loose.";
            }

            return returnString;
        }

        /// <summary>
        /// Add the castle images to the map if need.
        /// </summary>
        /// <param name="group">Drawing group to add to.</param>
        public void AddDrawingToGroup(DrawingGroup group)
        {
            foreach (CastleModel item in this.castleImage)
            {
                if (item.X != 0 && item.Y != 0)
                {
                    Geometry castleSize = new RectangleGeometry(new Rect(item.X, item.Y, Constant.CastleHeight, Constant.CastleWidth));
                    Geometry otherCastle = new RectangleGeometry(new Rect(item.X, item.Y, Constant.CastleHeight, Constant.CastleWidth));
                    ImageBrush castle1 = new ImageBrush(item.Image);
                    ImageBrush castle2 = new ImageBrush(item.Image);
                    group.Children.Add(new GeometryDrawing(castle1, null, castleSize));
                    group.Children.Add(new GeometryDrawing(castle2, null, otherCastle));
                }
            }
        }

        /// <summary>
        /// Move castle on the map.
        /// </summary>
        /// <param name="name">Name of the player.</param>
        public void MoveLocalPlayerCastle(string name)
        {
            CastleModel model = this.castleImage.FirstOrDefault(e => e.Owner.Name == name);
            model.SetPosition(model.X, model.Y + 5);
        }

        /// <summary>
        /// Move castle on the map.
        /// </summary>
        /// <param name="name">Name of the player.</param>
        public void MoveRemotePLayerCastle(string name)
        {
            CastleModel model = this.castleImage.FirstOrDefault(e => e.Owner.Name == name);
            model.SetPosition(model.X, model.Y + 10);
        }

        /// <summary>
        /// Method to decide if the local player castle can move more down or not.
        /// </summary>
        /// <param name="name">Name of the player whose castle should be moved.</param>
        /// <param name="point">Point to move there.</param>
        /// <returns>Returns a bool to indicate that the castle can move more.</returns>
        public bool CanLocalCastleMoveMore(string name, System.Windows.Point point)
        {
            CastleModel castle = this.castleImage.FirstOrDefault(e => e.Owner.Name == name);
            return castle.Y <= point.Y - Constant.CastleHeight;
        }

        /// <summary>
        /// Decrease the point of the player.
        /// </summary>
        /// <param name="playerSubject">Remote player of the game.</param>
        /// <param name="type">Type of the area to be occupied.</param>
        public void DecreasePointForPlayer(Player playerSubject, AreaType type)
        {
            GamePlayerModel subject = this.listOfPlayers.FirstOrDefault(e => e.Name == playerSubject.Name);
            this.scoreLogic.DecreasePlayerScore(subject, type);
        }

        /// <summary>
        /// Set position for the castle to start the animation.
        /// </summary>
        /// <param name="name">Name of the player whose castle should be moved.</param>ú
        /// <param name="loc">Location of the click.</param>
        /// <returns>Returns the moved castle.</returns>
        public CastleModel StartCastleAnimation(string name, System.Windows.Point loc)
        {
            CastleModel model = this.castleImage.FirstOrDefault(e => e.Owner.Name == name);
            model.SetPosition((int)loc.X - (Constant.CastleWidth / 2), 0);
            return model;
        }

        /// <summary>
        /// Remove castle from the map.
        /// </summary>
        /// <param name="castle">Castle to remove.</param>
        public void RemoveCastle(CastleModel castle)
        {
            castle.SetPosition(0, 0);
        }

        /// <summary>
        /// Increase the point for the player.
        /// </summary>
        /// <param name="player">PLayer to whom we should increase the points.</param>
        /// <param name="area">Type of the area occupied.</param>
        public void IncreasePointForPlayer(Player player, AreaType area)
        {
            GamePlayerModel subject = this.listOfPlayers.FirstOrDefault(e => e.Name == player.Name);
            this.scoreLogic.IncreasePlayerScore(subject, area);
        }

        /// <summary>
        /// Loads the configuration from the provided file.
        /// </summary>
        private void LoadHeatMapColors()
        {
            using (StreamReader r = new StreamReader("config/colorCode.json"))
            {
                string json = r.ReadToEnd();
                List<GameDisplayColorMapModel> items = JsonConvert.DeserializeObject<List<GameDisplayColorMapModel>>(json);
                this.gameDisplayColors = items;
            }
        }
    }
}
