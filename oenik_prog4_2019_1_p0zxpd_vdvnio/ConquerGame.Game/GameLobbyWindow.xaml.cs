﻿//-----------------------------------------------------------------------
// <copyright file="GameLobbyWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using Conquer.Database.Model;
    using Conquer.Database.Model.PlayerModel;
    using Conquer.Game.ViewModel;
    using Conquer.Logic.ServerHub;

    /// <summary>
    /// Interaction logic for GameLobbyWindow.
    /// </summary>
    public partial class GameLobbyWindow : Window
    {
        /// <summary>
        /// View model for the window.
        /// </summary>
        private GameLobbyWindowViewModel viewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLobbyWindow"/> class.
        /// </summary>
        public GameLobbyWindow()
        {
            this.InitializeComponent();
            this.viewModel = this.FindResource("VM") as GameLobbyWindowViewModel;
            this.viewModel.Hub.StartGame += this.Hub_StartGame;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLobbyWindow"/> class.
        /// </summary>
        /// <param name="roomModel">Name of the room.</param>
        /// <param name="owner">Owner of the room.</param>
        public GameLobbyWindow(MainMenuRoomModel roomModel, Player owner)
            : this()
        {
            this.viewModel.ActualRoom = roomModel;
            this.viewModel.AddOwnerToTheRoom(owner);
            this.viewModel.CreateHubAndHandleEvent(roomModel.Name);
        }

        /// <summary>
        /// Add the local player to the view model.
        /// </summary>
        /// <param name="otherPlayer">Player want to enter to the room.</param>
        public void AddPlayerToWindow(Player otherPlayer)
        {
            this.viewModel.AddOwnerToTheRoom(otherPlayer);
        }

        /// <summary>
        ///  Start game signal event handler method.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Arguments for the event. Empty.</param>
        private void Hub_StartGame(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.Invoke(new Action(this.OpenWindow));
        }

        /// <summary>
        /// Open the main window of the application on the ui thread.
        /// </summary>
        private void OpenWindow()
        {
            this.viewModel.Hub.StopConnection();
            MainWindow gameWindow = new MainWindow(this.viewModel.ActualRoom, this.viewModel.Players);
            gameWindow.ShowDialog();
        }

        /// <summary>
        /// Called when the start game is called.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Parameters for the event.</param>
        private void StartGameClickHandler(object sender, RoutedEventArgs e)
        {
            this.viewModel.SendStartSignal();
        }
    }
}
