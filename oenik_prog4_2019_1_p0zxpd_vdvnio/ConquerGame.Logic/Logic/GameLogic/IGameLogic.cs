﻿//-----------------------------------------------------------------------
// <copyright file="IGameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.Logic.GameLogic
{
    using System;
    using Conquer.Database.Model.MapModel;
    using Conquer.Database.Model.PlayerModel;
    using Conquer.Database.Model.RoundModel;
    using Conquer.Logic.Logic.GameLogic.Map;
    using Conquer.Logic.LogicModel.GameBoard;

    /// <summary>
    /// Interface to hold the description what a logic can do.
    /// </summary>
    public interface IGameLogic
    {
        /// <summary>
        /// Event raised when a question arrives.
        /// </summary>
        event EventHandler<PlayerRoundModel> QuestionArrived;

        /// <summary>
        /// Event raised when an area get occupied.
        /// </summary>
        event EventHandler<AreaOccupyModel> OccupiedArea;

        /// <summary>
        /// Gets Round counter logic.
        /// </summary>
        RoundCounter RoundLogic { get; }

        /// <summary>
        /// Gets or sets a value indicating whether the player can pick field.
        /// </summary>
        bool CanChoose { get; set; }

        /// <summary>
        /// Gets the logic to handle the map connected logic.
        /// </summary>
        IMapLogic MapLogic { get; }

        /// <summary>
        /// Get the type of area from the id.
        /// </summary>
        /// <param name="areaId">Id of the area.</param>
        /// <returns>Returns the type of the area.</returns>
        AreaType GetTypeOfArea(int areaId);

        /// <summary>
        /// Send a request to the server that a user want to occupy an are.
        /// </summary>
        /// <param name="field">Model of the new field.</param>
        /// <returns>Returns a bool value indicating if the occupy was a success or not.</returns>
        bool? OccupyOneArea(HubOccupyModel field);

        /// <summary>
        /// Send an acknowledgement to the server about the answer.
        /// </summary>
        /// <param name="round">Actual round of the game.</param>
        /// <param name="answerIsCorrect">Answer was correct or not.</param>
        void SendRoundAcknowledgement(int round, bool answerIsCorrect);

        /// <summary>
        /// Get if the local player can click to occupy one are.
        /// </summary>
        /// <param name="winner">Winner of the previous question.</param>
        /// <returns>Returns a bool value indicating whether the player can conquer or not.</returns>
        bool GetCanClick(string winner);

        /// <summary>
        /// Create the hub connection to the server and register for the responses.
        /// </summary>
        void CreateHub();

        /// <summary>
        /// Set the remote player of the game.
        /// </summary>
        /// <param name="otherPlayer">Other player of the game.</param>
        void SetRemotePlayer(Player otherPlayer);

        /// <summary>
        /// Disconnect the player from the game hub on close.
        /// </summary>
        void DisconnectFromHub();
    }
}
