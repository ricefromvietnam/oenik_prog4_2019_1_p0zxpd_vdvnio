﻿//-----------------------------------------------------------------------
// <copyright file="IPlayerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Logic.PlayerLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Conquer.Server.Database.Database;
    using Conquer.Server.Database.ResponseModel;

    /// <summary>
    ///  Logic interface to store the player based operations.
    /// </summary>
    public interface IPlayerLogic
    {
        /// <summary>
        /// Called from the controller when a player want to connect to a room.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        /// <param name="newPlayer">Player that want to connect to the room.</param>
        /// <returns>Returns the other player.</returns>
        Player JoinToRoom(string roomName, Player newPlayer);

        /// <summary>
        /// Called from the controller to check if the chosen username is taken or not.
        /// </summary>
        /// <param name="playerName">Name of the player.</param>
        /// <returns>Returns the list of players that has the same name.</returns>
        PlayerExistModel CheckPlayerName(string playerName);

        /// <summary>
        /// Insert new player to the database.
        /// </summary>
        /// <param name="player">Player to insert.</param>
        /// <returns>Returns the inserted player.</returns>
        Player InsertNewPlayer(Player player);

        /// <summary>
        /// return all the player.
        /// </summary>
        /// <returns>all the player.</returns>
        IQueryable<Player> GetAllPlayer();
    }
}