﻿//-----------------------------------------------------------------------
// <copyright file="MainMenuLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Conquer.Server.Test.LogicTest
{
    using System;
    using System.Collections.Generic;
    using Conquer.Server.Database.Database;
    using Conquer.Server.Logic.MainMenuLogic;
    using Conquer.Server.Logic.MainMenuLogic.Model;
    using Conquer.Server.Repository.GameRoomRepository;
    using Conquer.Server.Repository.PlayerRepository;
    using Conquer.Server.Test.TestUtils;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test class to test the main menu functions.
    /// </summary>
    public class MainMenuLogicTest : TestingContext<MainMenuLogic>, IDisposable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenuLogicTest"/> class.
        /// </summary>
        public MainMenuLogicTest()
        {
            this.ControllerSetup();
        }

        /// <summary>
        /// Dispose method for the class.
        /// </summary>
        public void Dispose()
        {
            this.FixtureConf = null;
            this.InjectedMock.Clear();
        }

        /// <summary>
        /// Setup method to clear all previous usages of mocks.
        /// </summary>
        [SetUp]
        public void SetupMethod()
        {
            this.InjectedMock.Clear();
        }

        /// <summary>
        /// Test method to test the insert mechanics of a game room when no category was selected.
        /// </summary>
        [Test]
        public void CreateNewRoomTestWhenCategoryIdIsNull()
        {
            GameRoom newRoom = new GameRoom()
            {
                Name = "Room1",
                RoomId = "Room1",
                Id = "45",
            };
            GameRoomCategoryModel input = new GameRoomCategoryModel()
            {
                CategoryId = null,
                Name = "Room1",
                Owner = new Player()
                {
                    Id = 1,
                    Name = "Alex",
                },
            };
            Player owner = new Player()
            {
                Name = "Alex",
                Id = 1,
            };

            this.GetMockFor<IGameRoomRepository>().Setup(e => e.Insert(It.IsAny<GameRoom>())).Returns(newRoom);
            this.GetMockFor<IPlayerRepository>().Setup(e => e.GetCreatorPlayer(It.IsAny<string>())).Returns(owner);

            Mock<IPlayerRepository> playerRepo = this.GetMockFor<IPlayerRepository>();
            Mock<IGameRoomRepository> menuRepo = this.GetMockFor<IGameRoomRepository>();

            GameRoom returnRoom = this.TestClass.CreateNewGameRoom(input);
            Assert.AreEqual(returnRoom.Name, input.Name);
            playerRepo.Verify(e => e.GetCreatorPlayer(It.IsAny<string>()), Times.Once);
            playerRepo.Verify(e => e.Update(owner, owner.Id), Times.Once);
            menuRepo.Verify(e => e.InsertGameCategoryConnections(It.IsAny<string>(), It.IsAny<IEnumerable<int>>()), Times.Never);
        }

        /// <summary>
        /// Test method to test the insert mechanics of a game room when category was selected.
        /// </summary>
        [Test]
        public void CreateNewRoomTestWhenCategoryIdIsNotNull()
        {
            GameRoom newRoom = new GameRoom()
            {
                Name = "Room1",
                RoomId = "Room1",
                Id = "45",
            };
            GameRoomCategoryModel input = new GameRoomCategoryModel()
            {
                CategoryId = new List<int>()
                {
                    1,
                    2,
                    3,
                },
                Name = "Room1",
                Owner = new Player()
                {
                    Id = 1,
                    Name = "Alex",
                },
            };
            Player owner = new Player()
            {
                Name = "Alex",
                Id = 1,
            };

            this.GetMockFor<IGameRoomRepository>().Setup(e => e.Insert(It.IsAny<GameRoom>())).Returns(newRoom);
            this.GetMockFor<IPlayerRepository>().Setup(e => e.GetCreatorPlayer(input.Owner.Name)).Returns(owner);

            Mock<IPlayerRepository> playerRepo = this.GetMockFor<IPlayerRepository>();
            Mock<IGameRoomRepository> menuRepo = this.GetMockFor<IGameRoomRepository>();

            GameRoom returnRoom = this.TestClass.CreateNewGameRoom(input);
            Assert.AreEqual(returnRoom.Name, input.Name);
            playerRepo.Verify(e => e.GetCreatorPlayer(It.IsAny<string>()), Times.Once);
            playerRepo.Verify(e => e.Update(owner, owner.Id), Times.Once);
            menuRepo.Verify(e => e.InsertGameCategoryConnections(It.IsAny<string>(), It.IsAny<IEnumerable<int>>()), Times.Once);
        }
    }
}
