﻿//-----------------------------------------------------------------------
// <copyright file="ApiQuestionResponse.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Database.ResponseModel
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Newtonsoft.Json;

    /// <summary>
    /// Model to hold the response of the request.
    /// </summary>
    public class ApiQuestionResponse
    {
        /// <summary>
        /// Gets or sets the response code of the response.
        /// </summary>
        [JsonProperty("response_code")]
        public int ResponseCode { get; set; }

        /// <summary>
        /// Gets or sets the list of questions arrived.
        /// </summary>
        [JsonProperty("results")]
        public IList<ApiQuestionModel> Questions { get; set; }
    }
}
