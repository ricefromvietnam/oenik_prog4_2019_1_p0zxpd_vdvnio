﻿//-----------------------------------------------------------------------
// <copyright file="IMainMenuLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Logic.MainMenuLogic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Conquer.Server.Database.Database;
    using Conquer.Server.Database.RequestModel;
    using Conquer.Server.Database.ResponseModel;
    using Conquer.Server.Logic.MainMenuLogic.Model;

    /// <summary>
    /// Interface to hold the server side BL of the main menu.
    /// </summary>
    public interface IMainMenuLogic
    {
        /// <summary>
        /// Get all the active game rooms.
        /// </summary>
        /// <returns>Returns the list of available rooms.</returns>
        IEnumerable<ConquerMainMenuRoomsModel> GetAllGameRoom();

        /// <summary>
        /// Create a new game room into the database.
        /// </summary>
        /// <param name="newRoom">New room instance.</param>
        /// <returns>Returns the created entity.</returns>
        GameRoom CreateNewGameRoom(GameRoomCategoryModel newRoom);
    }
}
