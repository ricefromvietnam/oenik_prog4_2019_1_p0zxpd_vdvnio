﻿//-----------------------------------------------------------------------
// <copyright file="PlayerController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Conquer.Server.Database.Database;
    using Conquer.Server.Logic.PlayerLogic;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.SignalR;

    /// <summary>
    /// Controller for the player based requests.
    /// </summary>
    [Route("[controller]")]
    public class PlayerController : Controller
    {
        /// <summary>
        /// Logic refence.
        /// </summary>
        private readonly IPlayerLogic player;

        /// <summary>
        /// Hub refence to notify other users.
        /// </summary>
        private readonly IHubContext<GameHub> hub;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerController"/> class.
        /// </summary>
        /// <param name="player">Player base logic.</param>
        /// <param name="hub">Hub context for the game.</param>
        public PlayerController(IPlayerLogic player, IHubContext<GameHub> hub)
        {
            this.player = player;
            this.hub = hub;
        }

        /// <summary>
        /// Method to call when a player want to join to a room.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        /// <param name="newPlayer">New player model to connect.</param>
        /// <returns>Returns the other player inside the room.</returns>
        [HttpPost("{roomname}/joinRoom")]
        public async Task<IActionResult> JoinToRoom(string roomName, [FromBody] Player newPlayer)
        {
            Player otherPlayer = this.player.JoinToRoom(roomName, newPlayer);
            await this.hub.Clients.Group(roomName).SendAsync("playerJoined", newPlayer);
            return this.Ok(otherPlayer);
        }

        /// <summary>
        /// Check if the player name is unique.
        /// </summary>
        /// <param name="playerName">Name of the player.</param>
        /// <returns>Returns a list of players who which has that name.</returns>
        [HttpGet("check")]
        public IActionResult CheckPlayerNameUnique([FromQuery] string playerName)
        {
            return this.Ok(this.player.CheckPlayerName(playerName));
        }

        /// <summary>
        /// Called on the start of the client side when the user logs in with their user name.
        /// </summary>
        /// <param name="player">Model of the user.</param>
        /// <returns>Returns the inserted player.</returns>
        [HttpPost("")]
        public IActionResult LoginWithPlayer([FromBody]Player player)
        {
            return this.Ok(this.player.InsertNewPlayer(player));
        }
    }
}