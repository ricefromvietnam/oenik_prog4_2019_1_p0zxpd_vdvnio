﻿//-----------------------------------------------------------------------
// <copyright file="PlayerExistModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Database.ResponseModel
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Model to store that the chosen player name exists or not.
    /// </summary>
    public class PlayerExistModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerExistModel"/> class.
        /// </summary>
        public PlayerExistModel()
        {
            this.Exists = false;
        }

        /// <summary>
        /// Gets or sets a value indicating whether there is a player with that name or not.
        /// </summary>
        public bool Exists { get; set; }
    }
}
