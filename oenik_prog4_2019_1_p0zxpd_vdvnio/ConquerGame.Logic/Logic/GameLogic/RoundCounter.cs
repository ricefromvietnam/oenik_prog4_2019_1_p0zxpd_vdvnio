﻿//-----------------------------------------------------------------------
// <copyright file="RoundCounter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.Logic.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Round counter helper class.
    /// </summary>
    public class RoundCounter : IRoundCounter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RoundCounter"/> class.
        /// </summary>
        public RoundCounter()
        {
            this.Round = -1;
        }

        /// <summary>
        /// Gets the round counter.
        /// </summary>
        public int Round { get; private set; }

        /// <summary>
        /// Gets the color of the player that can choose next.
        /// </summary>
        public SolidColorBrush PlayerColorPick { get; private set; }

        /// <summary>
        /// Increase the round counter.
        /// </summary>
        public void IncreaseRoundCount()
        {
            this.Round++;
        }

        /// <summary>
        /// Increase the round counter.
        /// </summary>
        public void IncreaseRoundCountAndSwapColor()
        {
            this.Round++;
            if (this.PlayerColorPick == Brushes.Red)
            {
                this.PlayerColorPick = Brushes.Black;
            }
            else
            {
                this.PlayerColorPick = Brushes.Red;
            }
        }

        /// <summary>
        /// Set the color of the next player that can choose.
        /// </summary>
        /// <param name="color">Color of the next choosing player.</param>
        public void SetColorPick(SolidColorBrush color)
        {
            this.PlayerColorPick = color;
        }
    }
}
