﻿//-----------------------------------------------------------------------
// <copyright file="AcknowledgedRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Repository.AcknowledgedRepositroy
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Text;
    using Conquer.Server.Database.Database;
    using Conquer.Server.Database.RequestModel;

    /// <summary>
    /// Repository to hold the acknowledges from the user for action.
    /// </summary>
    public class AcknowledgedRepository : IAcknowledgedRepository
    {
        /// <summary>
        /// List to hold the acknowledged requests.
        /// </summary>
        private List<AcknowledgeModel> acknowledge;

        /// <summary>
        /// Lock object to handle concurrency.
        /// </summary>
        private object listLock;

        /// <summary>
        /// Lock object to handle parallel request.
        /// </summary>
        private object removeLock;

        /// <summary>
        /// Round counter.
        /// </summary>
        private int roundCount;

        /// <summary>
        /// Initializes a new instance of the <see cref="AcknowledgedRepository"/> class.
        /// </summary>
        public AcknowledgedRepository()
        {
            this.listLock = new object();
            this.removeLock = new object();
            this.acknowledge = new List<AcknowledgeModel>();
        }

        /// <summary>
        /// Get if all players has acknowledged the pending request.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        /// <returns>Returns a bool indicating that the number of acknowledgements are equal to two.</returns>
        public bool AreAllPlayersAcknowledged(string roomName)
        {
            AcknowledgeModel actualRoom = this.acknowledge.Find(e => e.RoomName == roomName);
            if (actualRoom != null)
            {
                return actualRoom.Acknowledge.Count == 2;
            }

            return false;
        }

        /// <summary>
        /// Add an acknowledge signal to the acknowledge signal list.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        /// <param name="acknowledgePlayer">Player who sends the acknowledgement signal.</param>
        public void AddAcknowledgeToRoom(string roomName, PlayerQuestionModel acknowledgePlayer)
        {
            lock (this.listLock)
            {
                AcknowledgeModel room = this.acknowledge.Find(e => e.RoomName == roomName);

                // First round of the game no acknowledgement sent before from that room.
                if (room == null)
                {
                    this.acknowledge.Add(new AcknowledgeModel() { RoomName = roomName, Acknowledge = new List<PlayerQuestionModel>() { acknowledgePlayer } });
                }
                else
                {
                    // There were acknowledgements from that room. so just append to the list if not contains.
                    if (!room.Acknowledge.Contains(acknowledgePlayer))
                    {
                        room.Acknowledge.Add(acknowledgePlayer);
                    }
                }
            }
        }

        /// <summary>
        /// Reset the acknowledgements for the specified room.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        public void ResetAcknowledgeForRoom(string roomName)
        {
            lock (this.removeLock)
            {
                AcknowledgeModel room = this.acknowledge.Find(e => e.RoomName == roomName);
                this.acknowledge.Remove(room);
            }
        }

        /// <summary>
        /// Get all the acknowledgments for that room.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        /// <returns>Returns the room model of acknowledgements.</returns>
        public AcknowledgeModel GetPlayerAcknowledgements(string roomName)
        {
            lock (this.listLock)
            {
                return this.acknowledge.Find(e => e.RoomName == roomName);
            }
        }
    }
}
