var class_conquer_1_1_server_1_1_repository_1_1_generic_repository =
[
    [ "GenericRepository", "class_conquer_1_1_server_1_1_repository_1_1_generic_repository.html#aef7f77c302d1b198b269abcd27f1a68e", null ],
    [ "Delete", "class_conquer_1_1_server_1_1_repository_1_1_generic_repository.html#ac4adf280dc607662f9ecfe9052faee5f", null ],
    [ "Delete", "class_conquer_1_1_server_1_1_repository_1_1_generic_repository.html#aa9a9005b6d3daabff862c28c1d5ed906", null ],
    [ "GetWithExtras", "class_conquer_1_1_server_1_1_repository_1_1_generic_repository.html#a18e483aa1bf2fbd6d2c0bbc09f375ca3", null ],
    [ "Insert< T >", "class_conquer_1_1_server_1_1_repository_1_1_generic_repository.html#a8f3447335c35d44096a904d2b94a25eb", null ],
    [ "Save", "class_conquer_1_1_server_1_1_repository_1_1_generic_repository.html#a50e801a7f364719632fe3e8aa15af68e", null ],
    [ "Update< T >", "class_conquer_1_1_server_1_1_repository_1_1_generic_repository.html#ac3692cb30b15fea2b397d23dc9ff2269", null ],
    [ "Context", "class_conquer_1_1_server_1_1_repository_1_1_generic_repository.html#adb4d6bceb378f8bf628308b975154e2b", null ],
    [ "GetAll", "class_conquer_1_1_server_1_1_repository_1_1_generic_repository.html#a9d8df2d3d7db2f1079bec2e5d74f2ce2", null ]
];