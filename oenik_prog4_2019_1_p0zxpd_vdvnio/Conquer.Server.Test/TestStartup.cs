﻿//-----------------------------------------------------------------------
// <copyright file="TestStartup.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Test
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading;
    using AutoMapper;
    using Conquer.Server.Database.Database;
    using Conquer.Server.Repository;
    using Conquer.Server.Test.TestUtils;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.ChangeTracking;
    using Microsoft.EntityFrameworkCore.Diagnostics;
    using Microsoft.EntityFrameworkCore.ValueGeneration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;

    /// <summary>
    /// Startup class for the test project.
    /// Example reference can be found in GameRoomsControllerTest class.
    /// </summary>
    public class TestStartup
    {
        /// <summary>
        /// Configure the services for the test project.
        /// </summary>
        /// <param name="services">Services to configure.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(typeof(IMapper), AutomapperConfig.InitializeMapper());

            // TODO remove obsolete function.
            services.AddAutoMapper();
            services.AddSignalR();
            services.AddDbContext<ConquerDatabase>(
                    optionBuilder => optionBuilder.UseInMemoryDatabase("COnquerDatabase").ConfigureWarnings(e => e.Ignore(InMemoryEventId.TransactionIgnoredWarning)));
            services.AddMvc().AddJsonOptions(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
        }

        /// <summary>
        /// Configure for the test project.
        /// </summary>
        /// <param name="app">Application to configure.</param>
        /// <param name="env">Current environment.</param>
        /// <param name="serviceProvider">Provider for the service collection.</param>
        public void Configure(
            IApplicationBuilder app,
            IHostingEnvironment env,
            IServiceProvider serviceProvider)
        {
            app.UseSignalR(routes =>
            {
                routes.MapHub<GameHub>("/game");
            });
            app.UseMvc();
        }
    }
}
