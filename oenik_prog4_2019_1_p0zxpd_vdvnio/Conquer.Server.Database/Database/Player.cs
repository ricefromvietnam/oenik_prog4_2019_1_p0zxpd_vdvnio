﻿//-----------------------------------------------------------------------
// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Database.Database
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Database model of a player.
    /// </summary>
    public class Player
    {
        /// <summary>
        /// Gets or sets the id of the player.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the player.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the id of the room which the player is in currently.
        /// Null if not in any room.
        /// </summary>
        public string GameRoomId { get; set; }
    }
}
