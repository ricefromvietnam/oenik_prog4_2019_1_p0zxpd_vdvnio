﻿//-----------------------------------------------------------------------
// <copyright file="TestingContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Conquer.Server.Test.TestUtils
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoFixture;
    using AutoFixture.AutoMoq;
    using Moq;

    /// <summary>
    /// TestingContext for the methods.
    /// </summary>
    /// <typeparam name="T">Class to create the mock instance for.</typeparam>
    public abstract class TestingContext<T>
                    where T : class
    {
        /// <summary>
        /// Configuration object for the AutoFixture.
        /// </summary>
        private Fixture fixtureConf;

        /// <summary>
        /// Injected mock configurations.
        /// </summary>
        private Dictionary<Type, Mock> injectedMock;

        /// <summary>
        /// Gets the created test class instance with T type.
        /// </summary>
        public T TestClass => this.FixtureConf.Create<T>();

        /// <summary>
        /// Gets or sets the configuration for the fixture.
        /// </summary>
        protected Fixture FixtureConf { get => this.fixtureConf; set => this.fixtureConf = value; }

        /// <summary>
        /// Gets or sets the list of injected mock items.
        /// </summary>
        protected Dictionary<Type, Mock> InjectedMock { get => this.injectedMock; set => this.injectedMock = value; }

        /// <summary>
        /// Setup for the context creation.
        /// </summary>
        public void Setup()
        {
            this.FixtureConf = new Fixture();
            this.InjectedMock = new Dictionary<Type, Mock>();
            this.FixtureConf.Customize(new AutoMoqCustomization());
        }

        /// <summary>
        /// Setup a new controller configuration.
        /// </summary>
        public void ControllerSetup()
        {
            this.FixtureConf = new Fixture();
            this.InjectedMock = new Dictionary<Type, Mock>();
            this.FixtureConf.Customize(new AutoMoqCustomization());
        }

        /// <summary>
        /// Get mock for a class.
        /// </summary>
        /// <typeparam name="TMockType">Type to create the mock for.</typeparam>
        /// <returns>Returns a new mock instance with that type.</returns>
        public Mock<TMockType> GetMockFor<TMockType>()
                where TMockType : class
        {
            var existingMock = this.InjectedMock.FirstOrDefault(x => x.Key == typeof(TMockType));
            if (existingMock.Key == null)
            {
                var newMock = new Mock<TMockType>();
                existingMock = new KeyValuePair<Type, Mock>(typeof(TMockType), newMock);
                this.InjectedMock.Add(existingMock.Key, existingMock.Value);
                this.FixtureConf.Inject(newMock.Object);
            }

            return existingMock.Value as Mock<TMockType>;
        }
    }
}
