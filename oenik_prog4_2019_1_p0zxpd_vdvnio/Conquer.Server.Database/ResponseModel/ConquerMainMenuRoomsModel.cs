﻿//-----------------------------------------------------------------------
// <copyright file="ConquerMainMenuRoomsModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Conquer.Server.Database.ResponseModel
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Conquer.Server.Database.Database;

    /// <summary>
    /// Modified model of a game room for the main menu.
    /// </summary>
    public class ConquerMainMenuRoomsModel : GameRoom
    {
        /// <summary>
        /// Gets or sets the topics for the room.
        /// </summary>
        public IEnumerable<string> Topics { get; set; }
    }
}
