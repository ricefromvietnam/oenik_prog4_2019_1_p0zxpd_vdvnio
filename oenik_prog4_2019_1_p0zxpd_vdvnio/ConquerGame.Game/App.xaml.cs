﻿//-----------------------------------------------------------------------
// <copyright file="App.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Game
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using CommonServiceLocator;
    using Conquer.Game.ViewModel;
    using Conquer.Logic.Logic.GameLobby;
    using Conquer.Logic.Logic.MainMenuLogic;
    using Conquer.Logic.Logic.PlayerLogic;
    using Conquer.Logic.ServerHub;
    using GalaSoft.MvvmLight.Ioc;

    /// <summary>
    /// Interaction logic for App.
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            SimpleIoc.Default.Register<IMainMenuLogic, MainMenuLogic>();
            SimpleIoc.Default.Register<ICreateNewRoomLogic, CreateNewRoomLogic>();
            SimpleIoc.Default.Register<IPlayerLogic, PlayerLogic>();
            SimpleIoc.Default.Register<IGameLobbyLogic, GameLobbyLogic>();
        }
    }
}
