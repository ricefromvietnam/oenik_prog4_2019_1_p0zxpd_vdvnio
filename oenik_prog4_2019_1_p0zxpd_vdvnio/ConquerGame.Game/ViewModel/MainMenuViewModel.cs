﻿//-----------------------------------------------------------------------
// <copyright file="MainMenuViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Game.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using Conquer.Database.Model;
    using Conquer.Database.Model.PlayerModel;
    using Conquer.Logic.Logic.MainMenuLogic;
    using Conquer.Logic.Logic.PlayerLogic;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// View model of the main menu.
    /// </summary>
    public class MainMenuViewModel : ViewModelBase
    {
        /// <summary>
        /// Selected room.
        /// </summary>
        private MainMenuRoomModel selectedRoom;

        /// <summary>
        /// Actual player on  the local side.
        /// </summary>
        private Player actualPlayer;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenuViewModel"/> class.
        /// </summary>
        public MainMenuViewModel()
        {
            this.Rooms = new ObservableCollection<MainMenuRoomModel>();
            if (this.IsInDesignMode)
            {
                MainMenuRoomModel room1 = new MainMenuRoomModel() { Name = "Alpha Team" };
                MainMenuRoomModel room2 = new MainMenuRoomModel() { Name = "Beta Team" };
                this.Rooms.Add(room1);
                this.Rooms.Add(room2);
                this.actualPlayer = new Player() { Id = 1, Name = "Joska" };
            }
            else
            {
                this.Logic.GetMainMenuRooms(this.Rooms);
            }

            this.RefreshCommand = new RelayCommand(() => this.Logic.GetMainMenuRooms(this.Rooms));
        }

        /// <summary>
        /// Gets or sets the actual player.
        /// </summary>
        public Player ActualPlayer
        {
            get
            {
                return this.actualPlayer;
            }

            set
            {
                Player newPlayer = this.PlayerLogic.PlayerLogin(value);
                this.Set(ref this.actualPlayer, newPlayer);
            }
        }

        /// <summary>
        /// Gets the command to refresh room screen.
        /// </summary>
        public ICommand RefreshCommand { get; private set; }

        /// <summary>
        /// Gets the list of available rooms.
        /// </summary>
        public ObservableCollection<MainMenuRoomModel> Rooms { get; private set; }

        /// <summary>
        /// Gets player logic injected by the IOC container.
        /// </summary>
        public IPlayerLogic PlayerLogic
        {
            get { return ServiceLocator.Current.GetInstance<IPlayerLogic>(); }
        }

        /// <summary>
        /// Gets or sets the selected room for the window.
        /// </summary>
        public MainMenuRoomModel SelectedRoom
        {
            get { return this.selectedRoom; }
            set { this.Set(ref this.selectedRoom, value); }
        }

        /// <summary>
        /// Gets logic injected by the IOC container.
        /// </summary>
        public IMainMenuLogic Logic
        {
            get { return ServiceLocator.Current.GetInstance<IMainMenuLogic>(); }
        }
    }
}
