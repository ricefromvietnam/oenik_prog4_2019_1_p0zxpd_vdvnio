﻿//-----------------------------------------------------------------------
// <copyright file="LogicGameField.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.LogicModel.GameBoard
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class to hold the owner Properties of one GameField.
    /// </summary>
    public class LogicGameField
    {
        /// <summary>
        /// Gets or sets the unique ID of the field.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the new Owner userId of the particular Field.
        /// </summary>
        public int Owner { get; set; }
    }
}
