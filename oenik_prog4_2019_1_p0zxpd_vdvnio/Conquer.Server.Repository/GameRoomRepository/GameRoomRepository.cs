﻿//-----------------------------------------------------------------------
// <copyright file="GameRoomRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Repository.GameRoomRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Conquer.Server.Database.Database;
    using Conquer.Server.Database.ResponseModel;

    /// <summary>
    /// Repository operations for the game rooms.
    /// </summary>
    public class GameRoomRepository : GenericRepository<GameRoom>, IGameRoomRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameRoomRepository"/> class in order to get the result of CRUD operations on database.
        /// </summary>
        /// <param name="context">Database context which is injected to the repository.</param>
        public GameRoomRepository(ConquerDatabase context)
                                        : base(context)
        {
        }

        /// <summary>
        /// Database query to get all game rooms.
        /// </summary>
        /// <returns>List of available rooms.</returns>
        public IEnumerable<ConquerMainMenuRoomsModel> GetAllGameRooms()
        {
            return from room in this.Context.Rooms
                   join conn in this.Context.GameCategory on room.Id equals conn.RoomId
                   join category in this.Context.Category on conn.CategoryId equals category.Id
                   group new { room, category } by new { room.Id, room.Name, room.RoomId } into groupRoom
                   select new ConquerMainMenuRoomsModel()
                   {
                       Id = groupRoom.Key.Id,
                       Name = groupRoom.Key.Name,
                       RoomId = groupRoom.Key.RoomId,
                       Topics = groupRoom.Select(e => e.category.Name),
                   };
        }

        /// <summary>
        /// Get all the category id associated with the room.
        /// </summary>
        /// <param name="roomName">name of the room.</param>
        /// <returns>Returns a list of category id for that room.</returns>
        public IList<int> GetCategoryIdsForGameRoom(string roomName)
        {
            return (from room in this.Context.Rooms
                    join category in this.Context.GameCategory on room.Id equals category.RoomId
                    where room.Name == roomName
                    select category.CategoryId).ToList();
        }

        /// <summary>
        /// Create a record in the game / category connection table.
        /// </summary>
        /// <param name="roomId">Id of the room.</param>
        /// <param name="categoryId">List of category ids.</param>
        public void InsertGameCategoryConnections(string roomId, IEnumerable<int> categoryId)
        {
            foreach (int category in categoryId)
            {
                this.Context.GameCategory.Add(new GameCategory() { RoomId = roomId, CategoryId = category });
            }
        }
    }
}
