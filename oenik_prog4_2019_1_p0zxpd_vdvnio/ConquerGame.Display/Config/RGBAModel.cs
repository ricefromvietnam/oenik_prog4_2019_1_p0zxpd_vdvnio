﻿//-----------------------------------------------------------------------
// <copyright file="RGBAModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Display.Config
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class to hold the RGB values of the heatmap.
    /// </summary>
    public class RGBAModel
    {
        /// <summary>
        /// Gets or sets the Red value or RGBA.
        /// </summary>
        public int Red { get; set; }

        /// <summary>
        /// Gets or sets the Green value or RGBA.
        /// </summary>
        public int Green { get; set; }

        /// <summary>
        /// Gets or sets the Blue value or RGBA.
        /// </summary>
        public int Blue { get; set; }

        /// <summary>
        /// Gets or sets the Alpha value or RGBA.
        /// </summary>
        public int Alpha { get; set; }
    }
}
