﻿//-----------------------------------------------------------------------
// <copyright file="ApiQuestionModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Database.ResponseModel
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    /// <summary>
    /// Model to hold the important properties for one question.
    /// </summary>
    public class ApiQuestionModel
    {
        /// <summary>
        /// Gets or sets the category of the response.
        /// </summary>
        [JsonProperty("category")]
        public string Category { get; set; }

        /// <summary>
        /// Gets or sets the difficulty of a question.
        /// </summary>
        [JsonProperty("difficulty")]
        public string Difficulty { get; set; }

        /// <summary>
        /// Gets or sets the question.
        /// </summary>
        [JsonProperty("question")]
        public string Question { get; set; }

        /// <summary>
        /// Gets or sets the correct answer of a question.
        /// </summary>
        [JsonProperty("correct_answer")]
        public string CorrectAnswer { get; set; }

        /// <summary>
        /// Gets or sets the incorrect answers of a question.
        /// </summary>
        [JsonProperty("incorrect_answers")]
        public IEnumerable<string> Incorrect_Answers { get; set; }
    }
}