﻿//-----------------------------------------------------------------------
// <copyright file="ConquerMapArea.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Database.Model.MapModel
{
    using Conquer.Database.Model.PlayerModel;

    /// <summary>
    /// Model to hold the properties about one area on the map.
    /// </summary>
    public class ConquerMapArea
    {
        /// <summary>
        /// Gets or sets the id of the area.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the owner of the area.
        /// </summary>
        public Player Owner { get; set; }

        /// <summary>
        /// Gets or sets the type of the area.
        /// </summary>
        public AreaType Type { get; set; }

        /// <summary>
        /// Gets or sets the life of the area.
        /// If castle=> 3 otherwise 1.
        /// </summary>
        public int Life { get; set; }
    }
}
