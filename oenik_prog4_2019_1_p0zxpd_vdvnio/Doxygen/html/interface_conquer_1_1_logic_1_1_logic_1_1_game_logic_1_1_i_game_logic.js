var interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_i_game_logic =
[
    [ "CreateHub", "interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_i_game_logic.html#aadd3d4c5c1b2b8bf8359c0dd5b7515cf", null ],
    [ "DisconnectFromHub", "interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_i_game_logic.html#a46babcf36d29ef68d174e987dc5db50a", null ],
    [ "GetCanClick", "interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_i_game_logic.html#a44a8afbd83090ceef6317445295899a9", null ],
    [ "GetTypeOfArea", "interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_i_game_logic.html#a904bd494d9fd756a55d65b8c271317ca", null ],
    [ "OccupyOneArea", "interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_i_game_logic.html#a23d31e01015874ddd42cf900721b3245", null ],
    [ "SendRoundAcknowledgement", "interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_i_game_logic.html#a8097091baeb9a1aec9782b1ba405260f", null ],
    [ "SetRemotePlayer", "interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_i_game_logic.html#a2f8b5970ab3043201ad6089afc1912e4", null ],
    [ "CanChoose", "interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_i_game_logic.html#a561c17f71a773e42ba231b27b2ccbf2c", null ],
    [ "MapLogic", "interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_i_game_logic.html#a1300c2cb41f175c49689f6768650321a", null ],
    [ "RoundLogic", "interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_i_game_logic.html#a15cef10ae9c2ae4811774183b831dfe2", null ],
    [ "OccupiedArea", "interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_i_game_logic.html#a5d6a6a8b3649f29f4797a6c7cccf6e27", null ],
    [ "QuestionArrived", "interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_i_game_logic.html#aa5dcdfd5efd5716c30ac96c55785cbd5", null ]
];