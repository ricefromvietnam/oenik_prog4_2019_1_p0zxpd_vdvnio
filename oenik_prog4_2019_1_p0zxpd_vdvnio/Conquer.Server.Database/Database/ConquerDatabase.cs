﻿//-----------------------------------------------------------------------
// <copyright file="ConquerDatabase.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Database.Database
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// In memory database context for the game.
    /// </summary>
    public class ConquerDatabase : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConquerDatabase"/> class.
        /// </summary>
        /// <param name="context">Context of the database.</param>
        public ConquerDatabase(DbContextOptions<ConquerDatabase> context)
            : base(context)
        {
        }

        /// <summary>
        /// Gets or sets the database table of the rooms.
        /// </summary>
        public virtual DbSet<GameRoom> Rooms { get; set; }

        /// <summary>
        /// Gets or sets the database table of players.
        /// </summary>
        public virtual DbSet<Player> Players { get; set; }

        /// <summary>
        /// Gets or sets the database table of categories.
        /// </summary>
        public virtual DbSet<Category> Category { get; set; }

        /// <summary>
        /// Gets or sets the database table of game categories.
        /// </summary>
        public virtual DbSet<GameCategory> GameCategory { get; set; }
    }
}