var class_conquer_1_1_game_1_1_view_model_1_1_conquer_control =
[
    [ "ConquerControl", "class_conquer_1_1_game_1_1_view_model_1_1_conquer_control.html#ac58b5ea32e647fac38bd6880c5b5c2bf", null ],
    [ "OnRender", "class_conquer_1_1_game_1_1_view_model_1_1_conquer_control.html#abc61dc1280cd228b28f99a43d97613e2", null ],
    [ "ActualQuestion", "class_conquer_1_1_game_1_1_view_model_1_1_conquer_control.html#aba95347b468f8db70fd2803a83365e11", null ],
    [ "LocalPlayer", "class_conquer_1_1_game_1_1_view_model_1_1_conquer_control.html#a851a2ec0fa8c6f7ecb2f2122627e10a2", null ],
    [ "RemotePlayer", "class_conquer_1_1_game_1_1_view_model_1_1_conquer_control.html#a49fccf9cb159a9922eef1d888de556c4", null ],
    [ "CanOpen", "class_conquer_1_1_game_1_1_view_model_1_1_conquer_control.html#aeed05129942e3e9d1745ad2cd834f00f", null ],
    [ "ControlClose", "class_conquer_1_1_game_1_1_view_model_1_1_conquer_control.html#a72cf1c6ff27e2a8d63f7c2f39dfcf0d6", null ]
];