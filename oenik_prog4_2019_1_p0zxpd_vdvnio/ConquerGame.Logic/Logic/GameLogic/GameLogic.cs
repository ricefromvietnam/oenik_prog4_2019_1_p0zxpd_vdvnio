﻿//-----------------------------------------------------------------------
// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.Logic.GameLogic
{
    using System;
    using System.Diagnostics;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Conquer.Database.Model.MapModel;
    using Conquer.Database.Model.PlayerModel;
    using Conquer.Database.Model.RoundModel;
    using Conquer.Logic.ConquerHttp;
    using Conquer.Logic.Logic.GameLogic.Map;
    using Conquer.Logic.ServerHub;
    using Newtonsoft.Json;

    /// <summary>
    /// Client side logic to handle the core logic of the game.
    /// </summary>
    public class GameLogic : IGameLogic
    {
        /// <summary>
        /// Http helper class to handler requests to the server.
        /// </summary>
        private readonly ConquerHttpClient http;

        /// <summary>
        /// Name of the room.
        /// </summary>
        private readonly string roomName;

        /// <summary>
        /// Local player of the game.
        /// </summary>
        private readonly Player player;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        /// <param name="player">Local player of the game.</param>
        public GameLogic(string roomName, Player player)
        {
            this.http = new ConquerHttpClient();
            this.roomName = roomName;
            this.player = player;
            this.RoundLogic = new RoundCounter();
            this.MapLogic = new MapLogic();
            this.CanChoose = true;
        }

        /// <summary>
        /// Event handler when the question arrives.
        /// </summary>
        public event EventHandler<PlayerRoundModel> QuestionArrived;

        /// <summary>
        /// Event raised when an area get occupied.
        /// </summary>
        public event EventHandler<AreaOccupyModel> OccupiedArea;

        /// <summary>
        /// Gets the logic to handle the map connected logic.
        /// </summary>
        public IMapLogic MapLogic { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the player can pick field.
        /// </summary>
        public bool CanChoose { get; set; }

        /// <summary>
        /// Gets the hub connection logic for the server.
        /// </summary>
        public ServerHubLogic HubLogic { get; private set; }

        /// <summary>
        /// Gets the round counter logic.
        /// </summary>
        public RoundCounter RoundLogic { get; private set; }

        /// <summary>
        /// Gets the remote player.
        /// </summary>
        public Player RemotePlayer { get; private set; }

        /// <summary>
        /// Set the remote player of the game.
        /// </summary>
        /// <param name="otherPlayer">Other player of the game.</param>
        public void SetRemotePlayer(Player otherPlayer)
        {
            this.RemotePlayer = otherPlayer;
        }

        /// <summary>
        /// Get the type of area from the id.
        /// </summary>
        /// <param name="areaId">Id of the area.</param>
        /// <returns>Returns the type of the area.</returns>
        public AreaType GetTypeOfArea(int areaId)
        {
            return this.MapLogic.GetTypeOfArea(areaId);
        }

        /// <summary>
        /// Create and connect to the server hub.
        /// </summary>
        public void CreateHub()
        {
            if (this.HubLogic == null)
            {
                this.HubLogic = new ServerHubLogic();

                this.HubLogic.InitiateConnection();
                this.HubLogic.JoinRoom(this.roomName);
                this.HubLogic.NextRound += this.Hub_NextRound;
                this.HubLogic.OccupyArea += this.HubLogic_OccupyArea;
            }
        }

        /// <summary>
        /// Occupy one area on the map.
        /// </summary>
        /// <param name="field">Field which to occupy.</param>
        /// <returns>Bool value indicating the player can occupy the are or not.</returns>
        public bool? OccupyOneArea(HubOccupyModel field)
        {
            bool? success = this.MapLogic.OccupyOneArea(field.AreaId, this.player);
            if (success != null)
            {
                this.HubLogic.OccupyHubMethod(this.roomName, field);
            }

            return success;
        }

        /// <summary>
        /// Send an acknowledgement to the server about the answer.
        /// </summary>
        /// <param name="round">Actual round of the game.</param>
        /// <param name="answerIsCorrect">Variable to indicate the answer was correct or not.</param>
        public void SendRoundAcknowledgement(int round, bool answerIsCorrect)
        {
            PlayerQuestionModel actualPlayer = new PlayerQuestionModel()
            {
                Id = this.player.Id,
                Name = this.player.Name,
                Round = round,
                AnswerIsCorrect = answerIsCorrect,
                PlayerRole = this.player.PlayerRole,
                AnswerTime = DateTime.Now,
            };
            HttpContent acknowledgePlayer = new StringContent(JsonConvert.SerializeObject(actualPlayer), Encoding.UTF8, "application/json");
            this.http.ConquerHttp.PostAsync($"/game/{this.roomName}/acknowledge", acknowledgePlayer);
        }

        /// <summary>
        /// Get if the player can click on one area.
        /// </summary>
        /// <param name="winner">Winner of the previous question.</param>
        /// <returns>Returns a bool value indicating whether the player can conquer or not.</returns>
        public bool GetCanClick(string winner)
        {
            return this.player.Name == winner && this.CanChoose;
        }

        /// <summary>
        /// Disconnect the user from the hub.
        /// </summary>
        public void DisconnectFromHub()
        {
            this.HubLogic.StopConnection();
        }

        /// <summary>
        /// Called when getting a signal from the hub that one area was captured.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Id of the field.</param>
        private void HubLogic_OccupyArea(object sender, HubOccupyModel e)
        {
            string prevOwner = this.MapLogic.GetOwnerOfTheArea(e.AreaId).Name;
            AreaType area = this.MapLogic.GetPlayerAreaType(e.AreaId, this.RemotePlayer);
            this.MapLogic.OccupyOneArea(e.AreaId, this.RemotePlayer);
            AreaOccupyModel occupy = new AreaOccupyModel()
            {
                Id = e.AreaId,
                Type = area,
                ActualPlayer = this.RemotePlayer,
                X = e.X,
                Y = e.Y,
                PreviousOwnerName = prevOwner,
            };
            this.OccupiedArea?.Invoke(this, occupy);
        }

        /// <summary>
        /// Event handler of the hub nextRound event.
        /// </summary>
        /// <param name="sender">Sender of the event. (Hub).</param>
        /// <param name="e">player who is choosing next.</param>
        private void Hub_NextRound(object sender, PlayerRoundModel e)
        {
            this.RoundLogic.IncreaseRoundCount();
            this.QuestionArrived?.Invoke(this, e);
        }
    }
}
