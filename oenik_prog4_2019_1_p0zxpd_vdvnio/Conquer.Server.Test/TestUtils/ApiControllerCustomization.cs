﻿//-----------------------------------------------------------------------
// <copyright file="ApiControllerCustomization.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Test.TestUtils
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using AutoFixture;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Microsoft.AspNetCore.Mvc.ModelBinding.Metadata;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;

    /// <summary>
    /// Customization of the endpoint controller for test purposes.
    /// </summary>
    public class ApiControllerCustomization : ICustomization
    {
        /// <summary>
        /// Customize the injections.
        /// </summary>
        /// <param name="fixture">Fixture to inject.</param>
        public void Customize(IFixture fixture)
        {
            fixture.Inject(new ViewDataDictionary(fixture.Create<DefaultModelMetadataProvider>(), fixture.Create<ModelStateDictionary>()));
        }
    }
}
