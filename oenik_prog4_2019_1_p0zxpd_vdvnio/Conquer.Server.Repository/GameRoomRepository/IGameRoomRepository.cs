﻿//-----------------------------------------------------------------------
// <copyright file="IGameRoomRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Repository.GameRoomRepository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Conquer.Server.Database.Database;
    using Conquer.Server.Database.ResponseModel;

    /// <summary>
    /// Repository interface to access the database for operations in connection with the game room.
    /// </summary>
    public interface IGameRoomRepository : IRepository<GameRoom>
    {
        /// <summary>
        /// Get all game room from the database.
        /// </summary>
        /// <returns>Returns the list of game rooms.</returns>
        IEnumerable<ConquerMainMenuRoomsModel> GetAllGameRooms();

        /// <summary>
        /// Add records to the game/ category connection table.
        /// </summary>
        /// <param name="roomId">Id of the room.</param>
        /// <param name="categoryId">List of category id that is accepted for the room.</param>
        void InsertGameCategoryConnections(string roomId, IEnumerable<int> categoryId);

        /// <summary>
        /// Get the list of category id for the specified game room.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        /// <returns>Returns list of category id.</returns>
        IList<int> GetCategoryIdsForGameRoom(string roomName);
    }
}
