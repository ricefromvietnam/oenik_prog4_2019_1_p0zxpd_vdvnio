﻿//-----------------------------------------------------------------------
// <copyright file="LogicOccupyModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.LogicModel.GameBoard
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Model to hold the occupy model.
    /// </summary>
    public class LogicOccupyModel
    {
        /// <summary>
        /// Gets or sets the unique id associated with the room.
        /// </summary>
        public int RoomId { get; set; }

        /// <summary>
        /// Gets or sets the field that the user want to occupy.
        /// </summary>
        public LogicGameField Field { get; set; }
    }
}
