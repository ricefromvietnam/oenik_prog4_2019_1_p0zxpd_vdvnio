var interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_map_1_1_i_map_logic =
[
    [ "GetOwnerOfTheArea", "interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_map_1_1_i_map_logic.html#a81fbe275a4e94520041cf261108de54a", null ],
    [ "GetPlayerAreaType", "interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_map_1_1_i_map_logic.html#a5475e1c734cf1ebb8bf8214a8e953869", null ],
    [ "GetTypeOfArea", "interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_map_1_1_i_map_logic.html#a392f672622fbdd3d0b870e4d391f910d", null ],
    [ "OccupyOneArea", "interface_conquer_1_1_logic_1_1_logic_1_1_game_logic_1_1_map_1_1_i_map_logic.html#ac1d53833b5d9806fe0b65e028c4f851d", null ]
];