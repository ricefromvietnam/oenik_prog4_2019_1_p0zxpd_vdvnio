var indexSectionsWithContent =
{
  0: "abcdefghijlmnopqrstuvwxy",
  1: "abcdghilmpqrstw",
  2: "cx",
  3: "abcdegijlmopqrsuv",
  4: "bchmnst",
  5: "ap",
  6: "clnr",
  7: "abcdefghilmnopqrstuvxy",
  8: "cnopqs",
  9: "ln"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "properties",
  8: "events",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Properties",
  8: "Events",
  9: "Pages"
};

