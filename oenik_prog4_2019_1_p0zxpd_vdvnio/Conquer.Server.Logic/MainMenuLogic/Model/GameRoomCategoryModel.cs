﻿//-----------------------------------------------------------------------
// <copyright file="GameRoomCategoryModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Logic.MainMenuLogic.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Text;
    using Conquer.Server.Database.Database;

    /// <summary>
    /// Model to get when creating a new game room.
    /// </summary>
    public class GameRoomCategoryModel
    {
        /// <summary>
        /// Gets or sets the primary Id of the game room.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the game room.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the selected categories.
        /// </summary>
        public IEnumerable<int> CategoryId { get; set; }

        /// <summary>
        /// Gets or sets the creator of the room.
        /// </summary>
        public Player Owner { get; set; }
    }
}
