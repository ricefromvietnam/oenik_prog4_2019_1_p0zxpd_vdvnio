var interface_conquer_1_1_server_1_1_repository_1_1_game_room_repository_1_1_i_game_room_repository =
[
    [ "GetAllGameRooms", "interface_conquer_1_1_server_1_1_repository_1_1_game_room_repository_1_1_i_game_room_repository.html#a96aba3be77ad26270084e169ab82d6f2", null ],
    [ "GetCategoryIdsForGameRoom", "interface_conquer_1_1_server_1_1_repository_1_1_game_room_repository_1_1_i_game_room_repository.html#ab2b3058dc400d0c7a8ef9565f09ba1ed", null ],
    [ "InsertGameCategoryConnections", "interface_conquer_1_1_server_1_1_repository_1_1_game_room_repository_1_1_i_game_room_repository.html#aebd373f5f78fb7a1dfe8e0adad3bbe4b", null ]
];