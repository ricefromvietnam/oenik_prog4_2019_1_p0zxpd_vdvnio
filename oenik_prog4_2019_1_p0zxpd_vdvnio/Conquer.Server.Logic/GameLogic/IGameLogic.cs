﻿//-----------------------------------------------------------------------
// <copyright file="IGameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Logic.GameLogic
{
    using Conquer.Server.Database.RequestModel;
    using Conquer.Server.Database.ResponseModel;

    /// <summary>
    /// Interface to hold the necessary operations for the game.
    /// </summary>
    public interface IGameLogic
    {
        /// <summary>
        /// Get the next question for the players.
        /// </summary>
        /// <param name="roomName">Name of the room where the players are actually.</param>
        /// <returns>Returns the modified question model.</returns>
        ConquerQuestionModel GetNextQuestion(string roomName);

        /// <summary>
        /// Send an acknowledge request to the database.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        /// <param name="player">Player who is sending the acknowledgement.</param>
        /// <returns>Returns string to indicate all players inside room has acknowledged the request.</returns>
        ConquerNextRoundModel AcknowledgeRequest(string roomName, PlayerQuestionModel player);
    }
}