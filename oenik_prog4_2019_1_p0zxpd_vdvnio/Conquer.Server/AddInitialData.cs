﻿//-----------------------------------------------------------------------
// <copyright file="AddInitialData.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Conquer.Server.Database.Database;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;

    /// <summary>
    /// Class to hold the default state of the database.
    /// </summary>
    public static class AddInitialData
    {
        /// <summary>
        /// Add all the default rows to the database.
        /// </summary>
        /// <param name="serviceProvider">Service provider.</param>
        public static void AddTestData(IServiceProvider serviceProvider)
        {
            using (var context = new ConquerDatabase(
           serviceProvider.GetRequiredService<DbContextOptions<ConquerDatabase>>()))
            {
                // Look for any board games.
                if (context.Category.Any())
                {
                    return;   // Data was already seeded
                }

                List<Category> categories = new List<Category>()
            {
                new Category()
                {
                    Id = 9,
                    Name = "General Knowledge",
                },
                new Category()
                {
                    Id = 15,
                    Name = "Entertainment: Video Games",
                },
                new Category()
                {
                    Id = 20,
                    Name = "Sports",
                },
                new Category()
                {
                    Id = 23,
                    Name = "History",
                },
                new Category()
                {
                    Id = 25,
                    Name = "Art",
                },
                new Category()
                {
                    Id = 27,
                    Name = "Animals",
                },
                new Category()
                {
                    Id = 28,
                    Name = "Vehicles",
                },
            };
                context.Category.AddRange(categories);
                context.SaveChanges();
            }
        }
    }
}
