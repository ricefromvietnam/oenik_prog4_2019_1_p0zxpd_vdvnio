﻿//-----------------------------------------------------------------------
// <copyright file="GamePlayerModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Database.PlayerModel.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;
    using Conquer.Database.Model;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Player model inside the game.
    /// </summary>
    public class GamePlayerModel
    {
        /// <summary>
        /// Id of the player.
        /// </summary>
        private int id;

        /// <summary>
        /// Name of the player.
        /// </summary>
        private string name;

        /// <summary>
        /// Point of the player.
        /// </summary>
        private int point;

        /// <summary>
        /// Color of the player.
        /// </summary>
        private SolidColorBrush playerColor;

        /// <summary>
        /// Position of the player. Local/Remote player.
        /// </summary>
        private PlayerRole position;

        /// <summary>
        /// Gets or sets the position of the player. Local/Remote player.
        /// </summary>
        public PlayerRole Position
        {
            get { return this.position; }
            set { this.position = value; }
        }

        /// <summary>
        /// Gets or sets the color of the player.
        /// </summary>
        public SolidColorBrush PlayerColor
        {
            get { return this.playerColor; }
            set { this.playerColor = value; }
        }

        /// <summary>
        /// Gets or sets the point of the actual player.
        /// </summary>
        public int Point
        {
            get { return this.point; }
            set { this.point = value; }
        }

        /// <summary>
        /// Gets or sets the id of the player.
        /// </summary>
        public int Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <summary>
        /// Gets or sets the name of the player.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
    }
}
