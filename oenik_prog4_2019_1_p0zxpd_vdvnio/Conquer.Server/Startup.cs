﻿//-----------------------------------------------------------------------
// <copyright file="Startup.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server
{
    using Conquer.Server.Database.Database;
    using Conquer.Server.Logic.CategoryLogic;
    using Conquer.Server.Logic.GameLogic;
    using Conquer.Server.Logic.MainMenuLogic;
    using Conquer.Server.Logic.PlayerLogic;
    using Conquer.Server.Logic.QuestionLogic;
    using Conquer.Server.Middleware;
    using Conquer.Server.Repository.AcknowledgedRepositroy;
    using Conquer.Server.Repository.CategoryRepository;
    using Conquer.Server.Repository.GameRoomRepository;
    using Conquer.Server.Repository.PlayerRepository;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    /// <summary>
    /// Automatically called on start.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">Configuration injected.</param>
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        /// <summary>
        /// Gets the configuration to the basic properties of the server.
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">Injected Service collections.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddDbContext<ConquerDatabase>(context => { context.UseInMemoryDatabase("Conquer"); });

            // services.AddDbContext<ConquerDatabase>(options => options.UseSqlServer(this.Configuration.GetConnectionString("DefaultConnection")));
            services.AddSignalR();
            services.AddTransient<IMainMenuLogic, MainMenuLogic>();
            services.AddTransient<IGameRoomRepository, GameRoomRepository>();
            services.AddTransient<ICategoryLogic, CategoryLogic>();
            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<IPlayerLogic, PlayerLogic>();
            services.AddTransient<IGameLogic, GameLogic>();
            services.AddTransient<IPlayerRepository, PlayerRepository>();
            services.AddSingleton<IAcknowledgedRepository, AcknowledgedRepository>();
            services.AddTransient<IQuestionLogic, QuestionLogic>();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">Application builder instance.</param>
        /// <param name="env">Hosting environment (production/development).</param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<MaxConcurrentRequestsMiddleware>();
            app.UseSignalR(routes =>
            {
                routes.MapHub<GameHub>("/game");
            });
            app.UseMvc();
        }
    }
}
