﻿//-----------------------------------------------------------------------
// <copyright file="PlayeNameValidatorRule.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Game.Validator
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Controls;
    using Conquer.Database.Model.PlayerModel;
    using Conquer.Logic.ConquerHttp;

    /// <summary>
    /// Validator for the uniqueness of the player name.
    /// </summary>
    /// <remarks>More info: https://docs.microsoft.com/en-us/dotnet/framework/wpf/app-development/dialog-boxes-overview.</remarks>
    public class PlayeNameValidatorRule : ValidationRule
    {
        /// <summary>
        /// Backend path to check for validity.
        /// </summary>
        private readonly ConquerHttpClient http;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayeNameValidatorRule"/> class.
        /// </summary>
        public PlayeNameValidatorRule()
        {
            this.http = new ConquerHttpClient();
        }

        /// <summary>
        /// Called automatically from the view.
        /// </summary>
        /// <param name="value">String value written inside the textbox.</param>
        /// <param name="cultureInfo">Current culture info.</param>
        /// <returns>Returns validation result.</returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string playerName = value as string;
            if (playerName != null)
            {
                PlayerExistModel result = this.http.ConquerHttp.GetAsync($"/player/check?playerName={playerName}").Result.ReadAndCastJsonResponse<PlayerExistModel>();
                if (!result.Exists)
                {
                    return new ValidationResult(true, null);
                }

                return new ValidationResult(false, "Name must be unique");
            }

            return new ValidationResult(false, "Cannot be empty");
        }
    }
}
