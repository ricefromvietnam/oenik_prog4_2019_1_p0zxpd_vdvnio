﻿//-----------------------------------------------------------------------
// <copyright file="MaxConcurrentRequestsEnqueuer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Middleware
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Enqueue the requests if there is any under process.
    /// </summary>
    public class MaxConcurrentRequestsEnqueuer
    {
        /// <summary>
        /// List of failed tasks.
        /// </summary>
        private static readonly Task<bool> EnqueueFailedTask = Task.FromResult(false);

        /// <summary>
        /// Lock object for the parallel operation.
        /// </summary>
        private readonly object lockObject = new object();

        /// <summary>
        /// Length of the queue.
        /// </summary>
        private readonly int maxQueueLength;

        /// <summary>
        /// Value in ms that the request can spend on the server.
        /// </summary>
        private readonly int maxTimeInQueue;

        /// <summary>
        /// Queue of the current requests.
        /// </summary>
        private readonly LinkedList<TaskCompletionSource<bool>> queue = new LinkedList<TaskCompletionSource<bool>>();

        /// <summary>
        /// Initializes a new instance of the <see cref="MaxConcurrentRequestsEnqueuer"/> class.
        /// </summary>
        /// <param name="maxQueueLength">Max queue length.</param>
        /// <param name="maxTimeInQueue">Timeout for being in the queue.</param>
        public MaxConcurrentRequestsEnqueuer(int maxQueueLength, int maxTimeInQueue)
        {
            this.maxQueueLength = maxQueueLength;
            this.maxTimeInQueue = maxTimeInQueue;
        }

        /// <summary>
        /// Enqueue a request async.
        /// </summary>
        /// <param name="requestAbortedCancellationToken">Cancel token for the request.</param>
        /// <returns>Returns a bool to signal if it was successful.</returns>
        public Task<bool> EnqueueAsync(CancellationToken requestAbortedCancellationToken)
        {
            Task<bool> enqueueTask = EnqueueFailedTask;

            if (this.maxQueueLength > 0)
            {
                CancellationToken enqueueCancellationToken = this.GetEnqueueCancellationToken(requestAbortedCancellationToken);

                lock (this.lockObject)
                {
                    if (this.queue.Count < this.maxQueueLength)
                    {
                        enqueueTask = this.InternalEnqueueAsync(enqueueCancellationToken);
                    }
                }
            }

            return enqueueTask;
        }

        /// <summary>
        /// Get one request from the waiting queue.
        /// </summary>
        /// <returns>Returns a bool if it was success.</returns>
        public bool Dequeue()
        {
            bool dequeued = false;

            lock (this.lockObject)
            {
                if (this.queue.Count > 0)
                {
                    this.InternalDequeue(true);
                    dequeued = true;
                }
            }

            return dequeued;
        }

        /// <summary>
        /// Enqueue one request to the queue.
        /// </summary>
        /// <param name="enqueueCancellationToken">Cancellation token for the task.</param>
        /// <returns>Returns a bool to indicate the request was successful.</returns>
        private Task<bool> InternalEnqueueAsync(CancellationToken enqueueCancellationToken)
        {
            Task<bool> enqueueTask = EnqueueFailedTask;

            if (!enqueueCancellationToken.IsCancellationRequested)
            {
                TaskCompletionSource<bool> enqueueTaskCompletionSource = new TaskCompletionSource<bool>(TaskCreationOptions.RunContinuationsAsynchronously);

                enqueueCancellationToken.Register(this.CancelEnqueue, enqueueTaskCompletionSource);

                this.queue.AddLast(enqueueTaskCompletionSource);
                enqueueTask = enqueueTaskCompletionSource.Task;
            }

            return enqueueTask;
        }

        /// <summary>
        /// Get token for the enqueue cancel.
        /// </summary>
        /// <param name="requestAbortedCancellationToken">Token for abort.</param>
        /// <returns>Returns a token for cancel.</returns>
        private CancellationToken GetEnqueueCancellationToken(CancellationToken requestAbortedCancellationToken)
        {
            CancellationToken enqueueCancellationToken = CancellationTokenSource.CreateLinkedTokenSource(
                requestAbortedCancellationToken,
                this.GetTimeoutToken()).Token;

            return enqueueCancellationToken;
        }

        /// <summary>
        /// Get the timeout token.
        /// </summary>
        /// <returns>Returns the token for the timeout of the request.</returns>
        private CancellationToken GetTimeoutToken()
        {
            CancellationToken timeoutToken = CancellationToken.None;

            CancellationTokenSource timeoutTokenSource = new CancellationTokenSource();

            timeoutToken = timeoutTokenSource.Token;

            timeoutTokenSource.CancelAfter(this.maxTimeInQueue);

            return timeoutToken;
        }

        /// <summary>
        /// Cancel the enqueue process.
        /// </summary>
        /// <param name="state">State of the process.</param>
        private void CancelEnqueue(object state)
        {
            bool removed = false;

            TaskCompletionSource<bool> enqueueTaskCompletionSource = (TaskCompletionSource<bool>)state;
            lock (this.lockObject)
            {
                removed = this.queue.Remove(enqueueTaskCompletionSource);
            }

            if (removed)
            {
                enqueueTaskCompletionSource.SetResult(false);
            }
        }

        /// <summary>
        /// Dequeue a request.
        /// </summary>
        /// <param name="result">Result of the operation.</param>
        private void InternalDequeue(bool result)
        {
            TaskCompletionSource<bool> enqueueTaskCompletionSource = this.queue.First.Value;

            this.queue.RemoveFirst();

            enqueueTaskCompletionSource.SetResult(result);
        }
    }
}
