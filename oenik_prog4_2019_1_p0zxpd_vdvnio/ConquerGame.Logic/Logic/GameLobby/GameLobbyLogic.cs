﻿//-----------------------------------------------------------------------
// <copyright file="GameLobbyLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.Logic.GameLobby
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Conquer.Database.Model.PlayerModel;
    using Conquer.Logic.ConquerHttp;

    /// <summary>
    /// Logic for the game lobby.
    /// </summary>
    public class GameLobbyLogic : IGameLobbyLogic
    {
        /// <summary>
        /// Helper for the http requests.
        /// </summary>
        private readonly ConquerHttpClient http;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLobbyLogic"/> class.
        /// </summary>
        public GameLobbyLogic()
        {
            this.http = new ConquerHttpClient();
        }

        /// <summary>
        /// Add a player to the game lobby.
        /// </summary>
        /// <param name="list">List to add.</param>
        /// <param name="newPlayer">New player who has just arrived.</param>
        public void AddPlayerToList(IList<Player> list, Player newPlayer)
        {
            list.Add(newPlayer);
        }

        /// <summary>
        /// Send request to the server to start the game.
        /// </summary>
        /// <param name="roomName">name of the room.</param>
        public void StartTheGame(string roomName)
        {
            this.http.ConquerHttp.PostAsync($"/gamerooms/{roomName}/start", null);
        }
    }
}
