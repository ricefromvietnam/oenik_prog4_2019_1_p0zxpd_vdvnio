﻿//-----------------------------------------------------------------------
// <copyright file="ConquerQuestionModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Database.ResponseModel
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Converted model of the question for the client side.
    /// </summary>
    public class ConquerQuestionModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConquerQuestionModel"/> class.
        /// </summary>
        public ConquerQuestionModel()
        {
            this.Options = new List<string>();
        }

        /// <summary>
        /// Gets or sets the category of the question.
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// Gets or sets the difficulty of the question.
        /// </summary>
        public string Difficulty { get; set; }

        /// <summary>
        /// Gets or sets the question.
        /// </summary>
        public string Question { get; set; }

        /// <summary>
        /// Gets or sets the correct answer of the question.
        /// </summary>
        public string CorrectAnswer { get; set; }

        /// <summary>
        /// Gets or sets the options of the question.
        /// </summary>
        public IList<string> Options { get; set; }
    }
}
