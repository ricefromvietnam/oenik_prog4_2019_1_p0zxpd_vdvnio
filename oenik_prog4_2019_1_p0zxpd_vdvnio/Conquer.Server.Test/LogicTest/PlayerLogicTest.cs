﻿//-----------------------------------------------------------------------
// <copyright file="PlayerLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Test.LogicTest
{
    using System;
    using System.Collections.Generic;
    using Conquer.Server.Database.Database;
    using Conquer.Server.Database.ResponseModel;
    using Conquer.Server.Logic.PlayerLogic;
    using Conquer.Server.Repository.PlayerRepository;
    using Conquer.Server.Test.TestUtils;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test for the player logic.
    /// </summary>
    public class PlayerLogicTest : TestingContext<PlayerLogic>, IDisposable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerLogicTest"/> class.
        /// </summary>
        public PlayerLogicTest()
        {
            this.ControllerSetup();
        }

        /// <summary>
        /// Setup method to clear all previous usages of mocks.
        /// </summary>
        [SetUp]
        public void SetupMethod()
        {
            this.InjectedMock.Clear();
        }

        /// <summary>
        /// Dispose method of the class.
        /// </summary>
        public void Dispose()
        {
            this.FixtureConf = null;
            this.InjectedMock.Clear();
        }

        /// <summary>
        /// Check the player name test when player name is an empty string.
        /// </summary>
        [Test]
        public void CheckPlayerNameWhenNameIsEmpty()
        {
            PlayerExistModel model = this.TestClass.CheckPlayerName(string.Empty);

            Mock<IPlayerRepository> player = this.GetMockFor<IPlayerRepository>();
            Assert.AreEqual(model.Exists, false);
            player.Verify(e => e.CheckForPlayerName(It.IsAny<string>()), Times.Never);
        }

        /// <summary>
        /// Check the player name test when player name is not an empty string and non existent name.
        /// </summary>
        [Test]
        public void CheckPlayerNameWhenNameIsNotEmptyAndNonExistent()
        {
            string name = "Alex";

            Mock<IPlayerRepository> player = this.GetMockFor<IPlayerRepository>();
            player.Setup(e => e.CheckForPlayerName(name)).Returns(new List<Player>());

            PlayerExistModel model = this.TestClass.CheckPlayerName(name);
            Assert.AreEqual(false, model.Exists);

            player.Verify(e => e.CheckForPlayerName(It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// Check the player name test when player name is not an empty string and existent name.
        /// </summary>
        [Test]
        public void CheckPlayerNameWhenNameIsNotEmptyAndExistent()
        {
            string name = "Alex";

            Mock<IPlayerRepository> player = this.GetMockFor<IPlayerRepository>();
            player.Setup(e => e.CheckForPlayerName(name)).Returns(new List<Player>() { new Player() });

            PlayerExistModel model = this.TestClass.CheckPlayerName(name);

            Assert.AreEqual(true, model.Exists);
            player.Verify(e => e.CheckForPlayerName(It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// Insert new player test.
        /// </summary>
        [Test]
        public void InsertNewPlayer()
        {
            Player newPlayer = new Player()
            {
                Name = "Alex",
            };

            Player returnPlayer = new Player()
            {
                Id = 1,
                Name = "Alex",
            };

            Mock<IPlayerRepository> player = this.GetMockFor<IPlayerRepository>();
            player.Setup(e => e.Insert(It.IsAny<Player>())).Returns(returnPlayer);

            Player model = this.TestClass.InsertNewPlayer(newPlayer);

            Assert.AreEqual(returnPlayer.Name, model.Name);
            player.Verify(e => e.Insert(It.IsAny<Player>()), Times.Once);
            player.Verify(e => e.Save(), Times.Once);
        }

        /// <summary>
        /// Join to the room test when input is empty.
        /// </summary>
        [Test]
        public void JoinToRoomWhenGameRoomIdIsEmptyAndPlayerIsNull()
        {
            Mock<IPlayerRepository> player = this.GetMockFor<IPlayerRepository>();

            Assert.Throws<ArgumentNullException>(() => this.TestClass.JoinToRoom(string.Empty, null));
            player.Verify(e => e.GetOtherPlayerInsideTheRoom(It.IsAny<string>()), Times.Never);
            player.Verify(e => e.Save(), Times.Never);
        }

        /// <summary>
        /// Join to the room test when input is incorrect.
        /// </summary>
        [Test]
        public void JoinToRoomWhenGameRoomIdIsNotEmptyAndPlayerIsNull()
        {
            Mock<IPlayerRepository> player = this.GetMockFor<IPlayerRepository>();

            Assert.Throws<ArgumentNullException>(() => this.TestClass.JoinToRoom("Alex", null));
            player.Verify(e => e.GetOtherPlayerInsideTheRoom(It.IsAny<string>()), Times.Never);
            player.Verify(e => e.Save(), Times.Never);
        }

        /// <summary>
        /// Join to the room test when player is not null but name is empty.
        /// </summary>
        [Test]
        public void JoinToRoomWhenGameRoomIdIsNotEmptyAndPlayerIsNotNull()
        {
            Mock<IPlayerRepository> player = this.GetMockFor<IPlayerRepository>();

            Assert.Throws<ArgumentNullException>(() => this.TestClass.JoinToRoom(string.Empty, new Player()));
            player.Verify(e => e.GetOtherPlayerInsideTheRoom(It.IsAny<string>()), Times.Never);
            player.Verify(e => e.Save(), Times.Never);
        }
    }
}
