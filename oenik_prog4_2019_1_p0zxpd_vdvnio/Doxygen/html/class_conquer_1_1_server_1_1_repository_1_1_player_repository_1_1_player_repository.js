var class_conquer_1_1_server_1_1_repository_1_1_player_repository_1_1_player_repository =
[
    [ "PlayerRepository", "class_conquer_1_1_server_1_1_repository_1_1_player_repository_1_1_player_repository.html#abd2a322e06f3e9dc794fe8fa10abc188", null ],
    [ "CheckForPlayerName", "class_conquer_1_1_server_1_1_repository_1_1_player_repository_1_1_player_repository.html#a08b6410bf3c8c7dd730c01c076be3e4f", null ],
    [ "GetAllPlayer", "class_conquer_1_1_server_1_1_repository_1_1_player_repository_1_1_player_repository.html#ac4981e3b211af714a01731dd66a1c015", null ],
    [ "GetCreatorPlayer", "class_conquer_1_1_server_1_1_repository_1_1_player_repository_1_1_player_repository.html#a9fe352959d71b29defa64047d217b269", null ],
    [ "GetOtherPlayerInsideTheRoom", "class_conquer_1_1_server_1_1_repository_1_1_player_repository_1_1_player_repository.html#a246f482d48a8bf2fd58cf8906b8dece2", null ]
];