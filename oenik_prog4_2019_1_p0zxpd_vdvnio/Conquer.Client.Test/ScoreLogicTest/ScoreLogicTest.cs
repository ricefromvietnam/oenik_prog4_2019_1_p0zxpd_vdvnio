﻿//-----------------------------------------------------------------------
// <copyright file="ScoreLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Client.Test.ScoreLogicTest
{
    using System;
    using Conquer.Database.PlayerModel.Model;
    using Conquer.Logic.Logic.GameLogic.ScoreLogic;
    using Conquer.Server.Test.TestUtils;
    using NUnit.Framework;

    /// <summary>
    /// Test for the score based logic.
    /// </summary>
    public class ScoreLogicTest : TestingContext<ScoreLogic>, IDisposable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ScoreLogicTest"/> class.
        /// </summary>
        public ScoreLogicTest()
        {
            this.ControllerSetup();
        }

        /// <summary>
        /// Dispose the objects.
        /// </summary>
        public void Dispose()
        {
            this.FixtureConf = null;
            this.InjectedMock.Clear();
        }

        /// <summary>
        /// Clear all the used mocks from the list for a clean start.
        /// </summary>
        [SetUp]
        public void SetupMethod()
        {
            this.InjectedMock.Clear();
        }

        /// <summary>
        /// Test increase if the specified player is null.
        /// </summary>
        [Test]
        public void IncreasePlayerScoreTestWhenPlayerIsNull()
        {
            Assert.Throws<ArgumentException>(() => this.TestClass.IncreasePlayerScore(null, Database.Model.MapModel.AreaType.Castle));
        }

        /// <summary>
        /// Test increase if the specified player is not null and want to occupy a castle.
        /// </summary>
        [Test]
        public void IncreasePlayerScoreTestWhenPlayerIsNotNullAndOccupyCastle()
        {
            this.TestClass.IncreasePlayerScore(new GamePlayerModel(), Database.Model.MapModel.AreaType.Castle);
        }

        /// <summary>
        /// Test increase if the specified player is not null and want to occupy a normal area.
        /// </summary>
        [Test]
        public void IncreasePlayerScoreTestWhenPlayerIsNotNullAndOccupyNormalArea()
        {
            this.TestClass.IncreasePlayerScore(new GamePlayerModel(), Database.Model.MapModel.AreaType.Normal);
        }
    }
}
