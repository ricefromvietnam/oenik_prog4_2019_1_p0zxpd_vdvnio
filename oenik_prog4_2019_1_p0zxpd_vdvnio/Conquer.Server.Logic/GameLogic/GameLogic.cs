﻿//-----------------------------------------------------------------------
// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Logic.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Web;
    using Conquer.Server.Database.RequestModel;
    using Conquer.Server.Database.ResponseModel;
    using Conquer.Server.Logic.CollectionExtensions;
    using Conquer.Server.Logic.QuestionLogic;
    using Conquer.Server.Repository.AcknowledgedRepositroy;
    using Conquer.Server.Repository.GameRoomRepository;
    using Newtonsoft.Json;

    /// <summary>
    /// Logic for the main operations of the game.
    /// </summary>
    public class GameLogic : IGameLogic
    {
        /// <summary>
        /// Random number to choose a random category for the room.
        /// </summary>
        private static readonly Random RandomNumber = new Random();

        /// <summary>
        /// Repository to store the database operations of the game room.
        /// </summary>
        private readonly IGameRoomRepository gameRoomRepository;

        /// <summary>
        /// Repository to hold the acknowledged requests.
        /// </summary>
        private readonly IAcknowledgedRepository acknowledgedRepository;

        /// <summary>
        /// Logic to handle the question logic.
        /// </summary>
        private readonly IQuestionLogic questionLogic;

        /// <summary>
        /// Lock object for the acknowledgment .
        /// </summary>
        private readonly object acknowLock;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        /// <param name="gameRoomRepository">Repository for the game room.</param>
        /// <param name="acknowledged">Reference to the acknowledged repository.</param>
        /// <param name="questionLogic">Logic for the question.</param>
        public GameLogic(IGameRoomRepository gameRoomRepository, IAcknowledgedRepository acknowledged, IQuestionLogic questionLogic)
        {
            this.gameRoomRepository = gameRoomRepository;
            this.acknowledgedRepository = acknowledged;
            this.questionLogic = questionLogic;
            this.acknowLock = new object();
        }

        /// <summary>
        /// Send an acknowledge request to the database.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        /// <param name="player">Player who is sending the IGameLobbyLogic.</param>
        /// <returns>Returns bool to indicate all players inside room has acknowledged the request.</returns>
        public ConquerNextRoundModel AcknowledgeRequest(string roomName, PlayerQuestionModel player)
        {
            lock (this.acknowLock)
            {
                if (!string.IsNullOrEmpty(roomName) && player != null)
                {
                    this.acknowledgedRepository.AddAcknowledgeToRoom(roomName, player);
                    bool answered = this.acknowledgedRepository.AreAllPlayersAcknowledged(roomName);
                    if (answered)
                    {
                        AcknowledgeModel acknowledges = this.acknowledgedRepository.GetPlayerAcknowledgements(roomName);
                        PlayerQuestionModel firstPlayer = acknowledges.Acknowledge[0];
                        PlayerQuestionModel secondPlayer = acknowledges.Acknowledge[1];

                        ConquerNextRoundModel returnModel = new ConquerNextRoundModel()
                        {
                            PrevWinnerPlayer = string.Empty,
                        };
                        Debug.Print("\nANSWERS: " + firstPlayer.AnswerIsCorrect.ToString() + " - " + secondPlayer.AnswerIsCorrect.ToString() + "\n");

                        // Both players answered corrctly --> time decide.
                        if (firstPlayer.AnswerIsCorrect && secondPlayer.AnswerIsCorrect)
                        {
                            if (firstPlayer.AnswerTime < secondPlayer.AnswerTime)
                            {
                                returnModel.PrevWinnerPlayer = firstPlayer.Name;
                            }
                            else
                            {
                                returnModel.PrevWinnerPlayer = secondPlayer.Name;
                            }
                        }
                        else if (firstPlayer.AnswerIsCorrect && !secondPlayer.AnswerIsCorrect)
                        {
                            // First player answered correctly only.
                            returnModel.PrevWinnerPlayer = firstPlayer.Name;
                        }
                        else if (!firstPlayer.AnswerIsCorrect && secondPlayer.AnswerIsCorrect)
                        {
                            // Second player answered correctly only.
                            returnModel.PrevWinnerPlayer = secondPlayer.Name;
                        }
                        else
                        {
                            returnModel.PrevWinnerPlayer = string.Empty;
                        }

                        this.acknowledgedRepository.ResetAcknowledgeForRoom(roomName);

                        // Fetch the question for the next round
                        returnModel.Question = this.GetNextQuestion(roomName);
                        returnModel.Round = player.Round + 1;
                        Debug.Print("\nROUND: " + returnModel.Round + "\n");
                        Debug.Print("\nWINNER: " + returnModel.PrevWinnerPlayer + "\n");
                        return returnModel;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Get the next question.
        /// </summary>
        /// <param name="roomName">Name of the room to search for categories.</param>
        /// <returns>Returns the actual question for the players.</returns>
        public ConquerQuestionModel GetNextQuestion(string roomName)
        {
            // Get the categories for the room for the random category question.
            IList<int> categoryIdList = this.gameRoomRepository.GetCategoryIdsForGameRoom(roomName);
            int number = RandomNumber.Next(categoryIdList.Count);
            ApiQuestionResponse actualQuestion = this.questionLogic.GetNextQuestion(categoryIdList[number]);

            ConquerQuestionModel returnQuestion = new ConquerQuestionModel();

            foreach (ApiQuestionModel oneQuestion in actualQuestion.Questions)
            {
                returnQuestion.Category = oneQuestion.Category;
                returnQuestion.CorrectAnswer = WebUtility.HtmlDecode(oneQuestion.CorrectAnswer);
                returnQuestion.Difficulty = WebUtility.HtmlDecode(oneQuestion.Difficulty);
                returnQuestion.Question = WebUtility.HtmlDecode(oneQuestion.Question);
                foreach (string oneAnswer in oneQuestion.Incorrect_Answers)
                {
                    returnQuestion.Options.Add(WebUtility.HtmlDecode(oneAnswer));
                }

                returnQuestion.Options.Add(oneQuestion.CorrectAnswer);
                returnQuestion.Options.Shuffle();
            }

            return returnQuestion;
        }
    }
}
