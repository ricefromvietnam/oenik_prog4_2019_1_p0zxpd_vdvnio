﻿//-----------------------------------------------------------------------
// <copyright file="CastleModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Display.Castle
{
    using System;
    using System.Windows.Media.Imaging;
    using Conquer.Constants;
    using Conquer.Database.Model.PlayerModel;

    /// <summary>
    /// Model to hold the important properties of a castle.
    /// </summary>
    public class CastleModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CastleModel"/> class.
        /// </summary>
        public CastleModel()
        {
            this.Image = new BitmapImage(new Uri(Constant.CastleImg, UriKind.Relative));
        }

        /// <summary>
        /// Gets the image of the castle.
        /// </summary>
        public BitmapImage Image { get; private set; }

        /// <summary>
        /// Gets the X position of the castle.
        /// </summary>
        public int X { get; private set; }

        /// <summary>
        /// Gets the Y position of the castle.
        /// </summary>
        public int Y { get; private set; }

        /// <summary>
        /// Gets or sets the owner of the castle.
        /// </summary>
        public Player Owner { get; set; }

        /// <summary>
        /// Set the position of the castle.
        /// </summary>
        /// <param name="x">X position to set.</param>
        /// <param name="y">Y position to set.</param>
        public void SetPosition(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
    }
}
