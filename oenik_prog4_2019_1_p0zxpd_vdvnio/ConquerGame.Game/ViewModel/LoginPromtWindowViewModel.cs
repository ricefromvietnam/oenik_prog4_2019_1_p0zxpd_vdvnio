﻿//-----------------------------------------------------------------------
// <copyright file="LoginPromtWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Game.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CommonServiceLocator;
    using Conquer.Logic.Logic.PlayerLogic;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// View model for the login window.
    /// </summary>
    public class LoginPromtWindowViewModel : ViewModelBase
    {
        /// <summary>
        /// Random value for the random name generator.
        /// </summary>
        private static Random random = new Random();

        /// <summary>
        /// Name of the player.
        /// </summary>
        private string playerName;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginPromtWindowViewModel"/> class.
        /// </summary>
        public LoginPromtWindowViewModel()
        {
            this.playerName = string.Empty;
        }

        /// <summary>
        /// Gets or sets the name of the player.
        /// </summary>
        public string Playername
        {
            get { return this.playerName; }
            set { this.Set(ref this.playerName, value); }
        }

        /// <summary>
        /// Generate random string if the user was lazy and just clicked ok/cancel.
        /// </summary>
        /// <returns>Returns the name of the user.</returns>
        public string RandomString()
        {
            const string Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(Chars, 8)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
