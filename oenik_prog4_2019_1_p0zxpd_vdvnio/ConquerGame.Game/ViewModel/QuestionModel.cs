﻿//-----------------------------------------------------------------------
// <copyright file="QuestionModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Game.ViewModel
{
    using System.Collections.ObjectModel;
    using Conquer.Database.Model.PlayerModel;
    using Conquer.Database.Model.RoundModel;
    using Conquer.Logic.Logic.GameLogic;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// View model for the question window.
    /// </summary>
    public class QuestionModel : ViewModelBase
    {
        /// <summary>
        /// Base model for the question.
        /// </summary>
        private ConquerQuestionModel questionBase;

        /// <summary>
        /// Stores whether the answer for the question was correct or not.
        /// </summary>
        private string correctMessage;

        /// <summary>
        /// Store the answer message if it is correct or not.
        /// </summary>
        private string answer;

        /// <summary>
        /// Left time for the question.
        /// </summary>
        private int leftTime;

        /// <summary>
        /// Initializes a new instance of the <see cref="QuestionModel"/> class.
        /// </summary>
        public QuestionModel()
        {
            if (this.IsInDesignMode)
            {
                this.questionBase = new ConquerQuestionModel()
                {
                    Category = "History",
                    CorrectAnswer = "Alma",
                    Difficulty = "Hard",
                    Options = new ObservableCollection<string>()
                        {
                          "a",
                          "b",
                          "c",
                        },
                    Question = "When was king Josh Born",
                };
                this.answer = "Correct";
            }

            this.leftTime = 15;
        }

        /// <summary>
        /// Gets or sets the left time for the question.
        /// </summary>
        public int LeftTime
        {
            get { return this.leftTime; }
            set { this.Set(ref this.leftTime, value); }
        }

        /// <summary>
        /// Gets or sets the answer messages string for the player.
        /// </summary>
        public string Answer
        {
            get { return this.answer; }
            set { this.Set(ref this.answer, value); }
        }

        /// <summary>
        /// Gets the game logic instance. Created by CreateGameLogic method.
        /// </summary>
        public IGameLogic ViewGameLogic { get; private set; }

        /// <summary>
        /// Gets or sets the correct message for the question.
        /// </summary>
        public string Correct
        {
            get { return this.correctMessage; }
            set { this.Set(ref this.correctMessage, value); }
        }

        /// <summary>
        /// Gets or sets the model for the question.
        /// </summary>
        public ConquerQuestionModel QuestionBase
        {
            get => this.questionBase;
            set => this.Set(ref this.questionBase, value);
        }

        /// <summary>
        /// Create the game logic with the room name.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        /// <param name="localPlayer">Local player of the game.</param>
        public void CreateGameLogic(string roomName, Player localPlayer)
        {
            this.ViewGameLogic = new GameLogic(roomName, localPlayer);
        }
    }
}
