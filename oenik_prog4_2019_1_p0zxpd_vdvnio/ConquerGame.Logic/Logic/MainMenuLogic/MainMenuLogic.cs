﻿//-----------------------------------------------------------------------
// <copyright file="MainMenuLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.Logic.MainMenuLogic
{
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text;
    using Conquer.Database.Model;
    using Conquer.Database.Model.MenuModel;
    using Conquer.Database.Model.PlayerModel;
    using Conquer.Logic.ConquerHttp;
    using Newtonsoft.Json;

    /// <summary>
    /// Implementation of the IMainMenuLogic.
    /// </summary>
    public class MainMenuLogic : IMainMenuLogic
    {
        /// <summary>
        /// Implementation of create new room on the server.
        /// </summary>
        /// <param name="listToAdd">List to add to the new room.</param>
        /// <param name="newRoom">New room to insert.</param>
        public void CreateNewRoom(IList<MainMenuRoomModel> listToAdd, CreateRoomModel newRoom)
        {
            ConquerHttpClient http = new ConquerHttpClient();

            HttpContent serverRoom = new StringContent(JsonConvert.SerializeObject(newRoom), Encoding.UTF8, "application/json");

            MainMenuRoomModel result = http.ConquerHttp.PostAsync("/gamerooms", serverRoom).Result.ReadAndCastJsonResponse<MainMenuRoomModel>();
            listToAdd.Add(result);
        }

        /// <summary>
        /// Enter to a room.
        /// </summary>
        /// <param name="roomName">Name of the room to enter.</param>
        /// <param name="actualPlayer">Actual player who want to join.</param>
        /// <returns>Returns the other player in the room.</returns>
        public Player EnterRoom(string roomName, Player actualPlayer)
        {
            ConquerHttpClient http = new ConquerHttpClient();
            HttpContent enteringPlayer = new StringContent(JsonConvert.SerializeObject(actualPlayer), Encoding.UTF8, "application/json");
            return http.ConquerHttp.PostAsync($"player/{roomName}/joinRoom", enteringPlayer).Result.ReadAndCastJsonResponse<Player>();
        }

        /// <summary>
        /// Implementation of the interface method.
        /// </summary>
        /// <param name="listToAdd">List to add to from the view model.</param>
        public void GetMainMenuRooms(IList<MainMenuRoomModel> listToAdd)
        {
            ConquerHttpClient http = new ConquerHttpClient();

            List<MainMenuRoomModel> result = http.ConquerHttp.GetAsync("/gamerooms").Result.ReadAndCastJsonResponse<List<MainMenuRoomModel>>();
            listToAdd.Clear();
            foreach (MainMenuRoomModel room in result)
            {
                listToAdd.Add(room);
            }
        }
    }
}