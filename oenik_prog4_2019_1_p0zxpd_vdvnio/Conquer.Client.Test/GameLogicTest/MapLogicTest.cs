﻿//-----------------------------------------------------------------------
// <copyright file="MapLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Client.Test.GameLogicTest
{
    using System;
    using Conquer.Database.Model;
    using Conquer.Database.Model.MapModel;
    using Conquer.Database.Model.PlayerModel;
    using Conquer.Logic.Logic.GameLogic.Map;
    using Conquer.Server.Test.TestUtils;
    using NUnit.Framework;

    /// <summary>
    /// Test methods for the map logic based operations.
    /// </summary>
    public class MapLogicTest : TestingContext<MapLogic>, IDisposable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MapLogicTest"/> class.
        /// </summary>
        public MapLogicTest()
        {
            this.ControllerSetup();
        }

        /// <summary>
        /// Dispose of the class.
        /// </summary>
        public void Dispose()
        {
            this.FixtureConf = null;
            this.InjectedMock.Clear();
        }

        /// <summary>
        /// Set up fixtures before every test.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.InjectedMock.Clear();
        }

        /// <summary>
        /// Test method to test the occupy when there is no such area.
        /// </summary>
        [Test]
        public void OccupyOneAreaTestWhenAreaIsNotFound()
        {
            int areaId = 178;
            Player occupyingPlayer = new Player()
            {
                Id = 1,
                Name = "Aley",
                PlayerRole = PlayerRole.Local,
            };

            bool? success = this.TestClass.OccupyOneArea(areaId, occupyingPlayer);
            Assert.False(success);
        }

        /// <summary>
        /// Test method to test the occupy when there such area and it is a normal area.
        /// </summary>
        [Test]
        public void OccupyOneAreaTestWhenAreaIsFoundAndNormalArea()
        {
            int areaId = 1;
            Player occupyingPlayer = new Player()
            {
                Id = 1,
                Name = "Aley",
                PlayerRole = PlayerRole.Local,
            };

            bool? success = this.TestClass.OccupyOneArea(areaId, occupyingPlayer);
            Assert.True(success);
        }

        /// <summary>
        /// Test the get area type when it is not found on the map.
        /// </summary>
        [Test]
        public void GetAreaTypeWhenNoSuchId()
        {
            Assert.Throws<ArgumentException>(() => this.TestClass.GetTypeOfArea(47));
        }

        /// <summary>
        /// Test the get area method when there is such field on the map.
        /// </summary>
        [Test]
        public void GetAreaTypeWhenFound()
        {
            AreaType type = this.TestClass.GetTypeOfArea(1);
            Assert.AreEqual(type, AreaType.Normal);
        }
    }
}
