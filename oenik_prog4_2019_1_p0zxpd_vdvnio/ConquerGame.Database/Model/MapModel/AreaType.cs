﻿//-----------------------------------------------------------------------
// <copyright file="AreaType.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Database.Model.MapModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Enum to store the type of the game fields.
    /// </summary>
    public enum AreaType
    {
        /// <summary>
        /// Normal area. Only one question to conquer.
        /// </summary>
        Normal,

        /// <summary>
        /// Castle area. Three questions to occupy.
        /// </summary>
        Castle,
    }
}
