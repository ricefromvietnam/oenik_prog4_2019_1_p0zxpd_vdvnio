﻿//-----------------------------------------------------------------------
// <copyright file="GameController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Controllers
{
    using System.Diagnostics;
    using System.Threading.Tasks;
    using Conquer.Server.Database.RequestModel;
    using Conquer.Server.Database.ResponseModel;
    using Conquer.Server.Logic.GameLogic;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.SignalR;

    /// <summary>
    /// Controller to store the basic operation of the game.
    /// </summary>
    [Route("[controller]")]
    public class GameController : Controller
    {
        /// <summary>
        /// Game logic reference.
        /// </summary>
        private readonly IGameLogic gameLogic;

        /// <summary>
        /// Hub reference to send events to players.
        /// </summary>
        private readonly IHubContext<GameHub> hub;

        /// <summary>
        /// Lock object to handle concurrency.
        /// </summary>
        private readonly object requestLock;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameController"/> class.
        /// </summary>
        /// <param name="logic">Reference to the game logic.</param>
        /// <param name="hub">Hub context to send events to players.</param>
        public GameController(IGameLogic logic, IHubContext<GameHub> hub)
        {
            this.gameLogic = logic;
            this.hub = hub;
            this.requestLock = new object();
        }

        /// <summary>
        /// Called when the user acknowledges a request.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        /// <param name="player">Player who acknowledges the request.</param>
        /// <returns>Returns a hub event if all players has acknowledged the request.</returns>
        [HttpPost("{roomName}/acknowledge")]
        public IActionResult AcknowledgeRoomRequest(string roomName, [FromBody]PlayerQuestionModel player)
        {
            lock (this.requestLock)
            {
                ConquerNextRoundModel nextRoundModel = this.gameLogic.AcknowledgeRequest(roomName, player);
                if (nextRoundModel != null)
                {
                    Debug.Print("\nSENDING WINNER: " + nextRoundModel.PrevWinnerPlayer + "\n");
                    this.hub.Clients.Group(roomName).SendAsync("nextRound", nextRoundModel);
                }
            }

            return this.Ok();
        }
    }
}
