﻿//-----------------------------------------------------------------------
// <copyright file="LoginPromptWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using Conquer.Game.ViewModel;

    /// <summary>
    /// Interaction logic for LoginPromptWindow.
    /// </summary>
    public partial class LoginPromptWindow : Window
    {
        /// <summary>
        /// View model for the login window.
        /// </summary>
        private LoginPromtWindowViewModel loginViewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginPromptWindow"/> class.
        /// </summary>
        public LoginPromptWindow()
        {
            this.InitializeComponent();
            this.loginViewModel = this.FindResource("VM") as LoginPromtWindowViewModel;
        }

        /// <summary>
        /// Gets the view model the the window.
        /// </summary>
        public LoginPromtWindowViewModel LoginViewModel
        {
            get { return this.loginViewModel; }
        }

        /// <summary>
        /// Automatically called when the window is closed.
        /// </summary>
        /// <param name="sender">Sender of the event (window).</param>
        /// <param name="e">Cancel arguments for the event.</param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (this.loginViewModel.Playername == string.Empty)
            {
                this.loginViewModel.Playername = this.loginViewModel.RandomString();
            }
        }

        /// <summary>
        /// User clicked on the save button.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Parameters for the event.</param>
        private void SaveClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
