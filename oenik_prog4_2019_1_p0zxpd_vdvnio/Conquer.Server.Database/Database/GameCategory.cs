﻿//-----------------------------------------------------------------------
// <copyright file="GameCategory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Database.Database
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Connector table between game room and category.
    /// </summary>
    public class GameCategory
    {
        /// <summary>
        /// Gets or sets the id of the connection.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the id of the room.
        /// </summary>
        public string RoomId { get; set; }

        /// <summary>
        /// Gets or sets the category id.
        /// </summary>
        public int CategoryId { get; set; }
    }
}
