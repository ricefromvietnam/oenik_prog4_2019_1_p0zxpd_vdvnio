﻿//-----------------------------------------------------------------------
// <copyright file="PlayerRole.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Conquer.Database.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Enum to hold the get of the player.
    /// </summary>
    public enum PlayerRole
    {
        /// <summary>
        /// Local is the player is the local player.
        /// </summary>
        Local,

        /// <summary>
        /// If the player is the remote or connecting player.
        /// </summary>
        Remote,
    }
}
