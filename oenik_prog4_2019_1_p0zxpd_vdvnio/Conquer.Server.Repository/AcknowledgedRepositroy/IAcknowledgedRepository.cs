﻿//-----------------------------------------------------------------------
// <copyright file="IAcknowledgedRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Repository.AcknowledgedRepositroy
{
    using Conquer.Server.Database.RequestModel;

    /// <summary>
    /// Interface to hold the repository of the acknowledgements.
    /// </summary>
    public interface IAcknowledgedRepository
    {
        /// <summary>
        /// Add an acknowledgement to a room.
        /// If the room is not in the list appends to it.
        /// Otherwise append the player to the list.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        /// <param name="acknowledgePlayer">Player who sends the acknowledgement.</param>
        void AddAcknowledgeToRoom(string roomName, PlayerQuestionModel acknowledgePlayer);

        /// <summary>
        /// Reset the acknowledgements for the specified room.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        void ResetAcknowledgeForRoom(string roomName);

        /// <summary>
        /// Get if all players has acknowledged the pending request.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        /// <returns>Returns a bool indicating that the number of acknowledgements are equal to two.</returns>
        bool AreAllPlayersAcknowledged(string roomName);

        /// <summary>
        /// Get all the acknowledgment for that room to decide who won.
        /// </summary>
        /// <param name="roomName">Name of the room.</param>
        /// <returns>Returns the acknowledgments for that room.</returns>
        AcknowledgeModel GetPlayerAcknowledgements(string roomName);
    }
}