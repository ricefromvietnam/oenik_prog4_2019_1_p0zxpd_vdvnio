﻿//-----------------------------------------------------------------------
// <copyright file="WaitingPlayerModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Database.Model.WaitingTextModel
{
    using System.Windows.Media;

    /// <summary>
    /// Model to hold the important properties of the waiting for player to choose text.
    /// </summary>
    public class WaitingPlayerModel
    {
        /// <summary>
        /// Gets or sets the content of the waiting string.
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Gets or sets the color of the text.
        /// </summary>
        public Brush TextColor { get; set; }
    }
}
