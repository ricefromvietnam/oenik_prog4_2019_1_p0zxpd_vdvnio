﻿//-----------------------------------------------------------------------
// <copyright file="AutomapperConfig.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using AutoMapper;
    using Conquer.Server.Database.Database;

    /// <summary>
    /// Class that is used to store all mapper configurations.
    /// </summary>
    public class AutomapperConfig
    {
        /// <summary>
        /// This method initializes a new mapper object with the prepared mapping capabilities.
        /// </summary>
        /// <returns>Return a new Mapper object.</returns>
        public static IMapper InitializeMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
            });

            IMapper mapper = config.CreateMapper();
            return mapper;
        }
    }
}
