﻿//-----------------------------------------------------------------------
// <copyright file="PlayerExistModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Database.Model.PlayerModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Model to store that the chosen player name exists or not.
    /// </summary>
    public class PlayerExistModel
    {
        /// <summary>
        /// Gets or sets a value indicating whether there is a player with that name or not.
        /// </summary>
        public bool Exists { get; set; }
    }
}
