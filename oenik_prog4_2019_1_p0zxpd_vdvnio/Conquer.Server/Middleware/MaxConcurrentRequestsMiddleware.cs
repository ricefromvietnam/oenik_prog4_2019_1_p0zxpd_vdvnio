﻿//-----------------------------------------------------------------------
// <copyright file="MaxConcurrentRequestsMiddleware.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Server.Middleware
{
    using System;
    using System.Diagnostics;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Http.Features;

    /// <summary>
    /// Middleware class to handle the concurrency with a request queue.
    /// https://www.tpeczek.com/2017/08/implementing-concurrent-requests-limit.html?fbclid=IwAR3aSt3beIN_AACer-NGE6qM8fL-CHsjsBpp5QuIGSZ9OuMhoKHM1ZSCpVc .
    /// </summary>
    public class MaxConcurrentRequestsMiddleware
    {
        /// <summary>
        /// Request handler delegate.
        /// </summary>
        private readonly RequestDelegate next;

        /// <summary>
        /// Global options for the queue length.
        /// </summary>
        private readonly int options;

        /// <summary>
        /// Enquer class.
        /// </summary>
        private readonly MaxConcurrentRequestsEnqueuer enqueuer;

        /// <summary>
        /// Current number of requests pending.
        /// </summary>
        private int concurrentRequestsCount;

        /// <summary>
        /// Initializes a new instance of the <see cref="MaxConcurrentRequestsMiddleware"/> class.
        /// </summary>
        /// <param name="next">Request to handle.</param>
        public MaxConcurrentRequestsMiddleware(RequestDelegate next)
        {
            this.concurrentRequestsCount = 0;

            this.next = next ?? throw new ArgumentNullException(nameof(next));
            this.options = 10;
            this.enqueuer = new MaxConcurrentRequestsEnqueuer(10, 10000);
        }

        /// <summary>
        /// Invoke method called when a request arrives into server.
        /// </summary>
        /// <param name="context">Context of the request.</param>
        /// <returns>Void task.</returns>
        public async Task Invoke(HttpContext context)
        {
            if (this.CheckLimitExceeded() && !(await this.TryWaitInQueueAsync(context.RequestAborted)))
            {
                IHttpResponseFeature responseFeature = context.Features.Get<IHttpResponseFeature>();

                responseFeature.StatusCode = StatusCodes.Status503ServiceUnavailable;
                responseFeature.ReasonPhrase = "Concurrent request limit exceeded.";
            }
            else
            {
                try
                {
                    await this.next(context);
                }
                finally
                {
                    if (await this.ShouldDecrementConcurrentRequestsCountAsync())
                    {
                        Interlocked.Decrement(ref this.concurrentRequestsCount);
                    }
                }
            }
        }

        /// <summary>
        /// Try again the request form the queue.
        /// </summary>
        /// <param name="requestAbortedCancellationToken">Token for the cancel operation.</param>
        /// <returns>Returns bool to indicate that the re try was success or not.</returns>
        private async Task<bool> TryWaitInQueueAsync(CancellationToken requestAbortedCancellationToken)
        {
            return (this.enqueuer != null) && (await this.enqueuer.EnqueueAsync(requestAbortedCancellationToken));
        }

        /// <summary>
        /// Decide if we can decrease the number of requests in the queue.
        /// </summary>
        /// <returns>bool value that we can process the next request.</returns>
        private async Task<bool> ShouldDecrementConcurrentRequestsCountAsync()
        {
            return (this.enqueuer == null) || !this.enqueuer.Dequeue();
        }

        /// <summary>
        /// Check if the limit of the parallel request is over or not.
        /// </summary>
        /// <returns>Returns bool.</returns>
        private bool CheckLimitExceeded()
        {
            bool limitExceeded;

            int initialConcurrentRequestsCount, incrementedConcurrentRequestsCount;
            do
            {
                limitExceeded = true;
                initialConcurrentRequestsCount = this.concurrentRequestsCount;
                Debug.Print("\nCHECK: " + initialConcurrentRequestsCount.ToString());
                if (initialConcurrentRequestsCount >= this.options)
                {
                    break;
                }

                limitExceeded = false;
                incrementedConcurrentRequestsCount = initialConcurrentRequestsCount + 1;
            }
            while (initialConcurrentRequestsCount != Interlocked.CompareExchange(
                ref this.concurrentRequestsCount, incrementedConcurrentRequestsCount, initialConcurrentRequestsCount));

            return limitExceeded;
        }
    }
}
