﻿//-----------------------------------------------------------------------
// <copyright file="IPlayerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Conquer.Logic.Logic.PlayerLogic
{
    using Conquer.Database.Model.PlayerModel;

    /// <summary>
    /// Interface to hold the logic operations in connection with the player.
    /// </summary>
    public interface IPlayerLogic
    {
        /// <summary>
        /// Login to the server with the new player.
        /// </summary>
        /// <param name="newPlayer">Model of the new player.</param>
        /// <returns>Returns the new player.</returns>
        Player PlayerLogin(Player newPlayer);
    }
}